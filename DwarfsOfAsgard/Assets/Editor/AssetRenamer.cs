﻿using System.Linq;
using UnityEngine;
using UnityEditor;

public class AssetRenamer : EditorWindow
{

    string[] selection;
    string initName;
    int initNumber;

    [MenuItem("Tools/Asset Renamer")]
    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(AssetRenamer));
    }

    void OnGUI()
    {
        selection = Selection.assetGUIDs;

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        EditorGUILayout.LabelField("Info", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Selected assets: " + selection.Length);

        EditorGUILayout.LabelField("Options", EditorStyles.boldLabel);
        initName = EditorGUILayout.TextField("Name: ", initName);
        initNumber = EditorGUILayout.IntField("Initial number: ", initNumber);

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        if (selection.Length == 0)
        {
            EditorGUILayout.HelpBox("No assets selected", MessageType.Info);
            GUI.enabled = false;
        }
        else GUI.enabled = true;

        if (GUILayout.Button("Rename!"))
        {
            for (int i = 0; i < selection.Length; i++)
            {
                AssetDatabase.RenameAsset(AssetDatabase.GUIDToAssetPath(selection[i]), initName + (initNumber + i));
            }
        }
    }
}