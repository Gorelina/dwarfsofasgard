﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(Effect))]
public class EffectEditor : Editor
{
    SerializedProperty spriteRenderer;
    SerializedProperty sprites;
    SerializedProperty nextEffect;
    SerializedProperty fps;
    SerializedProperty repeatTimes;
    SerializedProperty destroy;
    SerializedProperty loop;
    SerializedProperty playOnAwake;
    SerializedProperty random;
    SerializedProperty randomRot;
    SerializedProperty shake;
    SerializedProperty source;
    SerializedProperty randomClip;
    SerializedProperty clips;
    SerializedProperty randomPitch;
    SerializedProperty pitch;
    Effect script;

    private void OnEnable()
    {
        spriteRenderer = serializedObject.FindProperty("spriteRenderer");
        sprites = serializedObject.FindProperty("sprites");
        nextEffect = serializedObject.FindProperty("nextEffect");
        fps = serializedObject.FindProperty("fps");
        repeatTimes = serializedObject.FindProperty("repeatTimes");
        destroy = serializedObject.FindProperty("destroy");
        loop = serializedObject.FindProperty("loop");
        playOnAwake = serializedObject.FindProperty("playOnAwake");
        random = serializedObject.FindProperty("random");
        randomRot = serializedObject.FindProperty("randomRot");
        shake = serializedObject.FindProperty("shake");
        source = serializedObject.FindProperty("source");
        randomClip = serializedObject.FindProperty("randomClip");
        clips = serializedObject.FindProperty("clips");
        randomPitch = serializedObject.FindProperty("randomPitch");
        pitch = serializedObject.FindProperty("pitch");
        script = (Effect)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(spriteRenderer);
        EditorGUILayout.PropertyField(sprites, true);
        EditorGUILayout.PropertyField(nextEffect);
        EditorGUILayout.PropertyField(fps);
        EditorGUILayout.PropertyField(repeatTimes);
        EditorGUILayout.PropertyField(destroy);
        EditorGUILayout.PropertyField(loop);
        EditorGUILayout.PropertyField(playOnAwake);
        EditorGUILayout.PropertyField(random);
        EditorGUILayout.PropertyField(randomRot);
        EditorGUILayout.PropertyField(shake);

        EditorGUILayout.PropertyField(source);
        if (source.objectReferenceValue != null)
        {
            EditorGUILayout.PropertyField(randomClip);
            if (randomClip.boolValue)
                EditorGUILayout.PropertyField(clips, true);
            EditorGUILayout.PropertyField(randomPitch);
            if (randomPitch.boolValue)
                EditorGUILayout.PropertyField(pitch);
        }

        serializedObject.ApplyModifiedProperties();

        EditorGUILayout.LabelField("Effect time: " + ((float)sprites.arraySize / (float)fps.intValue) * (repeatTimes.intValue + 1) + " sec.");

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Play"))
            script.PlayEditor();

        if (GUILayout.Button("Stop"))
            script.StopEditor();

        EditorGUILayout.EndHorizontal();
    }
}