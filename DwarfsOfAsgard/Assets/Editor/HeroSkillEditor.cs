﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(HeroSkill))]
[CanEditMultipleObjects]
public class HeroSkillEditor : Editor
{

    SerializedProperty icon;
    SerializedProperty littleIcon;
    new SerializedProperty name;
    SerializedProperty cost;
    SerializedProperty type;
    SerializedProperty effect;
    SerializedProperty manaCost;
    SerializedProperty cooldown;
    SerializedProperty castTime;
    SerializedProperty requiredLevel;
    SerializedProperty value;
    SerializedProperty step;

    void OnEnable()
    {
        icon = serializedObject.FindProperty("icon");
        littleIcon = serializedObject.FindProperty("littleIcon");
        name = serializedObject.FindProperty("name");
        cost = serializedObject.FindProperty("cost");
        type = serializedObject.FindProperty("type");
        effect = serializedObject.FindProperty("effect");
        manaCost = serializedObject.FindProperty("manaCost");
        cooldown = serializedObject.FindProperty("cooldown");
        castTime = serializedObject.FindProperty("castTime");
        requiredLevel = serializedObject.FindProperty("requiredLevel");
        value = serializedObject.FindProperty("value");
        step = serializedObject.FindProperty("step");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(icon);
        Texture2D texture = AssetPreview.GetAssetPreview(icon.objectReferenceValue);
        if (texture != null)
            GUILayout.Label(texture);
        EditorGUILayout.PropertyField(littleIcon);
        Texture2D littleTexture = AssetPreview.GetAssetPreview(littleIcon.objectReferenceValue);
        if (littleTexture != null)
            GUILayout.Label(littleTexture);
        EditorGUILayout.PropertyField(name);
        EditorGUILayout.PropertyField(cost, true);
        EditorGUILayout.PropertyField(effect);
        EditorGUILayout.PropertyField(type);
        //0 - passive, 1 - active
        if (type.enumValueIndex == 0)
        {
            EditorGUILayout.PropertyField(cooldown);
            EditorGUILayout.PropertyField(requiredLevel);
            EditorGUILayout.PropertyField(value);
        }
        else
        {
            EditorGUILayout.PropertyField(manaCost);
            EditorGUILayout.PropertyField(cooldown);
            EditorGUILayout.PropertyField(castTime);
            EditorGUILayout.PropertyField(requiredLevel);
            EditorGUILayout.PropertyField(value);
            EditorGUILayout.PropertyField(step);
        }

        serializedObject.ApplyModifiedProperties();
    }
}