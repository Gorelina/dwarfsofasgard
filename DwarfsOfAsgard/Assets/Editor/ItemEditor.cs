﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Item))]
[CanEditMultipleObjects]
public class ItemEditor : Editor
{

    new SerializedProperty name;
    SerializedProperty icon;
    SerializedProperty type;
    SerializedProperty setItem;
    SerializedProperty setName;
    SerializedProperty rarity;
    SerializedProperty effect;
    SerializedProperty set2Effect;
    SerializedProperty set3Effect;
    SerializedProperty set4Effect;

    void OnEnable()
    {
        name = serializedObject.FindProperty("name");
        icon = serializedObject.FindProperty("icon");
        type = serializedObject.FindProperty("type");
        setItem = serializedObject.FindProperty("setItem");
        setName = serializedObject.FindProperty("setName");
        rarity = serializedObject.FindProperty("rarity");
        effect = serializedObject.FindProperty("effect");
        set2Effect = serializedObject.FindProperty("set2Effect");
        set3Effect = serializedObject.FindProperty("set3Effect");
        set4Effect = serializedObject.FindProperty("set4Effect");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(name);
        EditorGUILayout.PropertyField(icon);
        EditorGUILayout.PropertyField(type);
        EditorGUILayout.PropertyField(rarity);
        EditorGUILayout.PropertyField(effect, true);
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Set", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(setItem);
        if (setItem.boolValue)
        {
            EditorGUILayout.PropertyField(setName);
            EditorGUILayout.PropertyField(set2Effect, true);
            EditorGUILayout.PropertyField(set3Effect, true);
            EditorGUILayout.PropertyField(set4Effect, true);
        }

        serializedObject.ApplyModifiedProperties();
    }
}
