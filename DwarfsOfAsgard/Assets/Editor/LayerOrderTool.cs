﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LayerOrderTool : EditorWindow
{
    GameObject[] selection;

    [MenuItem("Tools/Layer Order Tool")]
    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(LayerOrderTool));
    }

    void OnGUI()
    {
        selection = Selection.gameObjects;

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        EditorGUILayout.LabelField("Info", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Selected objects: " + selection.Length);

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        if (selection.Length == 0)
        {
            EditorGUILayout.HelpBox("No objects selected", MessageType.Info);
            GUI.enabled = false;
        }
        else GUI.enabled = true;

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("+"))
        {
            for (int i = 0; i < selection.Length; i++)
            {
                SpriteRenderer sp = selection[i].GetComponent<SpriteRenderer>();
                if (sp)
                    sp.sortingOrder++;

                SpriteRenderer[] sps = selection[i].GetComponentsInChildren<SpriteRenderer>();
                foreach (SpriteRenderer spriteRenderer in sps)
                    spriteRenderer.sortingOrder++;
            }
        }

        if (GUILayout.Button("-"))
        {
            for (int i = 0; i < selection.Length; i++)
            {
                SpriteRenderer sp = selection[i].GetComponent<SpriteRenderer>();
                if (sp)
                    sp.sortingOrder--;

                SpriteRenderer[] sps = selection[i].GetComponentsInChildren<SpriteRenderer>();
                foreach (SpriteRenderer spriteRenderer in sps)
                    spriteRenderer.sortingOrder--;
            }
        }
        EditorGUILayout.EndHorizontal();
    }
}
