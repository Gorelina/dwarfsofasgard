﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class Renamer : EditorWindow
{
    GameObject[] selection;
    string initName;
    int initNumber;

    [MenuItem("Tools/Renamer")]
    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(Renamer));
    }

    void OnGUI()
    {
        selection = Selection.gameObjects;

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        EditorGUILayout.LabelField("Info", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Selected objects: " + selection.Length);

        EditorGUILayout.LabelField("Options", EditorStyles.boldLabel);
        initName = EditorGUILayout.TextField("Name: ", initName);
        initNumber = EditorGUILayout.IntField("Initial number: ", initNumber);

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        if (selection.Length == 0)
        {
            EditorGUILayout.HelpBox("No objects selected", MessageType.Info);
            GUI.enabled = false;
        }
        else GUI.enabled = true;

        if (GUILayout.Button("Rename!"))
        {

            selection = selection.OrderBy(c => c.transform.GetSiblingIndex()).ToArray();

            for (int i = 0; i < selection.Length; i++)
            {
                selection[i].transform.name = initName + (i + initNumber);
                Undo.RegisterCreatedObjectUndo(selection[i], "Rename");
            }
        }
    }
}