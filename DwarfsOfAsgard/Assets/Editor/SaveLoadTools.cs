﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEditor;

public class SaveLoadTools : EditorWindow
{

    static string profilePath, progressPath, dirPath;

    [MenuItem("Tools/SaveLoad Tools")]
    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(SaveLoadTools));

        profilePath = Application.persistentDataPath + "/save/profile.dat";
        progressPath = Application.persistentDataPath + "/save/progress.dat";
        dirPath = Application.persistentDataPath + "/save";
        if (!Directory.Exists(dirPath))
            Directory.CreateDirectory(dirPath);
    }

    public string GetProfile()
    {
        if (!File.Exists(profilePath))
            return "error";

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(profilePath, FileMode.Open);

        SaveLoad.Profile data = (SaveLoad.Profile)bf.Deserialize(file);
        file.Close();
        return data.Show();
    }

    public string GetProgress()
    {
        if (!File.Exists(progressPath))
            return "error";

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(progressPath, FileMode.Open);

        SaveLoad.Progress data = (SaveLoad.Progress)bf.Deserialize(file);
        file.Close();
        return data.Show();
    }

    void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Show progress") && File.Exists(progressPath))
        {
            EditorUtility.DisplayDialog("Progress", GetProgress(), "Close");
        }

        if (GUILayout.Button("Show profile") && File.Exists(profilePath))
        {
            EditorUtility.DisplayDialog("Profile", GetProfile(), "Close");
        }

        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Delete save files"))
        {
            if (File.Exists(profilePath))
                File.Delete(profilePath);

            if (File.Exists(progressPath))
                File.Delete(progressPath);
        }

        if (GUILayout.Button("Show save files"))
        {
            Application.OpenURL("file://" + dirPath);
        }
    }
}