﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(SpriteImage))]
public class SpriteImageDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty sprite = property.FindPropertyRelative("sprite");

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        Rect fieldRect = new Rect(position.x, position.y, position.width - 128, 16);
        EditorGUI.PropertyField(fieldRect, sprite, GUIContent.none, false);

        Rect previewRect = new Rect(position.x + position.width - 96, position.y, 96, 96);
        Texture2D texture = AssetPreview.GetAssetPreview(sprite.objectReferenceValue);
        if (texture != null)
            GUI.Label(previewRect, texture);

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + 80;
    }
}
