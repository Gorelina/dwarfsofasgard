﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Tween))]
[CanEditMultipleObjects]
public class TweenEditor : Editor
{

    Vector3 oldPos;

    SerializedProperty targetPos;
    Tween script;

    void OnEnable()
    {
        targetPos = serializedObject.FindProperty("targetPos");
        script = (Tween)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Save Current Pos"))
        {
            oldPos = script.transform.localPosition;
        }

        serializedObject.Update();

        if (GUILayout.Button("Set Target Pos"))
        {
            targetPos.vector3Value = script.transform.localPosition;
        }

        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("Revert"))
        {
            script.transform.localPosition = oldPos;
        }
    }

}