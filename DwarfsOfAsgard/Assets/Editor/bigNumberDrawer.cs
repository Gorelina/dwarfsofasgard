﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(bigNumber))]
public class bigNumberDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty number = property.FindPropertyRelative("number");
        SerializedProperty prefixIndex = property.FindPropertyRelative("prefixIndex");

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Rect numberRect = new Rect(position.x, position.y, position.width / 2, position.height / 2);
        EditorGUI.PropertyField(numberRect, number, GUIContent.none, false);

        Rect prefixRect = new Rect(position.x + position.width / 2, position.y, position.width / 2, position.height / 2);
        EditorGUI.PropertyField(prefixRect, prefixIndex, GUIContent.none, false);

        Rect labelRect = new Rect(position.x, position.y + position.height / 2, position.width, position.height / 2);
        string labelContent = number.floatValue + "E+" + prefixIndex.intValue * 3 + " || ";
        labelContent += number.floatValue + bigNumber.GetPrefix(prefixIndex.intValue);
        EditorGUI.LabelField(labelRect, labelContent);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) * 2;
    }
}