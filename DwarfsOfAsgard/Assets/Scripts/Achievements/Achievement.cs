﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
[CreateAssetMenu(fileName = "New Achievement", menuName = "General/Achievement")]
public class Achievement : DBEntry
{

    [System.Serializable]
    public class Stage
    {
        public bigNumber target;
        public int reward;
    }

    public SpriteImage icon;
    public Stage[] stages;

    public void Init()
    {
        for (int i = 0; i < stages.Length; i++)
        {
            stages[i].target = new bigNumber(stages[i].target);
        }
    }

}

public class AchievementInstance
{
    public Achievement data;
    public int stage;
    public bigNumber value;
    public float progress;
    public AchievementManager manager;
    public bool completed;
    public int reward;
    public bool changed;

    public AchievementInstance(Achievement data, AchievementManager manager)
    {
        this.data = data;
        this.manager = manager;
    }

    public void Load(AchievementData data)
    {
        stage = data.stage;
        completed = data.completed;
        value = new bigNumber(data.value);
        progress = (value / this.data.stages[stage].target).number;
        reward = data.reward;
    }

    public AchievementData Save()
    {
        return new AchievementData(data.id, stage, completed, value.GetString(), reward);
    }

    public void Update()
    {
        progress = (value / data.stages[stage].target).number;
        if (progress >= 1 && !completed)
        {
            reward += data.stages[stage].reward;
            changed = true;
            if (stage < data.stages.Length - 1)
            {
                stage++;
            }
            else completed = true;
            manager.Save();
        }
    }

    public void Set(bigNumber _value)
    {
        value = new bigNumber(_value);
        Update();
    }

    public void Add(bigNumber _value)
    {
        value += _value;
        Update();
    }

    public void Increment()
    {
        value += 1;
        Update();
    }
}

[System.Serializable]
public class AchievementData
{
    public int id;
    public int stage;
    public bool completed;
    public string value;
    public int reward;

    public AchievementData()
    {
        id = 0;
        stage = 0;
        completed = false;
        value = "0E+0";
        reward = 0;
    }

    public AchievementData(int id, int stage, bool completed, string value, int reward)
    {
        this.id = id;
        this.stage = stage;
        this.completed = completed;
        this.value = value;
        this.reward = reward;
    }
}