﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementManager : MonoBehaviour
{

    public GameObject window;
    public Sprite buttonNormal;
    public Sprite buttonReward;
    public GameObject notification;
    public Transform achievementHolder;
    public Transform achievementForm;
    private AchievementInstance[] achievementInstances;
    private AchievementForm[] achievementForms;
    public static AchievementManager instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        achievementInstances = new AchievementInstance[DataBase.instance.achievements.Length];
        for (int i = 0; i < achievementInstances.Length; i++)
        {
            achievementInstances[i] = new AchievementInstance(DataBase.instance.achievements[i], this);
            achievementInstances[i].Load(SaveLoad.achievementDatas[i]);
        }

        InitUI();
        UpdateUI();
        InvokeRepeating("Save", 1, 1);
    }

    void InitUI()
    {
        achievementForms = new AchievementForm[achievementInstances.Length];

        for (int i = 0; i < achievementForms.Length; i++)
        {
            Transform clone = Instantiate(achievementForm, achievementHolder);
            achievementForms[i] = clone.GetComponent<AchievementForm>();
            achievementForms[i].Set(i);

            achievementForms[i].icon.sprite = achievementInstances[i].data.icon.sprite;
            achievementForms[i].nameText.text = LanguageManager.ReturnString(achievementInstances[i].data.name).Replace("%N%", achievementInstances[i].data.stages[achievementInstances[i].stage].target.ToString());
            achievementForms[i].progress.value = achievementInstances[i].progress;
            if (!achievementInstances[i].completed)
            {
                for (int s = 0; s < achievementForms[i].stage.Length; s++)
                {
                    achievementForms[i].stage[s].SetActive(achievementInstances[i].stage > s);
                }
            }
            else
            {
                for (int s = 0; s < achievementForms[i].stage.Length; s++)
                {
                    achievementForms[i].stage[s].SetActive(true);
                }
            }

            if (achievementInstances[i].reward > 0)
            {
                achievementForms[i].action.Change("COLLECT");
                achievementForms[i].reward.text = achievementInstances[i].reward.ToString();
            }
            else
            {
                achievementForms[i].action.Change("REWARD");
                achievementForms[i].reward.text = achievementInstances[i].data.stages[achievementInstances[i].stage].reward.ToString();
            }

            achievementForms[i].progressText.text = achievementInstances[i].value.ToString() + " / " + achievementInstances[i].data.stages[achievementInstances[i].stage].target.ToString();

            achievementForms[i].collectButtonSprite.sprite = achievementInstances[i].reward > 0 ? buttonReward : buttonNormal;
        }
    }

    void UpdateUI()
    {
        notification.SetActive(false);
        for (int i = 0; i < achievementForms.Length; i++)
        {
            if (!notification.activeSelf && achievementInstances[i].reward > 0)
                notification.SetActive(true);

            if (!window.activeSelf)
                continue;

            if (achievementInstances[i].changed)
            {
                achievementForms[i].nameText.text = LanguageManager.ReturnString(achievementInstances[i].data.name).Replace("%N%", achievementInstances[i].data.stages[achievementInstances[i].stage].target.ToString());
                if (!achievementInstances[i].completed)
                {
                    for (int s = 0; s < achievementForms[i].stage.Length; s++)
                    {
                        achievementForms[i].stage[s].SetActive(achievementInstances[i].stage > s);
                    }
                }
                else
                {
                    for (int s = 0; s < achievementForms[i].stage.Length; s++)
                    {
                        achievementForms[i].stage[s].SetActive(true);
                    }
                }

                if (achievementInstances[i].reward > 0)
                {
                    achievementForms[i].action.Change("COLLECT");
                    achievementForms[i].reward.text = achievementInstances[i].reward.ToString();
                }
                else
                {
                    achievementForms[i].action.Change("REWARD");
                    achievementForms[i].reward.text = achievementInstances[i].data.stages[achievementInstances[i].stage].reward.ToString();
                }

                achievementInstances[i].changed = false;
            }

            achievementForms[i].progress.value = achievementInstances[i].progress;
            achievementForms[i].progressText.text = achievementInstances[i].value.ToString() + " / " + achievementInstances[i].data.stages[achievementInstances[i].stage].target.ToString();

            achievementForms[i].collectButtonSprite.sprite = achievementInstances[i].reward > 0 ? buttonReward : buttonNormal;
        }
    }

    public void CollectReward(int id)
    {
        if (achievementInstances[id].reward <= 0)
            return;

        GameUI.instance.soundContainer.PlaySound("click");
        Mining.instance.AddDiamonds(achievementInstances[id].reward);
        achievementInstances[id].reward = 0;
        achievementInstances[id].changed = true;
        achievementInstances[id].Update();
        UpdateUI();
    }

    int GetIndex(string name)
    {
        for (int i = 0; i < achievementInstances.Length; i++)
        {
            if (achievementInstances[i].data.name == name)
                return i;
        }

        return -1;
    }

    public void Save()
    {
        for (int i = 0; i < achievementInstances.Length; i++)
            SaveLoad.achievementDatas[i] = achievementInstances[i].Save();
    }    

    public void Add(string name, bigNumber value)
    {
        int index = GetIndex(name);

        if (index >= 0)
        {
            achievementInstances[index].Add(value);
            UpdateUI();
        }
        else
        {
            Debug.LogError("Achievement " + name + " not found");
        }
    }

    public void Increment(string name)
    {
        int index = GetIndex(name);

        if (index >= 0)
        {
            achievementInstances[index].Increment();
            UpdateUI();
        }
        else
        {
            Debug.LogError("Achievement " + name + " not found");
        }
    }

    public void Set(string name, bigNumber value)
    {
        int index = GetIndex(name);

        if (index >= 0)
        {
            achievementInstances[index].Set(value);
            UpdateUI();
        }
        else
        {
            Debug.LogError("Achievement " + name + " not found");
        }
    }

    public void OpenWindow()
    {
        window.SetActive(true);
        UpdateUI();
    }

    public void CloseWindow()
    {
        window.SetActive(false);
    }
}