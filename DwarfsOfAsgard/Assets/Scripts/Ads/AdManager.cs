﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdManager : MonoBehaviour
{

    public static AdManager instance;
    private static Action rewardCallback;
    private static RewardedAd rewardedAd;
    private static bool reward;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    void Start()
    {

        Amplitude amplitude = Amplitude.Instance;
        amplitude.logging = true;
        amplitude.init("3d9f14adb6e7d76de0a5917533ffef18", SystemInfo.deviceUniqueIdentifier);
        amplitude.logEvent("game_start");

        MobileAds.Initialize(initStatus => { });
        ReloadRewarded();
    }

    void Update()
    {
        if (reward)
        {
            rewardCallback.Invoke();
            reward = false;
        }
    }

    void ReloadRewarded()
    {
#if UNITY_IOS
        rewardedAd = new RewardedAd("ca-app-pub-3940256099942544/1712485313"); //ca-app-pub-3798788013988728/5713791686
#elif UNITY_ANDROID
        rewardedAd = new RewardedAd("ca-app-pub-3940256099942544/5224354917"); //ca-app-pub-3798788013988728/5887392941
#else
        rewardedAd = new RewardedAd("unused");
#endif
        rewardedAd.OnUserEarnedReward += RewardedAdAward;
        rewardedAd.OnAdLoaded += RewardedAd_OnAdLoaded;
        rewardedAd.OnAdFailedToLoad += RewardedAd_OnAdFailedToLoad;
        rewardedAd.OnAdClosed += RewardedAd_OnAdClosed;

        AdRequest request = new AdRequest.Builder().Build();
        rewardedAd.LoadAd(request);
    }

    private void RewardedAd_OnAdClosed(object sender, EventArgs e)
    {
        ReloadRewarded();
    }

    private void RewardedAd_OnAdFailedToLoad(object sender, AdErrorEventArgs e)
    {
        Debug.Log("Failed to load an ad. " + e.Message);
    }

    private void RewardedAd_OnAdLoaded(object sender, EventArgs e)
    {
        Debug.Log("Ad loaded");
    }

    public static void WatchRewarded(Action callback)
    {
#if UNITY_EDITOR
        reward = true;
        rewardCallback = callback;
#else
        if (rewardedAd.IsLoaded())
        {
            rewardCallback = callback;
            rewardedAd.Show();
        }
        else Debug.Log("ad not loaded");
#endif
    }

    public void RewardedAdAward(object sender, Reward args)
    {
        reward = true;
    }

    public static bool RewardAdLoaded()
    {
        return rewardedAd.IsLoaded();
    }

    void OnApplicationQuit()
    {
        Dictionary<string, object> options = new Dictionary<string, object>() {
            {"GameTime", Time.time}
        };
        Amplitude.Instance.logEvent("game_end", options);
    }

    void OnApplicationPause(bool pause)
    {
        Dictionary<string, object> options = new Dictionary<string, object>() {
            {"GameTime", Time.time}
        };
        Amplitude.Instance.logEvent("game_end", options);
    }
}