﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArtefactForm : MonoBehaviour
{
    public int id;
    public Image icon;
    public Text nameText;
    public Text descText;
    public Text cost;
    public Text level;
    public Button buyButton;

    public void Set(int _id)
    {
        id = _id;
        buyButton.onClick.AddListener(delegate { Artefacts.instance.Upgrade(id); });
    }
}