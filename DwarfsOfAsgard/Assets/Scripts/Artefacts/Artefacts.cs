﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Artefacts : MonoBehaviour
{

    public GameObject notification;
    public Button buyButton;
    public StringRead buyButtonPrice;
    public GameObject tab;
    private bigNumber artefactValue;
    private List<ArtefactInstance> artefacts = new List<ArtefactInstance>();
    private static bigNumber oldPrice;
    private static int oldCount;
    public static Artefacts instance;

    void Awake()
    {
        oldPrice = new bigNumber();
        oldCount = -1;
        instance = this;
    }

    void Start()
    {
        artefactValue = new bigNumber(SaveLoad.artefactValue);
        LoadArtefacts();
    }

    void Update()
    {
        if (!tab.activeSelf)
        {
            notification.SetActive(artefactValue >= GetPrice());
            return;
        }

        notification.SetActive(false);
        GameUI.instance.artefactCurrency.text = artefactValue.ToString();
        buyButton.interactable = artefactValue >= GetPrice();
        buyButtonPrice.Change("BUY", '\n' + GetPrice().ToString());
    }

    void UpdateArtefacts()
    {
        PerksManager.instance.ProcessArtefacts();
        GameUI.instance.UpdateArtefacts();
        SaveLoad.artefacts = new List<Inventory.ItemSave>();
        for (int i = 0; i < artefacts.Count; i++)
        {
            SaveLoad.artefacts.Add(new Inventory.ItemSave(artefacts[i].GetData().id, artefacts[i].GetLevel()));
        }
    }

    public void LoadArtefacts()
    {
        for (int i = 0; i < SaveLoad.artefacts.Count; i++)
            artefacts.Add(new ArtefactInstance(DataBase.instance.artefacts[SaveLoad.artefacts[i].id], SaveLoad.artefacts[i].level));
        UpdateArtefacts();
    }

    public void AddArtefact(Artefact artefact)
    {
        artefacts.Add(new ArtefactInstance(artefact, 0));
        UpdateArtefacts();
    }

    public static bigNumber GetPrice()
    {
        if (oldCount == instance.artefacts.Count)
            return oldPrice;
        else
        {
            bigNumber result = new bigNumber(2);
            for (int i = 0; i < instance.artefacts.Count; i++)
                result *= 1.5f;
            oldPrice = new bigNumber(result);
            oldCount = instance.artefacts.Count;
            return result;
        }
    }

    public static bigNumber GetUpgradePrice(int id)
    {
        return new bigNumber(2 + instance.artefacts[id].GetLevel() * 2 * 1.3f);
    }

    public void BuyArtefact()
    {
        if (artefactValue < GetPrice())
            return;

        GameUI.instance.soundContainer.PlaySound("click");

        int c = 50;
        int rand = Random.Range(0, DataBase.instance.artefacts.Length);
        while (true)
        {
            if (c <= 0)
                break;

            if (!HaveArtefact(DataBase.instance.artefacts[rand]))
            {
                AddArtefact(DataBase.instance.artefacts[rand]);
                AchievementManager.instance.Increment("ACHIEVEMENT_ARTEFACTS");
                SubArtefactValue(GetPrice());
                break;
            }
            else
            {
                rand = Random.Range(0, DataBase.instance.artefacts.Length);
            }

            c--;
        }
    }

    public void AddArtefactValue(bigNumber value)
    {
        artefactValue += value;
        SaveLoad.artefactValue = artefactValue.GetString();
        AchievementManager.instance.Add("ACHIEVEMENT_ARTEFACT_CURRENCY", new bigNumber(value));
    }

    public void SubArtefactValue(bigNumber value)
    {
        artefactValue -= value;
        SaveLoad.artefactValue = artefactValue.GetString();
    }

    public bigNumber GetArtefactValue()
    {
        return artefactValue;
    }

    public bool HaveArtefact(Artefact artefact)
    {
        for (int i = 0; i < artefacts.Count; i++)
        {
            if (artefacts[i].GetData().id == artefact.id)
                return true;
        }
        return false;
    }

    public List<ArtefactInstance> GetArtefacts()
    {
        return artefacts;
    }

    public List<int> GetArtefactsIndexes()
    {
        List<int> result = new List<int>();
        for (int i = 0; i < artefacts.Count; i++)
            result.Add(artefacts[i].GetData().id);
        return result;
    }

    public void Upgrade(int id)
    {
        if (artefactValue < GetUpgradePrice(id))
            return;

        string message = LanguageManager.ReturnString("ARTEFACTS_WINDOW_UPGRADE");
        message = message.Replace("%N%", LanguageManager.ReturnString(artefacts[id].GetData().name));
        message = message.Replace("%V%", GetUpgradePrice(id).ToString());
        GameUI.instance.ShowWindow(message, delegate { UpgradeConfirm(id); }, true);
    }

    void UpgradeConfirm(int id)
    {
        SubArtefactValue(GetUpgradePrice(id));

        GameUI.instance.soundContainer.PlaySound("click");

        artefacts[id].LevelUp();
        UpdateArtefacts();
    }
}