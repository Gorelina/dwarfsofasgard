﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{

    public Transform deathEffect;

    public void Die()
    {
        Instantiate(deathEffect, transform.position + Vector3.up * 1.2f, Quaternion.identity);
    }

}