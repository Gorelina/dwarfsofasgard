﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TapticPlugin;

public class CameraScript : MonoBehaviour
{

    public Animator animator;
    public static CameraScript instance;

    void Awake()
    {
        instance = this;
    }

    public void Shake()
    {
        TapticManager.Impact(ImpactFeedback.Light);
        animator.SetTrigger("Shake");
    }

    public void Critical()
    {
        animator.SetTrigger("Critical");
    }
}