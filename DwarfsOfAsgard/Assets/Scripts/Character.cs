﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageSource {Tap, Helper, Skill, Other };
public enum DamageTypes {Hero, Melee, Range };

public class Character : MonoBehaviour
{
    public bigNumber baseDamage;
    public bigNumber baseCost;
    private int level;
    private bool hero;
    private bigNumber damage;
    private bigNumber cost;
    private int lastLevelUpgrade;
    private KeyValuePair<int, bigNumber> lastCost;
    private List<DataBase.ImprovementEntry> improvementTable;

    public void Init(bool hero)
    {
        this.hero = hero;
        improvementTable = hero ? DataBase.instance.heroImprovementEntries : DataBase.instance.helperImprovementEntries;
        level = 1;
        damage = new bigNumber(baseDamage);
        cost = new bigNumber(baseCost);
        lastLevelUpgrade = 0;
        lastCost = new KeyValuePair<int, bigNumber>(-1, new bigNumber());
    }

    public virtual void LevelUp(int levelBonus = 1)
    {
        if (level == 6000)
            return;

        GetLevelUpBonus(out bigNumber damageBonus, out bigNumber costBonus, levelBonus);
        damage += damageBonus;
        cost = costBonus;
        level += levelBonus;
    }

    public virtual void SetLevel(int level)
    {
        if (this.level == level || level == 0)
        {
            Initial();
            return;
        }

        this.level = level;
        if (this.level > 6000)
            this.level = 6000;
        Initial();
    }

    public virtual bigNumber GetPureDamage()
    {
        return damage;
    }

    public virtual bigNumber GetFinalDamage()
    {
        return damage;
    }

    public virtual bigNumber GetActualDamage(out bool critical)
    {
        critical = false;
        return damage;
    }

    public virtual void Initial()
    {
        int currLvl = 1;
        int amount = 2;
        int levelUp = level;
        bigNumber multiplier = new bigNumber(1);
        int i = 1;
        for (; i < levelUp; i++)
        {
            for (int j = lastLevelUpgrade; j < improvementTable.Count; j++)
            {
                int lvl = improvementTable[j].level;
                if (hero)
                {
                    if (i + 1 >= lvl)
                    {
                        lastLevelUpgrade = j;
                        currLvl = i + 1;
                        if (currLvl == lvl)
                        {
                            amount = improvementTable[j].amount + 1;
                        }
                        else amount = 2;
                        multiplier = new bigNumber(improvementTable[j].multiplayer);
                    }
                }
                else
                {
                    if (damage >= lvl)
                    {
                        lastLevelUpgrade = j;
                        currLvl = i + 1;
                        if (currLvl == lvl)
                        {
                            amount = improvementTable[j].amount + 1;
                        }
                        else amount = 2;
                        multiplier = new bigNumber(improvementTable[j].multiplayer);
                    }
                }
            }

            if (hero)
            {
                damage += multiplier;
                if (i > 1)
                    cost += multiplier;
            }
            else
            {
                if (i > 1)
                {
                    damage += baseDamage;
                }
            }
        }

        if (!hero && i > 1)
            cost = damage * multiplier;

        lastCost = new KeyValuePair<int, bigNumber>(levelUp, cost);
    }

    public virtual void GetLevelUpBonus(out bigNumber damageBonus, out bigNumber costBonus, int additionalLevel = 0)
    {
        if (level == 6000)
        {
            damageBonus = new bigNumber();
            costBonus = new bigNumber();
            return;
        }

        damageBonus = new bigNumber(damage);
        costBonus = new bigNumber(cost);

        int currLvl = 1;
        int amount = 2;
        int levelUp = level + additionalLevel;
        if (levelUp > 6000)
            levelUp = 6000;
        bigNumber multiplier = new bigNumber(1);
        int i = level;
        for (; i < levelUp; i++)
        {
            for (int j = lastLevelUpgrade; j < improvementTable.Count; j++)
            {
                int lvl = improvementTable[j].level;
                if (hero)
                {
                    if (i + 1 >= lvl)
                    {
                        lastLevelUpgrade = j;
                        currLvl = i + 1;
                        if (currLvl == lvl)
                        {
                            amount = improvementTable[j].amount + 1;
                        }
                        else amount = 2;
                        multiplier = new bigNumber(improvementTable[j].multiplayer);
                    }
                }
                else
                {
                    if (damageBonus >= lvl)
                    {
                        lastLevelUpgrade = j;
                        currLvl = i + 1;
                        if (currLvl == lvl)
                        {
                            amount = improvementTable[j].amount + 1;
                        }
                        else amount = 2;
                        multiplier = new bigNumber(improvementTable[j].multiplayer);
                    }
                }
            }

            if (hero)
            {
                damageBonus += multiplier;
                if (i > 1)
                    costBonus += multiplier;
            }
            else
            {
                damageBonus += baseDamage;
            }
        }

        if (!hero)
            costBonus = damageBonus * multiplier;

        damageBonus -= damage;
        lastCost = new KeyValuePair<int, bigNumber>(levelUp, costBonus);
    }

    public virtual bigNumber GetCost()
    {
        if (Mining.instance.GetDiscount())
            return cost * Mining.instance.discountMult;
        else return cost;
    }

    public virtual bigNumber GetCost(int levelBonus)
    {
        if (--levelBonus <= 0)
            return GetCost();

        int levelUp = level + levelBonus;
        if (levelUp > 6000)
            levelUp = 6000;

        bigNumber costBonus;
        if (lastCost.Key == levelUp)
        {
            costBonus = new bigNumber(lastCost.Value);
        }
        else GetLevelUpBonus(out bigNumber damageBonus, out costBonus, levelBonus);

        if (Mining.instance.GetDiscount())
            return costBonus * Mining.instance.discountMult;
        else return costBonus;
    }

    public virtual bigNumber GetLevelCost(int level)
    {
        if (lastCost.Key == level)
        {
            return lastCost.Value;
        }

        bigNumber result = new bigNumber(baseCost);
        for (int i = 1; i < level; i++)
        {
            if (i > 1)
                result *= 1.08f;
        }
        lastCost = new KeyValuePair<int, bigNumber>(level, result);

        if (Mining.instance.GetDiscount())
            return result * Mining.instance.discountMult;
        else return result;
    }

    public int GetLevel()
    {
        return level;
    }
}