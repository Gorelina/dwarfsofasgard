﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{

    private bigNumber initialValue;
    private float targetY;

    void Awake()
    {
        targetY = Random.Range(1f, 0.25f);
        initialValue = new bigNumber("1E+3") * (Mining.instance.GetCurrentLevel() + 1);
        Destroy(gameObject, 10);
    }

    void Update()
    {
        if (transform.position.y > targetY)
            transform.position -= Vector3.up * Time.deltaTime * 4;
    }

    void OnMouseDown()
    {
        Open();
    }

    void Open()
    {
        Mining.instance.AddCoins(initialValue * PerksManager.instance.Get("ChestAmount"));
        Destroy(gameObject);
    }

}