﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Comics : MonoBehaviour
{

    public Animator animator;
    public Image skipCounter;
    private float skipTime;
    
    void Start()
    {
        Application.targetFrameRate = 60;

        if (!SaveLoad.first)
            Continue();
    }

    void Update()
    {
        if (Input.anyKey)
        {
            skipTime += Time.deltaTime;
        }
        else
        {
            skipTime = 0;
        }

        skipCounter.fillAmount = skipTime;

        if (skipTime >= 1)
            Continue();
    }

    void Continue()
    {
        SceneManager.LoadSceneAsync("Init");
    }

    public void SetLanguage(int lang)
    {
        SaveLoad.language = lang == 0 ? "en" : "ru";
        LanguageManager.Reload();
        animator.SetTrigger("Next");
    }

}
