﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Artefact", menuName = "General/Artefact")]
public class Artefact : DBEntry
{
    public SpriteImage icon;
    public enum Tiers {F, D, C, B, A, S };
    public Tiers tier;
    public string effect;
    public float multiplier;
    public float tapDamage;
}

public class ArtefactInstance
{
    private Artefact data;
    private int level;

    public ArtefactInstance()
    {
        data = null;
        level = 0;
    }

    public ArtefactInstance(Artefact data, int level)
    {
        this.data = data;
        this.level = level;
    }

    public void Set(Artefact data, int level)
    {
        this.data = data;
        this.level = level;
    }

    public Artefact GetData()
    {
        return data;
    }

    public int GetLevel()
    {
        return level;
    }

    public void LevelUp()
    {
        level++;
    }

    public PerkEffect GetEffect()
    {
        PerkEffect effect = new PerkEffect();
        effect.effect = data.effect;
        float mult = data.multiplier;
        bool larg = mult > 1;
        if (larg)
        {
            mult -= 1;
        }
        mult *= (level + 1);
        if (larg)
            mult++;
        effect.multiplier = mult;
        return effect;
    }

    public Inventory.ItemSave GetSaveData()
    {
        return new Inventory.ItemSave(data.id, level);
    }
}