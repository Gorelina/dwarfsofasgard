﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New BossSkill", menuName = "General/Boss skill")]
public class BossSkill : DBEntry
{
    public SpriteImage icon;
    public string effect;
    public float multiplier;
}