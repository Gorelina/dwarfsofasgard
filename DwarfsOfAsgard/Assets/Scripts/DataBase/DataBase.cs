﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBase : MonoBehaviour
{

    public struct ImprovementEntry
    {
        public int level;
        public int amount;
        public string multiplayer;

        public ImprovementEntry(int level, int amount, string multiplayer)
        {
            this.level = level;
            this.amount = amount;
            this.multiplayer = multiplayer;
        }
    }

    public struct SkillInfo
    {
        public string owner;
        public string name;
        public string bonusType;
        public float magnitude;
        public int requiredLevel;

        public SkillInfo(string owner, string name, string bonusType, float magnitude, int requiredLevel)
        {
            this.owner = owner;
            this.name = name;
            this.bonusType = bonusType;
            this.magnitude = magnitude;
            this.requiredLevel = requiredLevel;
        }
    }

    [Header("Warehouse")]
    public WarehouseBuff[] warehouseBuffs;
    [Header("SkillTree")]
    public TreeSkill[] treeSkills;
    [Header("Achievements")]
    public Achievement[] achievements;
    [Header("Bosses")]
    public BossSkill[] bossSkills;
    [Header("Artefacts")]
    public Artefact[] artefacts;
    [Header("Inventory")]
    public Item[] items;
    [Header("Levels")]
    public Level[] levels;
    [Header("Helpers")]
    public TextAsset helperImprovementsInfo;
    public TextAsset helperSkillInfo;
    public TextAsset heroImprovementsInfo;
    [System.NonSerialized]
    public List<ImprovementEntry> helperImprovementEntries;
    [System.NonSerialized]
    public List<SkillInfo> helperPerksEntries;
    [System.NonSerialized]
    public List<ImprovementEntry> heroImprovementEntries;
    public static DataBase instance;

    void Awake()
    {
        instance = this;

        for (int i = 0; i < warehouseBuffs.Length; i++)
        {
            warehouseBuffs[i].id = i;
        }

        for (int i = 0; i < treeSkills.Length; i++)
        {
            treeSkills[i].id = i;
        }

        for (int i = 0; i < achievements.Length; i++)
        {
            achievements[i].id = i;
            achievements[i].Init();
        }

        for (int i = 0; i < bossSkills.Length; i++)
        {
            bossSkills[i].id = i;
        }

        for (int i = 0; i < items.Length; i++)
        {
            items[i].id = i;
            /*switch (items[i].type)
            {
                case Item.Types.PickAxe:
                    items[i].effect.effect = Random.Range(0, 2) == 0 ? "CritDamage" : "TapDamage";
                    break;

                case Item.Types.Head:
                    int rand = Random.Range(0, 4);
                    if (rand == 0)
                        items[i].effect.effect = "EngineerSkills";
                    else if (rand == 1)
                        items[i].effect.effect = "GunnersSkills";
                    else if (rand == 2)
                        items[i].effect.effect = "StonersSkills";
                    else items[i].effect.effect = "SupportersSkills";
                    break;

                case Item.Types.Chest:
                    items[i].effect.effect = Random.Range(0, 2) == 0 ? "RangedHelperDamage" : "MeleeHelperDamage";
                    break;

                case Item.Types.Cloak:
                    items[i].effect.effect = Random.Range(0, 2) == 0 ? "AirDamage" : "GroundDamage";
                    break;
            }

            float mult = Random.Range(0.00001f, 0.00009f);
            mult *= Mathf.Pow(10, (int)items[i].rarity);
            mult += 1;
            items[i].effect.multiplier = mult;
            items[i].SetDirty();*/
        }

        for (int i = 0; i < artefacts.Length; i++)
        {
            artefacts[i].id = i;
        }

        for (int i = 0; i < levels.Length; i++)
        {
            levels[i].id = i;
        }

        helperImprovementEntries = new List<ImprovementEntry>();
        heroImprovementEntries = new List<ImprovementEntry>();
        helperPerksEntries = new List<SkillInfo>();

        LoadTable_Improvement(helperImprovementsInfo.text, helperImprovementEntries);
        LoadTable_Skill(helperSkillInfo.text, helperPerksEntries);
        LoadTable_Improvement(heroImprovementsInfo.text, heroImprovementEntries);
    }

    void LoadTable_Improvement(string data, List<ImprovementEntry> target)
    {
        string[] lines = data.Split('\n');
        string[] keys = new string[0];
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].IndexOf((char)13) >= 0)
                lines[i] = lines[i].Remove(lines[i].IndexOf((char)13));

            if (lines[i].Length <= 2)
                continue;

            string[] lineData = LanguageManager.Parse(lines[i]);
            if (i > 0)
            {
                target.Add(new ImprovementEntry(int.Parse(lineData[0]), int.Parse(lineData[1]), lineData[2]));
            }
        }
    }

    void LoadTable_Skill(string data, List<SkillInfo> target)
    {
        string[] lines = data.Split('\n');
        string[] keys = new string[0];
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].IndexOf((char)13) >= 0)
                lines[i] = lines[i].Remove(lines[i].IndexOf((char)13));

            if (lines[i].Length <= 2)
                continue;

            string[] lineData = LanguageManager.Parse(lines[i]);
            if (i > 0)
            {
                target.Add(new SkillInfo(lineData[0], lineData[1], lineData[2], float.Parse(lineData[3], System.Globalization.CultureInfo.InvariantCulture.NumberFormat), int.Parse(lineData[4])));
            }
        }
    }

    public HelperSkill[] GetHelperSkill(string id)
    {
        List<HelperSkill> result = new List<HelperSkill>();

        for (int i = 0; i < helperPerksEntries.Count; i++)
        {
            if (helperPerksEntries[i].owner == id)
            {
                result.Add(new HelperSkill(helperPerksEntries[i]));
            }
        }

        return result.ToArray();
    }

    public Level GetLevel(int level)
    {
        int levelDiv = level / 5;
        while(true)
        {
            if (levelDiv >= levels.Length)
                levelDiv -= levels.Length;
            else break;
        }
        return levels[levelDiv];
    }

    public List<Level> GetLevels(int level, int count)
    {
        List<Level> lvls = new List<Level>();

        for (int i = level; i < count; i++)
            lvls.Add(GetLevel(i));

        return lvls;
    }
}