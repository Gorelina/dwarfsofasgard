﻿public class Drop
{

    public enum Types {stone, coal, copper, iron, silver, gold, platinum };
    public bigNumber coins;
    public bigNumber stone;
    public bigNumber coal;
    public bigNumber copper;
    public bigNumber iron;
    public bigNumber silver;
    public bigNumber gold;
    public bigNumber platinum;

    public Drop()
    {
        coins = new bigNumber();
        stone = new bigNumber();
        coal = new bigNumber();
        copper = new bigNumber();
        iron = new bigNumber();
        silver = new bigNumber();
        gold = new bigNumber();
        platinum = new bigNumber();
    }

    public Drop(bigNumber _stone, bigNumber _coal, bigNumber _copper, bigNumber _iron, bigNumber _silver, bigNumber _gold, bigNumber _platinum)
    {
        coins = new bigNumber();
        stone = new bigNumber(_stone);
        coal = new bigNumber(_coal);
        copper = new bigNumber(_copper);
        iron = new bigNumber(_iron);
        silver = new bigNumber(_silver);
        gold = new bigNumber(_gold);
        platinum = new bigNumber(_platinum);
    }

    public Drop(bigNumber _coins, bigNumber _stone, bigNumber _coal, bigNumber _copper, bigNumber _iron, bigNumber _silver, bigNumber _gold, bigNumber _platinum)
    {
        coins = new bigNumber(_coins);
        stone = new bigNumber(_stone);
        coal = new bigNumber(_coal);
        copper = new bigNumber(_copper);
        iron = new bigNumber(_iron);
        silver = new bigNumber(_silver);
        gold = new bigNumber(_gold);
        platinum = new bigNumber(_platinum);
    }

    public Drop(bigNumber _coins, bigNumber _prestige, bigNumber _stone, bigNumber _coal, bigNumber _copper, bigNumber _iron, bigNumber _silver, bigNumber _gold, bigNumber _platinum)
    {
        coins = new bigNumber(_coins);
        stone = new bigNumber(_stone);
        coal = new bigNumber(_coal);
        copper = new bigNumber(_copper);
        iron = new bigNumber(_iron);
        silver = new bigNumber(_silver);
        gold = new bigNumber(_gold);
        platinum = new bigNumber(_platinum);
    }

    public Drop(Drop drop)
    {
        coins = new bigNumber(drop.coins);
        stone = new bigNumber(drop.stone);
        coal = new bigNumber(drop.coal);
        copper = new bigNumber(drop.copper);
        iron = new bigNumber(drop.iron);
        silver = new bigNumber(drop.silver);
        gold = new bigNumber(drop.gold);
        platinum = new bigNumber(drop.platinum);
    }

    public static Drop operator+ (Drop a, Drop b)
    {
        Drop result = new Drop(a);

        result.coal += b.coal;
        result.coins += b.coins;
        result.copper += b.copper;
        result.gold += b.gold;
        result.iron += b.iron;
        result.platinum += b.platinum;
        result.silver += b.silver;
        result.stone += b.stone;

        return result;
    }

    public static Drop operator- (Drop a, Drop b)
    {
        Drop result = new Drop(a);

        result.coal -= b.coal;
        result.coins -= b.coins;
        result.copper -= b.copper;
        result.gold -= b.gold;
        result.iron -= b.iron;
        result.platinum -= b.platinum;
        result.silver -= b.silver;
        result.stone -= b.stone;

        return result;
    }

    public static Drop operator *(Drop a, float b)
    {
        Drop result = new Drop(a);

        result.coal *= b;
        result.coins *= b;
        result.copper *= b;
        result.gold *= b;
        result.iron *= b;
        result.platinum *= b;
        result.silver *= b;
        result.stone *= b;

        return result;
    }

    public static Drop operator /(Drop a, float b)
    {
        Drop result = new Drop(a);

        result.coal /= b;
        result.coins /= b;
        result.copper /= b;
        result.gold /= b;
        result.iron /= b;
        result.platinum /= b;
        result.silver /= b;
        result.stone /= b;

        return result;
    }

    public static bool operator >(Drop a, Drop b)
    {
        if (a.coal <= b.coal)
            return false;
        if (a.coins <= b.coins)
            return false;
        if (a.copper <= b.copper)
            return false;
        if (a.gold <= b.gold)
            return false;
        if (a.iron <= b.iron)
            return false;
        if (a.platinum <= b.platinum)
            return false;
        if (a.silver <= b.silver)
            return false;
        if (a.stone <= b.stone)
            return false;

        return true;
    }

    public static bool operator >=(Drop a, Drop b)
    {
        if (a.coal < b.coal)
            return false;
        if (a.coins < b.coins)
            return false;
        if (a.copper < b.copper)
            return false;
        if (a.gold < b.gold)
            return false;
        if (a.iron < b.iron)
            return false;
        if (a.platinum < b.platinum)
            return false;
        if (a.silver < b.silver)
            return false;
        if (a.stone < b.stone)
            return false;

        return true;
    }

    public static bool operator <(Drop a, Drop b)
    {
        if (a.coal >= b.coal)
            return false;
        if (a.coins >= b.coins)
            return false;
        if (a.copper >= b.copper)
            return false;
        if (a.gold >= b.gold)
            return false;
        if (a.iron >= b.iron)
            return false;
        if (a.platinum >= b.platinum)
            return false;
        if (a.silver >= b.silver)
            return false;
        if (a.stone >= b.stone)
            return false;

        return true;
    }

    public static bool operator <=(Drop a, Drop b)
    {
        if (a.coal > b.coal)
            return false;
        if (a.coins > b.coins)
            return false;
        if (a.copper > b.copper)
            return false;
        if (a.gold > b.gold)
            return false;
        if (a.iron > b.iron)
            return false;
        if (a.platinum > b.platinum)
            return false;
        if (a.silver > b.silver)
            return false;
        if (a.stone > b.stone)
            return false;

        return true;
    }
}