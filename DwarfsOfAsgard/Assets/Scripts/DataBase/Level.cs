﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Level", menuName = "General/Level")]
public class Level : DBEntry
{

    public Vector2 mainChance;
    public Drop.Types mainResource;
    public Drop.Types[] otherResources;
    public Sprite[] blockLook;
    public Transform[] rocks;
    public Sprite[] smallRocks;
    public Sprite background;
    public Sprite foreground;
    public bool blockDetails;
    public Color[] effectColors;
    public Sprite[] trolleys;
    public Sprite[] icons;

    public Drop GetDrop(bigNumber mainQuantity, int levelIndex)
    {
        Drop drop = new Drop();
        if (Formules.CalculateChance(Random.Range(mainChance.x, mainChance.y)))
        {
            Formules.AddToDrop(mainResource, ref drop, Formules.CoinsToResource(mainResource, mainQuantity));
        }
        else
        {
            int rsc = Random.Range(0, otherResources.Length);
            Formules.AddToDrop(otherResources[rsc], ref drop, Formules.CoinsToResource(otherResources[rsc], mainQuantity));
        }

        return drop;
    }

    public Sprite GetSprite(int blockIndex, out bool flip)
    {
        flip = blockIndex % 2 == 0 ? true : false;

        while (true)
        {
            if (blockIndex < blockLook.Length)
            {
                break;
            }
            else
            {
                blockIndex -= blockLook.Length;
            }
        }

        return blockLook[blockIndex];
    }
}