﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dropper : MonoBehaviour
{

    public Transform dropperPrefab;
    [Header("UI")]
    public GameObject window;
    public Text message;
    private enum BonusTypes {Null, Skill, Money, Mana, Discount };
    private BonusTypes[] prevBonusType;
    private BonusTypes bonusType;
    private const float chance = 0.005f;
    private const float minLevel = 6;
    private Transform dropperInstance;
    private bool few;
    public static Dropper instance;

    void Awake()
    {
        instance = this;
        prevBonusType = new BonusTypes[2];
        prevBonusType[0] = prevBonusType[1] = bonusType = BonusTypes.Null;
    }

    void Start()
    {
        InvokeRepeating("Spawn", 180, 240);
    }

    void CalculateBonus()
    {
        prevBonusType[1] = prevBonusType[0];
        prevBonusType[0] = bonusType;
        bonusType = BonusTypes.Money;

        //Hero damage more than helpers damage
        if (Hero.instance.GetPureDamage() > HelpersController.instance.GetDPS() * 2)
        {
            //Can buy hero upgrade
            if (Hero.instance.CanBuy())
            {
                //Two bonuses before was money
                if (prevBonusType[0] == prevBonusType[1] && prevBonusType[0] == BonusTypes.Money)
                {
                    //if hero have any bought skills
                    if (Hero.instance.SkillCount() > 0)
                        bonusType = BonusTypes.Skill;
                    else bonusType = BonusTypes.Discount;
                }
                else
                {
                    bonusType = BonusTypes.Money;
                }
            }
            else
            {
                //Half price for hero and helpers
                if (Hero.instance.HalfPrice() && HelpersController.instance.HalfPrice())
                {
                    //Two bonuses before was money
                    if (prevBonusType[0] == prevBonusType[1] && prevBonusType[0] == BonusTypes.Money)
                    {
                        bonusType = BonusTypes.Discount;
                    }
                    else
                    {
                        bonusType = BonusTypes.Money;
                    }
                }
                else
                {
                    bonusType = BonusTypes.Discount;
                }
            }
        }
        else
        {
            //Can buy helper upgrade
            if (HelpersController.instance.CanBuy())
            {
                //More than 2 skills
                if (Hero.instance.SkillCount() >= 2)
                {
                    //Can cast skill
                    if (Hero.instance.CanCast())
                    {
                        bonusType = BonusTypes.Mana;
                    }
                    else
                    {
                        //Two bonuses before was skills
                        if (prevBonusType[0] == prevBonusType[1] && prevBonusType[0] == BonusTypes.Skill)
                        {
                            bonusType = BonusTypes.Mana;
                        }
                        else
                        {
                            bonusType = BonusTypes.Skill;
                        }
                    }
                }
                else
                {
                    //Two bonuses before was money
                    if (prevBonusType[0] == prevBonusType[1] && prevBonusType[0] == BonusTypes.Money)
                    {
                        bonusType = BonusTypes.Discount;
                    }
                    else
                    {
                        bonusType = BonusTypes.Money;
                    }
                }
            }
            else
            {
                //Half price for hero and helpers
                if (Hero.instance.HalfPrice() && HelpersController.instance.HalfPrice())
                {
                    //Two bonuses before was money
                    if (prevBonusType[0] == prevBonusType[1] && prevBonusType[0] == BonusTypes.Money)
                    {
                        bonusType = BonusTypes.Discount;
                    }
                    else
                    {
                        bonusType = BonusTypes.Money;
                    }
                }
                else
                {
                    bonusType = BonusTypes.Discount;
                }
            }
        }
    }

    public void Spawn()
    {
        if (dropperInstance != null)
            return;

        if (Mining.instance.GetCurrentLevel() <= minLevel)
            return;

        if (Formules.CalculateChance(PerksManager.instance.Get("FewDroppersChance")))
            dropperInstance = Instantiate(dropperPrefab, Vector3.zero, Quaternion.identity);
        dropperInstance = Instantiate(dropperPrefab, Vector3.zero, Quaternion.identity);
    }

    public void ShowWindow()
    {
        if (AdManager.RewardAdLoaded())
        {
            CalculateBonus();
            window.gameObject.SetActive(true);
            switch (bonusType)
            {
                case BonusTypes.Skill:
                    message.text = LanguageManager.ReturnString("DROPPER_MESSAGE_1");
                    break;

                case BonusTypes.Discount:
                    message.text = LanguageManager.ReturnString("DROPPER_MESSAGE_2");
                    break;

                case BonusTypes.Mana:
                    message.text = LanguageManager.ReturnString("DROPPER_MESSAGE_3");
                    break;

                case BonusTypes.Money:
                    message.text = LanguageManager.ReturnString("DROPPER_MESSAGE_4") + " " + MoneyBonus().ToString();
                    break;
            }
        }
        else
        {
            prevBonusType[1] = prevBonusType[0];
            prevBonusType[0] = bonusType;
            bonusType = BonusTypes.Money;
            Mining.instance.AddCoins(MoneyBonus(0.5f));
        }
    }

    public void Close()
    {
        window.gameObject.SetActive(false);
    }

    public void Bonus()
    {
        AdManager.WatchRewarded(OnAdWatched);
    }

    public void OnAdWatched()
    {
        AchievementManager.instance.Increment("ACHIEVEMENT_ADS");
        window.gameObject.SetActive(false);
        switch (bonusType)
        {
            case BonusTypes.Skill:
                Hero.instance.ActivateSkill();
                break;

            case BonusTypes.Discount:
                Mining.instance.SetDiscount();
                break;

            case BonusTypes.Mana:
                Hero.instance.AddMana();
                break;

            case BonusTypes.Money:
                Mining.instance.AddCoins(MoneyBonus());
                break;
        }
    }

    bigNumber MoneyBonus(float mult = 1)
    {
        bigNumber bonus = new bigNumber(Hero.instance.GetCost());

        /*if (bonus < 30)
            bonus = new bigNumber(30);

        for (int i = 0; i < HelpersController.instance.helpers.Length; i++)
        {
            if (!HelpersController.instance.helpers[i].gameObject.activeSelf)
                continue;

            if (bonus < HelpersController.instance.helpers[i].GetCost())
                bonus = new bigNumber(HelpersController.instance.helpers[i].GetCost());
        }*/

        return bonus * 5 * PerksManager.instance.Get("FairyGold") * mult;
    }
}