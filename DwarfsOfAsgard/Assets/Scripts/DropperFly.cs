﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropperFly : MonoBehaviour
{

    public float speed;
    public GameObject eyes;
    public GameObject staticBox;
    public GameObject activeBox;
    public Effect dust;
    private int step;
    private Vector2 xRange;
    private bool done;
    private float dropTime = 1;
    private Vector3 startPos;
    private Vector3 endPos;

    void Awake()
    {
        xRange.x = Formules.FixAspect(-1.8f);
        xRange.y = Formules.FixAspect(1.8f);

        transform.position = new Vector3(3, Random.Range(2f, 3.5f), -5);
    }

    void Update()
    {
        if (step == 0)
        {
            transform.position -= Vector3.right * Time.deltaTime * speed;

            if (!done)
            {
                if (transform.position.x <= xRange.x)
                {
                    Vector3 scale = transform.localScale;
                    scale.x *= -1;
                    transform.localScale = scale;
                    step = 1;
                }
            }
        }
        else
        {
            transform.position += Vector3.right * Time.deltaTime * speed;

            if (!done)
            {
                if (transform.position.x >= xRange.y)
                {
                    Vector3 scale = transform.localScale;
                    scale.x *= -1;
                    transform.localScale = scale;
                    step = 0;
                }
            }
        }

        if (done)
        {
            speed += Time.deltaTime / 3;

            if (dropTime > 0)
            {
                dropTime -= Time.deltaTime;
                activeBox.transform.position = Vector3.Lerp(endPos, startPos, Mathf.Sqrt(Mathf.Clamp(dropTime, 0.001f, 1f)));
            }
        }
    }

    void OnMouseDown()
    {
        if (done)
            return;

        done = true;
        Destroy(gameObject, 15);
        eyes.SetActive(true);
        staticBox.SetActive(false);
        activeBox.SetActive(true);
        activeBox.transform.SetParent(null, true);
        Destroy(activeBox, 3);
        startPos = staticBox.transform.position;
        startPos.z = -5;
        endPos = startPos;
        endPos.y = Random.Range(1.8f, 0.3f);
        Invoke("DustEffect", 1);
        Invoke("ShowWindow", 1.5f);
    }

    void DustEffect()
    {
        if (dust)
            dust.Play();
    }

    void ShowWindow()
    {
        Dropper.instance.ShowWindow();
    }
}