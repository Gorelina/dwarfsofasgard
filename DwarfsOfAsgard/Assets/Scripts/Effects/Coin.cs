﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{

    public SpriteRenderer sprite;
    public float initVelocity;
    private Vector3 velocity;
    private float targetY;
    private bool onGround;
    private float gravity;
    private float time;
    private Vector3 startPos;

    void Start()
    {
        Vector3 pos = transform.position;
        pos += Random.insideUnitSphere;
        pos.z = 0;
        transform.position = pos;

        Vector3 dir = Quaternion.AngleAxis(Random.Range(-30, 30), Vector3.forward) * Vector3.up;
        targetY = Random.Range(0.5f, 1.75f);
        velocity = dir * initVelocity;

        Destroy(gameObject, 2);
    }

    void Update()
    {
        if (!onGround)
        {
            gravity += 9.81f * Time.deltaTime;

            if (transform.position.y <= targetY)
            {
                sprite.sortingOrder = 8 - Mathf.RoundToInt((targetY - 0.5f) * 4);
                gravity = 0;
                velocity = new Vector3();
                onGround = true;
                startPos = transform.position;
                time = 1;
            }
        }
        else
        {
            sprite.color -= new Color(0, 0, 0, Time.deltaTime / 2);

            if (time > 0)
                time -= Time.deltaTime;

            transform.position = Vector3.Lerp(GameUI.instance.coinEndPos, startPos, time);
        }
        transform.position += (velocity - gravity * Vector3.up) * Time.deltaTime;

        transform.eulerAngles += Vector3.forward * velocity.magnitude;
    }

}