﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Effect : MonoBehaviour
{

    public SpriteRenderer spriteRenderer;
    public Sprite[] sprites;
    public Effect nextEffect;
    public int fps = 12;
    public bool destroy;
    public int repeatTimes;
    public bool loop;
    public bool playOnAwake;
    public bool random;
    public bool randomRot;
    public bool shake;
    [Header("Sound")]
    public AudioSource source;
    public bool randomClip;
    public AudioClip[] clips;
    public bool randomPitch;
    public Vector2 pitch;
    private float delay;
    private int frame;
    private bool isPlaying;
    private float editorTime;
    private int repeats;

    void OnEnable()
    {
        spriteRenderer.enabled = false;

        CalculateFPS();
        repeats = repeatTimes;

        if (playOnAwake)
        {
            Play();
            if (random)
                frame = Random.Range(0, sprites.Length - 1);
        }
    }

    void CalculateFPS()
    {
        delay = 1f / fps;
    }

#if UNITY_EDITOR
    void OnDisable()
    {
        StopEditor();
    }

    public void PlayEditor()
    {
        if (!Application.isEditor || Application.isPlaying)
            return;

        UnityEditor.EditorApplication.update += EditorUpdate;

        CalculateFPS();

        isPlaying = true;

        spriteRenderer.enabled = true;
        spriteRenderer.sprite = sprites[0];
        frame = 0;
        editorTime = delay;
    }

    void EditorNextFrame()
    {
        frame++;

        if (frame >= sprites.Length - 1)
        {
            frame = 0;
        }

        spriteRenderer.sprite = sprites[frame];
        editorTime = delay;
    }

    public void StopEditor()
    {
        if (!Application.isEditor || Application.isPlaying)
            return;

        UnityEditor.EditorApplication.update -= EditorUpdate;

        isPlaying = false;

        spriteRenderer.enabled = false;
    }

    void EditorUpdate()
    {
        if (Application.isEditor && !Application.isPlaying && isPlaying)
        {
            editorTime -= 0.0083333f;

            if (editorTime <= 0)
                EditorNextFrame();
        }
    }
#endif

    void PlayAnimation()
    {
        if (shake && CameraScript.instance)
        {
            CameraScript.instance.Shake();
        }

        if (source)
        {
            source.Stop();
            if (randomClip)
            {
                source.clip = clips[Random.Range(0, clips.Length)];
            }
            if (randomPitch)
            {
                source.pitch = Random.Range(pitch.x, pitch.y);
            }
            source.Play();
        }

        if (randomRot)
            transform.localEulerAngles += Vector3.forward * Random.Range(0, 360);
        spriteRenderer.enabled = true;
        spriteRenderer.sprite = sprites[0];
        frame = 0;
        InvokeRepeating("NextFrame", delay, delay);
    }

    void NextFrame()
    {
        frame++;

        if (frame >= sprites.Length - 1)
        {
            CancelInvoke();
            if (loop)
            {
                Play();
            }
            else if (repeats > 0)
            {
                repeats--;
                Play();
            }
            else
            {
                Stop();
            }
        }
        else
        {
            spriteRenderer.sprite = sprites[frame];
        }
    }

    void StopAnimation()
    {
        if (nextEffect)
            nextEffect.Play();

        repeats = repeatTimes;
        spriteRenderer.enabled = false;
        CancelInvoke();
        if (destroy)
        {
            if (!source)
                Destroy(gameObject);
            else Destroy(gameObject, source.clip.length);
        }
    }

    public void Play()
    {
        PlayAnimation();
    }

    public void Stop()
    {
        StopAnimation();
    }
}