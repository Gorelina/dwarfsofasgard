﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectAnchor : MonoBehaviour
{

    public Effect[] effects;

    public void PlayEffect(int i)
    {
        effects[i].Play();
    }

    public void PlayAllEffects()
    {
        foreach (Effect effect in effects)
            effect.Play();
    }
}