﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highlighter : MonoBehaviour
{
    public AnimationCurve alphaCurve;
    [Range(0, 1)]
    public float alphaMultiplier;
    public float timeSpeed = 1;
    public enum HighlighterTypes {Image, SpriteRenderer};
    public HighlighterTypes highlighterType;
    private Image image;
    private SpriteRenderer spriteRenderer;
    private Color startColor;

    void Awake()
    {
        if (highlighterType == HighlighterTypes.Image)
        {
            image = GetComponent<Image>();
            startColor = image.color;
        }
        else
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            startColor = spriteRenderer.color;
        }
    }

    void Update()
    {
        Color newColor = startColor;
        newColor.a = alphaCurve.Evaluate(Time.time * timeSpeed) * alphaMultiplier;

        if (highlighterType == HighlighterTypes.Image)
            image.color = newColor;
        else spriteRenderer.color = newColor;
    }
}