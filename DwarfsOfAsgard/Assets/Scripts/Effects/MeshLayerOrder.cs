﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshLayerOrder : MonoBehaviour
{

    public string layer;
    public int order;
    public MeshRenderer meshRenderer;

    void Start()
    {
        meshRenderer.sortingLayerName = layer;
        meshRenderer.sortingOrder = order;
    }

}