﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Formules
{

    private static Dictionary<int, bigNumber> calculatedLevels = new Dictionary<int, bigNumber>()
    {
        { -1, new bigNumber(20) }
    };
    private static Dictionary<int, bigNumber> calculatedBosses = new Dictionary<int, bigNumber>()
    {
        { -2, new bigNumber(20) },
        { -1, new bigNumber(20) }
    };
    private static Dictionary<int, bigNumber> calculatedBossResources = new Dictionary<int, bigNumber>();
    public const float idealAspect = 0.46f;

    public static float FixAspect(float value)
    {
        float aspect = (float)Screen.width / (float)Screen.height;
        return (aspect * value) / idealAspect;
    }

    private static bool CalculateChanceClear(int chance)
    {
        return Random.Range(1, 101) <= chance;
    }

    public static bool CalculateChance(int chance)
    {
        return Random.Range(1, 101) <= chance * PerksManager.instance.Get("AllChances");
    }

    public static bool CalculateChance(float chance)
    {
        return Random.Range(0.00001f, 100f) <= chance * PerksManager.instance.Get("AllChances");
    }

    public static bigNumber GetUpgradePrice(int level)
    {
        return new bigNumber(10 + level * 10);
    }

    public static void AddToDrop(Drop.Types type, ref Drop drop, bigNumber value)
    {
        switch (type)
        {
            case Drop.Types.coal:
                drop.coal += value;
                break;

            case Drop.Types.copper:
                drop.copper += value;
                break;

            case Drop.Types.gold:
                drop.gold += value;
                break;

            case Drop.Types.iron:
                drop.iron += value;
                break;

            case Drop.Types.platinum:
                drop.platinum += value;
                break;

            case Drop.Types.silver:
                drop.silver += value;
                break;

            case Drop.Types.stone:
                drop.stone += value;
                break;
        }
    }

    public static void GetChestNumbers(int levelIndex, out bigNumber hp, out bigNumber resources)
    {
        bigNumber prevHP, prevRSC, currHP, currRSC;
        GetLevelNumbers(levelIndex - 1, 8, out prevHP, out prevRSC);
        GetLevelNumbers(levelIndex, 8, out currHP, out currRSC);
        hp = (prevHP + currHP) / 2;
        resources = (prevRSC + currRSC) / 2;
    }

    public static void GetLevelNumbers(int levelIndex, int blockIndex, out bigNumber hp, out bigNumber resources)
    {
        CalculateLevelNumbers(levelIndex, blockIndex, out hp, out resources);
    }

    static void CalculateLevelNumbers(int levelIndex, int blockIndex, out bigNumber hp, out bigNumber resources)
    {
        bigNumber prevHP = hp = new bigNumber(calculatedLevels[-1]);

        if (blockIndex < 8)
        {
            //Resource
            int chance = 0;
            if (levelIndex < 1)
            {
                if (blockIndex < 6)
                {
                    chance = 0;
                    resources = new bigNumber(0);
                }
                else
                {
                    chance = 30;
                    resources = new bigNumber(1);
                }
            }
            else
            {
                chance = 10;
                if (blockIndex > 2)
                    chance = 20;
                if (blockIndex > 4)
                    chance = 30;

                resources = bigNumber.RandomRange(new bigNumber((levelIndex + 1) / 3), new bigNumber((levelIndex * 8 + blockIndex) / 2));
            }

            if (!CalculateChanceClear(chance))
                resources.ToZero();

            //HP
            float mult = 0.8f + (blockIndex / 10f);
            if (!calculatedLevels.ContainsKey(levelIndex - 1))
            {
                for (int i = 0; i < levelIndex; i++)
                {
                    prevHP = bigNumber.RandomRange(prevHP * 0.8f, prevHP) * 1.5f;

                    if (!calculatedLevels.ContainsKey(i))
                        calculatedLevels.Add(i, prevHP);
                }
            }

            hp = bigNumber.RandomRange(calculatedLevels[levelIndex - 1] * 0.8f, calculatedLevels[levelIndex - 1]) * mult;

            if (blockIndex == 7 && !calculatedLevels.ContainsKey(levelIndex))
            {
                calculatedLevels.Add(levelIndex, hp);
            }

            while (calculatedLevels.Count > 30)
            {
                calculatedLevels.Remove(calculatedLevels.ElementAt(1).Key);
            }
        }
        else
        {
            //Resources
            int li = levelIndex;
            int biom = 0;
            float mult = 0.6f;
            while(li > 4)
            {
                li -= 5;
                biom++;
            }

            switch(li)
            {
                case 1:
                    mult = 0.7f;
                    break;

                case 2:
                    mult = 0.9f;
                    break;

                case 3:
                    mult = 1.1f;
                    break;

                case 4:
                    mult = 1.3f;
                    break;
            }

            if (biom < 1)
            {
                resources = new bigNumber(10 * mult);
            }
            else
            {
                if (!calculatedBossResources.ContainsKey(biom - 1))
                {
                    bigNumber prevRSC = new bigNumber();
                    for (int i = 0; i < biom; i++)
                    {
                        if (calculatedBossResources.ContainsKey(i))
                            continue;
                        else
                        {
                            if (i < 1)
                                prevRSC = new bigNumber(10 * 1.3f);
                            else prevRSC = new bigNumber(calculatedBossResources[i - 1] * 2 * 1.3f);
                            calculatedBossResources.Add(i, prevRSC);
                        }
                    }
                }

                resources = new bigNumber(calculatedBossResources[biom - 1] * 2 * mult);
            }

            if (li == 4 && !calculatedBossResources.ContainsKey(biom))
            {
                calculatedBossResources.Add(biom, resources);
            }

            //HP
            float mult1 = 1;
            if (levelIndex == 0)
                mult1 = 2;
            float mult2 = 0.75f + levelIndex * 0.05f;

            if (!calculatedBosses.ContainsKey(levelIndex - 1) || !calculatedBosses.ContainsKey(levelIndex - 2))
            {
                for (int i = 0; i < levelIndex; i++)
                {
                    prevHP = calculatedBosses[i - 2] * mult2 + calculatedBosses[i - 1] * mult1;

                    if (!calculatedBosses.ContainsKey(i))
                        calculatedBosses.Add(i, prevHP);
                }
            }

            hp = calculatedBosses[levelIndex - 2] * mult1 + calculatedBosses[levelIndex - 1] * mult2;

            if (!calculatedBosses.ContainsKey(levelIndex))
                calculatedBosses.Add(levelIndex, hp);

            while (calculatedBosses.Count > 30)
            {
                calculatedBosses.Remove(calculatedBosses.ElementAt(2).Key);
            }
        }
    }

    public static bigNumber ResourceToCoins(Drop.Types type, bigNumber quantity)
    {
        return quantity * Mathf.Pow(10, (int)type);
    }

    public static bigNumber ResourceToCoins(int type, bigNumber quantity)
    {
        return quantity * Mathf.Pow(10, type);
    }

    public static bigNumber CoinsToResource(Drop.Types type, bigNumber coins)
    {
        return coins / Mathf.Pow(10, (int)type);
    }

    public static Drop CoinsToResources(bigNumber coins)
    {
        Drop drop = new Drop();
        for (int i = 6; i >= 0; i--)
        {
            if (coins < Mathf.Pow(10, i))
                continue;

            bigNumber q = coins / Mathf.Pow(10, i);
            switch (i)
            {
                case 0:
                    drop.stone = q;
                    break;

                case 1:
                    drop.coal = q;
                    break;

                case 2:
                    drop.copper = q;
                    break;

                case 3:
                    drop.iron = q;
                    break;

                case 4:
                    drop.silver = q;
                    break;

                case 5:
                    drop.gold = q;
                    break;

                case 6:
                    drop.platinum = q;
                    break;
            }
        }

        return drop;
    }
}