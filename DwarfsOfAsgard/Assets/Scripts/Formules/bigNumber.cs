﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class bigNumber
{

    public float number;
    public int prefixIndex;
    private string prefix;
    private string textFormat;
    private bool accurate;

    public bigNumber()
    {
        number = 0;
        prefixIndex = 0;
        prefix = "";
        textFormat = "0";
    }

    public bigNumber(float _number)
    {
        if (_number < 0)
        {
            number = 0;
            prefix = "";
            prefixIndex = 0;
            return;
        }

        number = _number;
        Update();
    }

    public bigNumber(bigNumber b)
    {
        number = b.number;
        prefixIndex = b.prefixIndex;
        prefix = b.prefix;
        accurate = b.accurate;
        Update();
    }

    public bigNumber(string s)
    {
        s = s.Replace("e", "E");
        int eIndex = s.IndexOf('E');
        if (eIndex > 0)
        {
            number = float.Parse(s.Substring(0, eIndex), System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            int factor = int.Parse(s.Substring(eIndex + 2));
            prefixIndex = factor / 3;

            int pow = factor % 3;
            if (pow > 0)
                number *= Mathf.Pow(10, pow);
        }
        else
        {
            number = float.Parse(s, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            prefixIndex = 0;
        }
        Update();
    }

    public void SetAccurate()
    {
        accurate = true;
        UpdatePrefix();
    }

    public void Update()
    {
        if (number < 0.01f && prefixIndex <= 0)
        {
            number = 0;
            prefixIndex = 0;
            prefix = "";
        }

        while (true)
        {
            if (number >= 1000)
            {
                number /= 1000;
                PrefixInc();
            }
            else if(number < 1f && prefixIndex > 0)
            {
                number *= 1000;
                PrefixDec();
            }
            else if (prefixIndex < 0)
            {
                number /= 1000;
                PrefixInc();
            }
            else
            {
                UpdatePrefix();
                break;
            }
        }

        if (accurate && prefixIndex == 0 && number > 0 && number < 1)
            textFormat = "<1";
        else textFormat = number.ToString(prefixIndex > 0 ? "F2" : "F0") + prefix;
    }

    static string toBase26(int i)
    {
        if (i == 0)
            return "";

        i--;

        return toBase26(i / 26) + (char)('a' + i % 26);
    }

    void PrefixInc()
    {
        prefixIndex++;
        UpdatePrefix();
    }

    void PrefixDec()
    {
        if (prefixIndex == 0)
            return;

        prefixIndex--;
        UpdatePrefix();
    }

    public static string GetPrefix(int index)
    {
        string result = "";
        if (index > 0)
        {
            switch (index)
            {
                case 1:
                    result = "K";
                    break;

                case 2:
                    result = "M";
                    break;

                case 3:
                    result = "B";
                    break;

                case 4:
                    result = "T";
                    break;

                default:
                    result = toBase26(index - 4);
                    break;
            }
        }

        return result;
    }

    void UpdatePrefix()
    {
        prefix = GetPrefix(prefixIndex);
        if (accurate && prefixIndex == 0 && number > 0 && number < 1)
            textFormat = "<1";
        else textFormat = number.ToString(prefixIndex > 0 ? "F2" : "F0") + prefix;
    }

    public static float NormalizeTo(bigNumber a, bigNumber b)
    {
        float result;
        int factor = a.prefixIndex - b.prefixIndex;
        if (factor == 0)
        {
            result = b.number;
        }
        else if (factor < 0)
        {
            result = b.number * Mathf.Pow(1000, Mathf.Abs(factor));
        }
        else
        {
            result = b.number / Mathf.Pow(1000, factor);
        }

        return result;
    }

    public static bigNumber operator+ (bigNumber a, bigNumber b)
    {
        bigNumber result = new bigNumber(a);

        float newB = NormalizeTo(a, b);

        result.number += newB;
        result.Update();

        return result;
    }

    public static bigNumber operator+ (bigNumber a, int b)
    {
        if (b == 0)
            return a;

        bigNumber result = new bigNumber(a);

        float newb = b;
        if (a.prefixIndex > 0)
            newb = (float)b / Mathf.Pow(1000f, a.prefixIndex);

        if (newb <= 0.001)
            return result;

        result.number += newb;
        result.Update();

        return result;
    }

    public static bigNumber operator+ (bigNumber a, float b)
    {
        if (b == 0)
            return a;

        bigNumber result = new bigNumber(a);

        float newb = b;
        if (a.prefixIndex > 0)
            newb = b / Mathf.Pow(1000f, a.prefixIndex);

        if (newb <= 0.001)
            return result;

        result.number += newb;
        result.Update();

        return result;
    }

    public static bigNumber operator- (bigNumber a, bigNumber b)
    {
        bigNumber result = new bigNumber(a);

        float newB = NormalizeTo(a, b);

        result.number -= newB;
        result.Update();

        return result;
    }

    public static bigNumber operator- (bigNumber a, int b)
    {
        if (b == 0)
            return a;

        bigNumber result = new bigNumber(a);

        float newb = b;
        if (a.prefixIndex > 0)
            newb = (float)b / Mathf.Pow(1000f, a.prefixIndex);

        if (newb <= 0.001)
            return result;

        result.number -= newb;
        result.Update();

        return result;
    }

    public static bigNumber operator- (bigNumber a, float b)
    {
        if (b == 0)
            return a;

        bigNumber result = new bigNumber(a);

        float newb = b;
        if (a.prefixIndex > 0)
            newb = b / Mathf.Pow(1000f, a.prefixIndex);

        if (newb <= 0.001)
            return result;

        result.number -= newb;
        result.Update();

        return result;
    }

    public static bigNumber operator* (bigNumber a, bigNumber b)
    {
        bigNumber result = new bigNumber(a);
        result.number *= b.number;
        result.prefixIndex += b.prefixIndex;
        result.Update();

        return result;
    }

    public static bigNumber operator* (bigNumber a, int b)
    {
        if (b == 1)
            return a;

        bigNumber result = new bigNumber(a);
        result.number *= b;
        result.Update();

        return result;
    }

    public static bigNumber operator* (bigNumber a, float b)
    {
        if (b == 1)
            return a;

        bigNumber result = new bigNumber(a);
        result.number *= b;
        result.Update();

        return result;
    }

    public static bigNumber operator/ (bigNumber a, bigNumber b)
    {
        bigNumber result = new bigNumber(a);
        result.number /= b.number;
        result.prefixIndex -= b.prefixIndex;
        result.Update();

        return result;
    }

    public static bigNumber operator/ (bigNumber a, int b)
    {
        if (b == 1)
            return a;

        bigNumber result = new bigNumber(a);
        result.number /= b;
        result.Update();

        return result;
    }

    public static bigNumber operator/ (bigNumber a, float b)
    {
        if (b == 1)
            return a;

        bigNumber result = new bigNumber(a);
        result.number /= b;
        result.Update();

        return result;
    }

    public static bool operator== (bigNumber a, bigNumber b)
    {
        if (a.prefixIndex != b.prefixIndex)
            return false;
        else if (a.prefixIndex == 0)
        {
            return Mathf.Round(a.number) == Mathf.Round(b.number);
        }
        else return a.number == b.number;
    }

    public static bool operator== (bigNumber a, float b)
    {
        if (a.prefixIndex != 0)
            return false;
        else return Mathf.Round(a.number) == b;
    }

    public static bool operator!= (bigNumber a, bigNumber b)
    {
        if (a.prefixIndex != b.prefixIndex)
            return true;
        else if (a.prefixIndex == 0)
        {
            return Mathf.Round(a.number) != Mathf.Round(b.number);
        }
        else return a.number != b.number;
    }

    public static bool operator!= (bigNumber a, float b)
    {
        if (a.prefixIndex != 0)
            return true;
        else return Mathf.Round(a.number) != b;
    }

    public static bool operator> (bigNumber a, bigNumber b)
    {
        if (a.prefixIndex > b.prefixIndex)
            return true;
        else if (a.prefixIndex == b.prefixIndex)
        {
            if (a.prefixIndex == 0)
            {
                return Mathf.Round(a.number) > Mathf.Round(b.number);
            }
            else return a.number > b.number;
        }

        return false;
    }

    public static bool operator> (bigNumber a, float b)
    {
        if (a.prefixIndex > 0)
            return true;
        else if (a.prefixIndex == 0)
        {
            return Mathf.Round(a.number) > b;
        }

        return false;
    }

    public static bool operator>= (bigNumber a, bigNumber b)
    {
        if (a.prefixIndex > b.prefixIndex)
            return true;
        else if (a.prefixIndex == b.prefixIndex)
        {
            if (a.prefixIndex == 0)
            {
                return Mathf.Round(a.number) >= Mathf.Round(b.number);
            }
            else return a.number >= b.number;
        }

        return false;
    }

    public static bool operator>= (bigNumber a, float b)
    {
        if (a.prefixIndex > 0)
            return true;
        else if (a.prefixIndex == 0)
        {
            return Mathf.Round(a.number) >= b;
        }

        return false;
    }

    public static bool operator< (bigNumber a, bigNumber b)
    {
        if (a.prefixIndex < b.prefixIndex)
            return true;
        else if (a.prefixIndex == b.prefixIndex)
        {
            if (a.prefixIndex == 0)
            {
                return Mathf.Round(a.number) < Mathf.Round(b.number);
            }
            else return a.number < b.number;
        }

        return false;
    }

    public static bool operator< (bigNumber a, float b)
    {
        if (a.prefixIndex < 0)
            return true;
        else if (a.prefixIndex == 0)
        {
            return Mathf.Round(a.number) < b;
        }

        return false;
    }

    public static bool operator<= (bigNumber a, bigNumber b)
    {
        if (a.prefixIndex < b.prefixIndex)
            return true;
        else if (a.prefixIndex == b.prefixIndex)
        {
            if (a.prefixIndex == 0)
            {
                return Mathf.Round(a.number) <= Mathf.Round(b.number);
            }
            else return a.number <= b.number;
        }

        return false;
    }

    public static bool operator<= (bigNumber a, float b)
    {
        if (a.prefixIndex < 0)
            return true;
        else if (a.prefixIndex == 0)
        {
            return Mathf.Round(a.number) <= b;
        }

        return false;
    }

    public override string ToString()
    {
        return textFormat;
    }

    public string GetString()
    {
        return number.ToString("F2").Replace(',', '.') + "E+" + prefixIndex * 3;
    }

    public static bigNumber RandomRange(bigNumber a, bigNumber b)
    {
        bigNumber result = new bigNumber();
        result.number = Random.Range(NormalizeTo(b, a), b.number);
        result.prefixIndex = b.prefixIndex;
        result.Update();

        return result;
    }

    public static bigNumber Lerp(bigNumber a, bigNumber b, float t)
    {
        bigNumber result = new bigNumber(a);
        result.number = Mathf.Lerp(result.number, b.number, t);
        result.Update();
        return result;
    }

    public static bigNumber Lerp(bigNumber a, float b, float t)
    {
        bigNumber result = new bigNumber(a);
        result.number = Mathf.Lerp(result.number, b, t);
        result.Update();
        return result;
    }

    public void ToZero()
    {
        number = 0;
        prefixIndex = 0;
        prefix = "";
        textFormat = "0";
    }

    public override bool Equals(object obj)
    {
        if (!(obj is bigNumber))
            return false;

        bigNumber p = (bigNumber)obj;
        return number == p.number && prefixIndex == p.prefixIndex;
    }

    public override int GetHashCode()
    {
        return (int)number + prefixIndex;
    }
}