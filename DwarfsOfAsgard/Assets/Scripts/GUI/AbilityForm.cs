﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityForm : MonoBehaviour
{
    public int id;
    public Image icon;
    public Text nameText;
    public Text descText;
    public GameObject buyLock;
    public Text cost;
    public Button buyButton;

    public void Set(int _id)
    {
        id = _id;
        buyButton.onClick.AddListener(delegate { Hero.instance.ActivateAbility(id); });
    }
}