﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementForm : MonoBehaviour
{
    public int id;
    public Image icon;
    public Text nameText;
    public GameObject[] stage;
    public Text progressText;
    public Slider progress;
    public StringRead action;
    public Text reward;
    public Image collectButtonSprite;
    public Button collectButton;

    public void Set(int _id)
    {
        id = _id;
        collectButton.onClick.AddListener(delegate { AchievementManager.instance.CollectReward(id); });
    }
}
