﻿public class Advicer
{

    private static int[] mainAdvices = {1, 2, 17, 20, 21, 24, 25, 26, 28, 29, 41, 42, 46, 47, 50, 51, 52 };
    private static int[] otherAdvices = {3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 23, 27, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 43, 44, 45, 48, 49};
    private static int mainAdviceIndex = 0;
    private static int otherAdviceIndex = 0;
    private static bool main = true;

    public static string GetAdvice()
    {
        string result;
        if (main)
        {
            result = "ADVICE_" + mainAdvices[mainAdviceIndex];
            mainAdviceIndex++;
            if (mainAdviceIndex >= mainAdvices.Length)
                mainAdviceIndex = 0;
            main = !main;
        }
        else
        {
            result = "ADVICE_" + otherAdvices[otherAdviceIndex];
            otherAdviceIndex++;
            if (otherAdviceIndex >= otherAdvices.Length)
                otherAdviceIndex = 0;
            main = !main;
        }

        return result;
    }

}