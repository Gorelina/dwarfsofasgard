﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementSync : MonoBehaviour
{
    public Transform target;

    void Awake()
    {
        transform.position = Camera.main.WorldToScreenPoint(target.position);
    }
}