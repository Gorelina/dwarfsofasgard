﻿using UnityEngine;
using UnityEngine.UI;

public class HelperForm : MonoBehaviour
{

    [System.Serializable]
    public class Perk
    {
        public Image icon;
        public GameObject locked;
        public Text level;
    }

    new public Text name;
    public GameObject melee;
    public GameObject ranged;
    public GameObject flying;
    public GameObject[] professions;
    public Image icon;
    public Text damage;
    public Text cost;
    public Text damageBonus;
    public Text level;
    public Perk[] perks;
    public GameObject locked;
    public Button levelUpButton;
    public Button windowButton;
    private int id;

    public void Set(int _id)
    {
        id = _id;
        levelUpButton.gameObject.name = "LevelUp_" + HelpersController.instance.helpers[_id].data.name;
        levelUpButton.onClick.AddListener(delegate { HelpersController.instance.LevelUp(_id); });
        windowButton.onClick.AddListener(delegate { GameUI.instance.OpenHelperWindow(_id); });
        for (int i = 0; i < professions.Length; i++)
            professions[i].SetActive((int)HelpersController.instance.helpers[_id].data.fraction == i);
    }

}