﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageUI : MonoBehaviour
{
    new public string name;
    public Text text;
    public Image icon;
    public Sprite read;
    public RectTransform layout;
    private bool opened;

    void Start()
    {
        text.text = LanguageManager.ReturnString(name + "_HEADER");
    }

    public void Open()
    {
        opened = !opened;
        text.text = opened ? LanguageManager.ReturnString(name + "_CONTENT") : LanguageManager.ReturnString(name + "_HEADER");
        icon.sprite = read;
        LayoutRebuilder.ForceRebuildLayoutImmediate(layout);
    }
}