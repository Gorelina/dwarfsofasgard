﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupData
{
    public string text;
    public Color color;
    public float size;

    public PopupData()
    {
        text = "";
        color = Color.white;
        size = 1;
    }

    public PopupData(string _text, Color _color)
    {
        text = _text;
        color = _color;
        size = 1;
    }

    public PopupData(string _text, Color _color, float _size)
    {
        text = _text;
        color = _color;
        size = _size;
    }
}

public class PopupUI : MonoBehaviour
{
    public Text text;
    private float time = 0.5f;

    void Update()
    {
        transform.position += Vector3.up * 220 * Time.unscaledDeltaTime;
        time -= Time.unscaledDeltaTime;
        if (time <= 0)
            Destroy(gameObject);
    }

    public void Set(PopupData data)
    {
        text.text = data.text;
        text.color = data.color;
        if (data.size != 1)
            text.fontSize = Mathf.RoundToInt(text.fontSize * data.size);
    }
}