﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour
{

    public int id;
    public GameObject locked;
    public Image icon;
    public Image cooldownTimer;
    public Text manaCost;

    void Start()
    {
        icon.sprite = Hero.instance.skills[id].littleIcon.sprite;
        manaCost.text = Hero.instance.skillInstances[id].GetManaCost().ToString();
        Hero.instance.skillInstances[id].onUpgrade += UpdateInfo;
    }

    void OnDisable()
    {
        Hero.instance.skillInstances[id].onUpgrade -= UpdateInfo;
    }

    void UpdateInfo()
    {
        manaCost.text = Hero.instance.skillInstances[id].GetManaCost().ToString();
    }

    void Update()
    {
        if (Hero.instance.skillInstances[id].bought)
        {
            if (!icon.gameObject.activeSelf)
            {
                icon.gameObject.SetActive(true);
                manaCost.gameObject.SetActive(true);
            }

            if (Hero.instance.skillInstances[id].casting)
                cooldownTimer.fillAmount = Hero.instance.skillInstances[id].castingTime / Hero.instance.skillInstances[id].GetCastTime();
            else if (Hero.instance.skillInstances[id].cooldown > 0)
                cooldownTimer.fillAmount = 1 - Hero.instance.skillInstances[id].cooldown / Hero.instance.skillInstances[id].GetCooldownTime();
            else cooldownTimer.fillAmount = 0;

            locked.SetActive(!Hero.instance.skillInstances[id].Possible(Hero.instance.Mana));
        }
        else
        {
            if (icon.gameObject.activeSelf)
            {
                icon.gameObject.SetActive(false);
                manaCost.gameObject.SetActive(false);
                locked.SetActive(false);
            }
        }
    }

    public void ActivateSkill()
    {
        if (Hero.instance.busy)
            return;

        Hero.instance.ActivateSkill(id);
        GameUI.instance.soundContainer.PlaySound("click");
    }
}