﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillForm : MonoBehaviour
{

    public int id;
    public Image icon;
    public Text nameText;
    public Text descText;
    public GameObject locked;
    public GameObject buyLock;
    public Text cost;
    public Text manaCost;
    public Text buttonContent;
    public Button buyButton;

    public void Set(int _id)
    {
        id = _id;
        buyButton.onClick.AddListener(delegate { Hero.instance.BuySkill(id); });
    }
}