﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tween : MonoBehaviour
{

    [Header("LookUp")]
    public Tween lookUp;
    public bool posLookUp;
    public bool scaleLookUp;
    public bool alphaLookUp;
    [Space]
    public Graphic graphic;
    public float time;
    public bool hold;
    public bool loop;
    public bool playOnAwake;
    [Header("Pos")]
    public bool posAnim = true;
    public Vector3 targetPos;
    [Header("Rot")]
    public bool rotAnim = false;
    public Vector3 targetRot;
    [Header("Scale")]
    public bool scaleAnim = false;
    public AnimationCurve scale;
    [Header("Alpha")]
    public bool alphaAnim = true;
    public AnimationCurve alpha;
    private Vector3 startPos;
    private Vector3 startRot;
    private Vector3 startScale;
    private Color startColor;
    private float t;
    private float td;
    [System.NonSerialized]
    public bool isPlaying;

    void Awake()
    {
        startPos = transform.localPosition;
        startRot = transform.localEulerAngles;
        startScale = transform.localScale;
        startColor = graphic.color;

        if (playOnAwake)
            Play();
    }

    void Update()
    {
        if (isPlaying)
        {
            if (t < time)
            {
                t += Time.deltaTime;
                td = t / time;

                if (posAnim)
                {
                    transform.localPosition = Vector3.Lerp(startPos, targetPos, td);
                }

                if (rotAnim)
                {
                    transform.localEulerAngles = Vector3.Lerp(startRot, targetRot, td);
                }

                if (scaleAnim)
                {
                    transform.localScale = startScale * scale.Evaluate(td);
                }

                if (alphaAnim)
                {
                    Color col = startColor;
                    col.a = alpha.Evaluate(td);
                    graphic.color = col;
                }
            }
            else
            {
                if (loop)
                {
                    t = 0;
                }
                else
                {
                    if (!hold)
                    {
                        Stop();
                    }
                    else
                    {
                        t = 0;
                        isPlaying = false;
                    }
                }
            }
        }

        if (lookUp && lookUp.isPlaying)
        {
            if (posLookUp)
                transform.localPosition = lookUp.transform.localPosition;

            if (scaleLookUp)
                transform.localScale = startScale * lookUp.scale.Evaluate(lookUp.td);

            if (alphaLookUp)
            {
                Color col = startColor;
                col.a = lookUp.graphic.color.a;
                graphic.color = col;
            }
        }
    }

    public void Play()
    {
        Stop();
        isPlaying = true;
    }

    public void Stop()
    {
        if (posAnim)
        {
            transform.localPosition = startPos;
        }
        if (rotAnim)
        {
            transform.localEulerAngles = startRot;
        }
        if (scaleAnim)
        {
            transform.localScale = startScale;
        }
        if (alphaAnim)
        {
            graphic.color = startColor;
        }

        t = 0;
        isPlaying = false;
    }

    public void ToEnd()
    {
        if (posAnim)
        {
            transform.localPosition = targetPos;
        }
        if (rotAnim)
        {
            transform.localEulerAngles = targetRot;
        }
        if (scaleAnim)
        {
            transform.localScale = startScale * scale.Evaluate(1);
        }
        if (alphaAnim)
        {
            Color col = startColor;
            col.a = alpha.Evaluate(1);
            graphic.color = col;
        }
    }

}