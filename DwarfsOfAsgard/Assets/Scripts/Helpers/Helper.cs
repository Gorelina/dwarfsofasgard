﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helper : Character
{

    public HelperData data;
    [System.NonSerialized]
    public int id;
    public Animator animator;
    public Effect punchEffect;
    [System.NonSerialized]
    public bool onSkill;
    [System.NonSerialized]
    public int skillIndex;
    private HelperSkill[] skills;
    [System.NonSerialized]
    public bool[] skillsActive;

    void Awake()
    {
        baseCost = new bigNumber(data.cost);
        baseDamage = new bigNumber(data.damage);
        Init(false);
        skills = DataBase.instance.GetHelperSkill(data.name);
        skillsActive = new bool[skills.Length];
    }

    public override void LevelUp(int levelBonus = 1)
    {
        for (int i = 0; i < skills.Length; i++)
        {
            if (skillsActive[i])
                continue;

            if (GetLevel() >= skills[i].minLevel)
            {
                skillsActive[i] = true;
                if (skills[i].effect == "AllGold")
                    Mining.instance.MultiplyCoins(skills[i].multiplier);
                onSkill = false;
                CheckSkills();
                return;
            }
        }

        base.LevelUp(levelBonus);
        CheckSkills();
    }

    public override void SetLevel(int level)
    {
        base.SetLevel(level);
        CheckSkills();
    }

    void CheckSkills()
    {
        for (int i = 0; i < skills.Length; i++)
        {
            if (skillsActive[i])
                continue;

            if (GetLevel() >= skills[i].minLevel)
            {
                onSkill = true;
                skillIndex = i;
                return;
            }
        }
    }

    public override bigNumber GetCost()
    {
        float multiplier = PerksManager.instance.Get("UpgradeCost");
        if (onSkill)
        {
            multiplier *= 10;
            return base.GetLevelCost(skills[skillIndex].minLevel) * multiplier;
        }
        else return base.GetCost() * multiplier;
    }

    public override bigNumber GetCost(int levelBonus)
    {
        float multiplier = PerksManager.instance.Get("UpgradeCost");
        if (onSkill)
        {
            multiplier *= 10;
            return base.GetLevelCost(skills[skillIndex].minLevel) * multiplier;
        }
        else return base.GetCost(levelBonus) * multiplier;
    }

    public override bigNumber GetFinalDamage()
    {
        return base.GetFinalDamage();
    }

    public override bigNumber GetActualDamage(out bool critical)
    {
        critical = false;
        bigNumber damage = new bigNumber(base.GetPureDamage());
        damage *= PerksManager.instance.Get("AllHelperDamage");

        if (data.type == HelperData.Types.Melee)
            damage *= PerksManager.instance.Get("MeleeHelperDamage");
        else damage *= PerksManager.instance.Get("RangedHelperDamage");

        if (data.flying == true)
        {
            damage *= PerksManager.instance.Get("AirDamage");
        }
        else
        {
            damage *= PerksManager.instance.Get("GroundDamage");
        }

        if (Hero.instance.skillInstances[4].active)
        {
            damage *= Hero.instance.skillInstances[4].GetValue();
            damage *= PerksManager.instance.Get("HelpersDamageWhileWarCry");
        }

        return damage;
    }

    public HelperSkill GetSkill(int index)
    {
        return skills[index];
    }

    public HelperSkill[] GetSkills()
    {
        return skills;
    }

    public bool GetSkillActive(int index)
    {
        return skillsActive[index];
    }

    public void SetSkills(int skillIndex)
    {
        if (skillIndex <= 0)
            return;

        skillIndex--;
        for (int i = 0; i <= skillIndex; i++)
        {
            skillsActive[i] = true;
            if (skills[i].effect == "AllGold")
                Mining.instance.MultiplyCoins(skills[i].multiplier);
        }
    }

    public void ShowAttack()
    {
        animator.SetTrigger("Attack");
    }
    
    public void ShowEffect()
    {
        if (punchEffect)
            punchEffect.Play();

        Mining.instance.Damage(GetActualDamage(out bool critical), DamageSource.Helper, data.type == HelperData.Types.Melee ? DamageTypes.Melee : DamageTypes.Range);
    }

    public void ProcessPerks()
    {
        PerksManager pm = PerksManager.instance;
        float multiplier = 1;
        switch (data.fraction)
        {
            case HelperData.Fractions.Engineers:
                multiplier = pm.Get("EngineerSkills");
                break;

            case HelperData.Fractions.Gunners:
                multiplier = pm.Get("GunnersSkills");
                break;

            case HelperData.Fractions.Stoners:
                multiplier = pm.Get("StonersSkills");
                break;
            case HelperData.Fractions.Supporters:
                multiplier = pm.Get("SupportersSkills");
                break;
        }
        for (int i = 0; i < skills.Length; i++)
        {
            if (skillsActive[i])
                pm.Set(skills[i].effect, skills[i].multiplier * multiplier);
        }
    }
}