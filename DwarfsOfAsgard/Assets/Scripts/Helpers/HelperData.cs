﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New HelperData", menuName = "General/HelperData")]
public class HelperData : ScriptableObject
{

    new public string name;
    public SpriteImage icon;
    public SpriteImage photo;
    public enum Types {Melee, Ranged};
    public Types type;
    public enum Fractions {Engineers, Gunners, Stoners, Supporters };
    public Fractions fraction;
    public bigNumber cost;
    public bigNumber damage;
    public bool flying;    

}

public class HelperSkill
{
    public string name;
    public string effect;
    public float multiplier;
    public int minLevel;

    public HelperSkill(string _name, string _effect, float _multiplier, int _minLevel)
    {
        name = _name;
        effect = _effect;
        multiplier = _multiplier;
        minLevel = _minLevel;
    }

    public HelperSkill(DataBase.SkillInfo skillInfo)
    {
        name = skillInfo.name;
        effect = skillInfo.bonusType;
        multiplier = skillInfo.magnitude;
        minLevel = skillInfo.requiredLevel;
    }
}