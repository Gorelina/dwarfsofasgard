﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpersController : MonoBehaviour
{

    public Helper[] helpers;
    public static HelpersController instance;

    void Awake()
    {
        instance = this;
        InvokeRepeating("Attack", 2, 2);
        InvokeRepeating("Speech", 120, 240);
    }

    void Start()
    {
        foreach (Helper helper in helpers)
            helper.gameObject.SetActive(false);
    }

    public int ActiveHelpersCount()
    {
        int c = 0;
        for (int i = 0; i < helpers.Length; i++)
        {
            if (helpers[i].gameObject.activeSelf)
                c++;
        }

        return c;
    }

    List<Helper> GetActiveHelpers()
    {
        List<Helper> ah = new List<Helper>();
        for (int i = 0; i < helpers.Length; i++)
        {
            if (helpers[i].gameObject.activeSelf)
                ah.Add(helpers[i]);
        }

        return ah;
    }

    void Speech()
    {
        if (Random.Range(0, 3) == 1 || ActiveHelpersCount() <= 0)
            return;

        List<Helper> activeHelpers = GetActiveHelpers();
        int rand = Random.Range(0, activeHelpers.Count);
        GameUI.instance.ShowSpeechBalloon(activeHelpers[rand].transform.position, LanguageManager.ReturnString("HELPER_RANDOM_SPEECH_" + Random.Range(1, 6)));
    }

    void Speech(string speech)
    {
        List<Helper> activeHelpers = GetActiveHelpers();
        GameUI.instance.ShowSpeechBalloon(activeHelpers[0].transform.position, LanguageManager.ReturnString(speech));
    }

    void Attack()
    {
        if (Mining.instance.disableHelpers.isOn)
            return;

        PerksManager.instance.ResetMultipliers();
        for (int i = 0; i < helpers.Length; i++)
        {
            if (!helpers[i].gameObject.activeSelf)
                continue;
            helpers[i].ProcessPerks();
        }

        StartCoroutine(HelpersAttackDisplay());
    }

    public float GetProgress()
    {
        float count = 0;
        for (int i = 0; i < helpers.Length; i++)
        {
            if (helpers[i].gameObject.activeSelf)
                count++;
        }
        count /= (float)helpers.Length;
        return count * 100;
    }

    public void LevelUp(int id)
    {
        if (Mining.instance.ProcessPurchase(helpers[id].GetCost(helpers[id].gameObject.activeSelf && !helpers[id].onSkill ? GameUI.instance.multiplier : 1)))
        {
            if (helpers[id].gameObject.activeSelf)
            {
                helpers[id].LevelUp(GameUI.instance.multiplier);
                GameUI.instance.UpdateHelpersTab();
                GameUI.instance.soundContainer.PlaySound("click");
            }
            else
            {
                helpers[id].gameObject.SetActive(true);
                GameUI.instance.soundContainer.PlaySound("hire");
                GameUI.instance.UpdateHelpersTab();
                GameUI.instance.ShowPhotocard(helpers[id].data.photo.sprite, LanguageManager.ReturnString("PHOTOCARD_MESSAGE_" + Random.Range(1, 5)).Replace("%NAME%", LanguageManager.ReturnString(helpers[id].data.name)));
                AchievementManager.instance.Increment("ACHIEVEMENT_HELPERS");

                if (GetActiveHelpers().Count == 1 && Time.timeSinceLevelLoad > 1)
                    Speech("HELPER_DAU_SPEECH");
            }

            SaveLoad.helpersLevels[id] = helpers[id].GetLevel();
            SaveLoad.helpersSkills[id] = 0;
            for (int i = 0; i < helpers[id].skillsActive.Length; i++)
                SaveLoad.helpersSkills[id] += helpers[id].skillsActive[i] ? 1 : 0;
            SaveLoad.SaveProgress();

            bigNumber levels = new bigNumber();
            for (int i = 0; i < helpers.Length; i++)
            {
                if (helpers[i].gameObject.activeSelf)
                    levels += helpers[i].GetLevel();
            }
            AchievementManager.instance.Set("ACHIEVEMENT_HELPERS_LEVELS", levels);
        }
    }

    public void SetLevel(int id, int level, int skillIndex)
    {
        helpers[id].gameObject.SetActive(level > 0);
        helpers[id].SetSkills(skillIndex);
        helpers[id].SetLevel(level);
    }

    public bigNumber GetDPS()
    {
        if (Mining.instance.disableHelpers.isOn)
            return new bigNumber();

        bigNumber dps = new bigNumber();
        foreach(Helper helper in helpers)
        {
            if (helper.gameObject.activeSelf)
                dps += helper.GetActualDamage(out bool critical);
        }

        AchievementManager.instance.Set("ACHIEVEMENT_HELPERS_DAMAGE", dps);

        return dps / 2;
    }

    public string GetDPSPercentage(bigNumber damage)
    {
        bigNumber result = new bigNumber(damage);
        result /= GetDPS() * 2;

        if (result.number >= 0.01f)
            return (Mathf.Clamp01(result.number) * 100).ToString("F0");
        else return "<1";
    }

    public int GetLevels()
    {
        int lvl = 0;
        for (int i = 0; i < helpers.Length; i++)
        {
            lvl += helpers[i].GetLevel();
        }
        return lvl;
    }

    public bool CanBuy()
    {
        for (int i = 0; i < helpers.Length; i++)
        {
            if (Mining.instance.GetCoins() >= helpers[i].GetCost())
                return true;
        }

        return false;
    }

    public bool HalfPrice()
    {
        bigNumber coeff = new bigNumber();
        for (int i = 0; i < helpers.Length; i++)
        {
            coeff = Mining.instance.GetCoins() / helpers[i].GetCost();

            if (coeff <= 0.5f)
                return true;
        }
        return false;
    }

    IEnumerator HelpersAttackDisplay()
    {
        for (int i = 0; i < helpers.Length; i++)
        {
            if (!helpers[i].gameObject.activeSelf)
                continue;

            yield return new WaitForSeconds(Random.Range(0, 0.2f));
            helpers[i].ShowAttack();
        }

        yield return null;
    }

    public void ResetHelpers()
    {
        for (int i = 0; i < helpers.Length; i++)
        {
            SetLevel(i, 0, 0);
            helpers[i].gameObject.SetActive(false);
            SaveLoad.helpersLevels[i] = 0;
            SaveLoad.helpersSkills[i] = 0;
        }
        GameUI.instance.UpdateHelpersTab();
    }
}