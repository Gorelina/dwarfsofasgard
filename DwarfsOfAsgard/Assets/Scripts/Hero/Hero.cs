﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : Character
{

    public HeroSkill[] skills;
    public HeroAbility[] abilities;
    [Header("Items")]
    public SpriteRenderer helmet;
    public SpriteRenderer armor;
    public SpriteRenderer cloak;
    public SpriteRenderer pickaxe;
    public SpriteRenderer shoulder;
    [Header("Components")]
    public Animator animator;
    public Effect[] punchEffects;
    public Effect[] skillEffects;
    public SoundContainer skillSounds;
    public GameObject midasCoins;
    public Animator shadowClone;
    private int maxMana;
    private float mana;
    public float Mana { get => mana; set { mana = value; manaChanged = true; } }
    [System.NonSerialized]
    public bool busy;
    [System.NonSerialized]
    public HeroSkillInstance[] skillInstances;
    [System.NonSerialized]
    public HeroAbilityInstance[] abilityInstances;
    private bool manaChanged;
    private float manaTime;
    private bool active;
    private float activeTime;
    private float damageMultiplier;
    private int attack = 1;
    public static Hero instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Init(true);

        skillInstances = new HeroSkillInstance[skills.Length];
        for (int i = 0; i < skills.Length; i++)
        {
            skillInstances[i] = new HeroSkillInstance(skills[i]);
            skillInstances[i].Init(SaveLoad.skillLevels[i]);
        }

        abilityInstances = new HeroAbilityInstance[abilities.Length];
        for (int i = 0; i < abilities.Length; i++)
        {
            abilityInstances[i] = new HeroAbilityInstance(i, abilities[i]);
        }
    }

    public void LoadMana()
    {
        manaTime = 60;
        int minutes = SaveLoad.ts.Minutes;
        if (minutes < 0)
            minutes = 0;
        SetMana(SaveLoad.mana + (Mathf.RoundToInt(2 + PerksManager.instance.Get("ManaRegen") * (AbilityIsActive("manaPotion") ? 1.5f : 1)) * minutes));
    }

    void ShadowCloneAttack()
    {
        Mining.instance.Damage(GetActualDamage(out bool critical), DamageSource.Skill, DamageTypes.Hero);
        shadowClone.SetTrigger("Attack" + attack);
        attack++;
        if (attack >= 6)
            attack = 1;
    }

    void MightyStrikes()
    {
        Mining.instance.Tap();
    }

    void Update()
    {
        if (activeTime > 0)
        {
            activeTime -= Time.deltaTime;
        }
        else if (active)
        {
            active = false;
        }

        busy = false;
        damageMultiplier = 1;
        for (int i = 0; i < skillInstances.Length; i++)
        {
            skillInstances[i].Update();
            if (skillInstances[i].busy)
                busy = true;
            if (skillInstances[i].active)
            {
                if (i == 1 || i == 3)
                {
                    damageMultiplier *= skillInstances[i].GetValue() * PerksManager.instance.Get("SpellHelperDamage") * PerksManager.instance.Get("ActiveSkillsBuff");
                }
            }
        }

        if (AbilityIsActive("adrenaline"))
            damageMultiplier *= 5;

        for (int i = 0; i < abilityInstances.Length; i++)
        {
            if (abilityInstances[i].active)
            {
                abilityInstances[i].Update();
            }
        }

        if (AbilityIsActive("mightyStrikes"))
        {
            if (!IsInvoking("MightyStrikes"))
                InvokeRepeating("MightyStrikes", 0.15f, 0.15f);
        }
        else
        {
            if (IsInvoking("MightyStrikes"))
                CancelInvoke("MightyStrikes");
        }

        if (Mana < GetMaxMana())
        {
            if (manaTime > 0)
                manaTime -= Time.deltaTime;
            else
            {
                manaTime = 60;
                Mana += Mathf.RoundToInt(2 + PerksManager.instance.Get("ManaRegen") * (AbilityIsActive("manaPotion") ? 1.5f : 1));
                if (Mana > GetMaxMana())
                    Mana = GetMaxMana();
            }
        }

        if (manaChanged)
        {
            GameUI.instance.SetMana(GetMaxMana(), (int)Mana, Mathf.RoundToInt(2 + PerksManager.instance.Get("ManaRegen")));
            manaChanged = false;
            SaveLoad.mana = (int)Mana;
            SaveLoad.SaveProgress();
        }

        midasCoins.SetActive(skillInstances[2].active);

        if (skillInstances[5].active)
        {
            if (!IsInvoking("ShadowCloneAttack"))
            {
                shadowClone.gameObject.SetActive(true);
                InvokeRepeating("ShadowCloneAttack", 1f / skillInstances[5].GetValue(), 1f / skillInstances[5].GetValue());
            }
        }
        else if(IsInvoking("ShadowCloneAttack"))
        {
            shadowClone.gameObject.SetActive(false);
            CancelInvoke("ShadowCloneAttack");
        }
    }

    public override bigNumber GetCost()
    {
        if (PerksManager.instance.Get("HeroCost") < 1)
            return base.GetCost() * PerksManager.instance.Get("HeroCost");
        else return base.GetCost();
    }

    public override bigNumber GetCost(int levelBonus)
    {
        if (PerksManager.instance.Get("HeroCost") < 1)
            return base.GetCost(levelBonus) * PerksManager.instance.Get("HeroCost");
        else return base.GetCost(levelBonus);
    }

    public bool CanBuy()
    {
        if (Mining.instance.GetCoins() >= GetCost())
            return true;
        return false;
    }

    public bool HalfPrice()
    {
        return Mining.instance.GetCoins() / GetCost() <= 0.5f;
    }

    public bool CanCast()
    {
        for (int i = 0; i < skillInstances.Length; i++)
        {
            if (skillInstances[i].bought && skillInstances[i].data.manaCost <= Mana)
                return true;
        }
        return false;
    }

    public int SkillCount()
    {
        int count = 0;
        for (int i = 0; i < skillInstances.Length; i++)
        {
            if (skillInstances[i].bought)
                count++;
        }
        return count;
    }

    public bool IsActive()
    {
        return active;
    }

    public void Activate()
    {
        active = true;
        activeTime = 5;
    }

    public override bigNumber GetPureDamage()
    {
        return base.GetPureDamage();
    }

    public override bigNumber GetFinalDamage()
    {
        bigNumber _damage = new bigNumber(base.GetPureDamage());

        _damage *= PerksManager.instance.Get("AllDamage");
        _damage *= PerksManager.instance.Get("TapDamage");

        if (active)
            _damage *= PerksManager.instance.Get("TapDamageFromHelpers");

        if (skillInstances[2].active)
            _damage *= PerksManager.instance.Get("DamageWhileMidas");

        _damage *= damageMultiplier;

        return _damage;
    }

    public override bigNumber GetActualDamage(out bool critical)
    {
        critical = false;
        bigNumber _damage = new bigNumber(GetFinalDamage());
        int additionalCritChance = 0;
        if (skillInstances[1].active)
            additionalCritChance = 50;
        if (Formules.CalculateChance(PerksManager.instance.Get("CritChance") + additionalCritChance))
        {
            critical = true;
            _damage *= PerksManager.instance.Get("CritDamage") * Random.Range(0.75f, 1.25f);
        }
        return _damage;
    }

    public void ShowAttack()
    {
        animator.SetTrigger("Attack" + attack);
        attack++;
        if (attack >= 6)
            attack = 1;
    }

    public void ShowEffect()
    {
        foreach(Effect effect in punchEffects)
            effect.Play();
    }

    public void ResetSkills()
    {
        for (int i = 0; i < skillInstances.Length; i++)
        {
            skillInstances[i].bought = false;
            skillInstances[i].level = 0;
            SaveLoad.skillLevels[i] = skillInstances[i].level;
        }

        maxMana = 0;
        SetMana(0);
    }

    public void SetMana(int _mana)
    {
        Mana = _mana;
        if (Mana > GetMaxMana())
        {
            Mana = GetMaxMana();
        }
    }

    public void AddMana()
    {
        Mana += Mathf.RoundToInt(GetMaxMana() * 0.35f);
        if (Mana > GetMaxMana())
        {
            Mana = GetMaxMana();
        }
    }

    public void AddMana(float _mana)
    {
        Mana += (float)GetMaxMana() * _mana;
        if (Mana > GetMaxMana())
        {
            Mana = GetMaxMana();
        }
    }

    public int GetMaxMana()
    {
        return maxMana + (int)PerksManager.instance.Get("ManaPoolCap");
    }

    public bigNumber GetSkillCost(int index)
    {
        bigNumber result = new bigNumber(skillInstances[index].cost);

        if (Mining.instance.GetDiscount())
            result *= Mining.instance.discountMult;

        return result;
    }

    public void SetSkill(int index, int level)
    {
        if (level == 0)
            return;

        if (!skillInstances[index].bought)
        {
            maxMana += 35;
        }
        skillInstances[index].bought = level > 0;
        skillInstances[index].level = level;

        GameUI.instance.UpdateCharacterTab();
    }

    public void BuySkill(int index)
    {
        if (!skillInstances[index].bought)
        {
            maxMana += 35;
            SetMana(GetMaxMana());
            skillInstances[index].bought = true;
        }

        skillInstances[index].LevelUp();

        GameUI.instance.soundContainer.PlaySound("click");
        GameUI.instance.UpdateCharacterTab();

        SaveLoad.skillLevels[index] = skillInstances[index].level;
        SaveLoad.SaveProgress();
    }

    public void PlaySkillEffect(int index)
    {
        skillEffects[index].Play();
    }

    public void ActivateSkill(int index)
    {
        if (!skillInstances[index].Possible(Mana))
            return;

        SkillActivate(index);

        switch (index)
        {
            case 0:
                AchievementManager.instance.Increment("ACHIEVEMENT_HEAVEN_STRIKE");
                break;

            case 2:
                AchievementManager.instance.Increment("ACHIEVEMENT_MIDAS_TOUCH");
                break;

            case 3:
                AchievementManager.instance.Increment("ACHIEVEMENT_FIRE_SWORD");
                break;

            case 4:
                AchievementManager.instance.Increment("ACHIEVEMENT_WAR_CRY");
                break;

            case 5:
                AchievementManager.instance.Increment("ACHIEVEMENT_SHADOW_CLONE");
                break;
        }
    }

    public void ActivateSkill()
    {
        int rand = Random.Range(0, skillInstances.Length);
        int c = 20;
        while(true)
        {
            if (c <= 0)
                return;

            if (skillInstances[rand].bought && !skillInstances[rand].busy)
                break;
            else rand = Random.Range(0, skillInstances.Length);

            c--;
        }

        SkillActivate(rand, true);
    }

    void SkillActivate(int index, bool ignoreMana = false)
    {
        skillSounds.PlaySound(index.ToString());

        animator.SetTrigger("Skill" + (index + 1).ToString());
        skillInstances[index].Activate(out int manaCost);
        if (!ignoreMana)
            Mana -= manaCost;

        if (PerksManager.instance.Get("ManaBack") > 0)
            Mana += Mathf.RoundToInt(manaCost * PerksManager.instance.Get("ManaBack"));

        if (PerksManager.instance.Get("SkillsGold") > 0)
        {
            Mining.instance.AddCoins(Mining.instance.GetCurrentLevelCoins() * PerksManager.instance.Get("SkillsGold"));
        }
    }

    public void ActivateAbility(int index)
    {
        abilityInstances[index].Activate();
        GameUI.instance.soundContainer.PlaySound("click");

        if (abilityInstances[index].data.effect == "heavenGift")
        {
            Mining.instance.AddCoins(Mining.instance.GetCoins() * 200);
        }
        else if (abilityInstances[index].data.effect == "manaPotion")
            Mana = GetMaxMana();

        AchievementManager.instance.Increment("ACHIEVEMENT_ABILITIES");
    }

    public bool AbilityIsActive(string name)
    {
        for (int i = 0; i < abilityInstances.Length; i++)
        {
            if (abilityInstances[i].data.effect == name && abilityInstances[i].active)
                return true;
        }

        return false;
    }
}