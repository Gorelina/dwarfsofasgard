﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
[CreateAssetMenu(fileName = "New Ability", menuName = "General/Ability")]
public class HeroAbility : DBEntry
{

    public SpriteImage icon;
    public string effect;
    [Tooltip("Hours")]
    public int time;

}

public class HeroAbilityInstance
{

    public int id;
    public HeroAbility data;
    public bool active;
    private TimeSpan timeSpan;

    public HeroAbilityInstance(int id, HeroAbility data)
    {
        this.id = id;
        this.data = data;
        Init();
    }

    public void Init()
    {
        timeSpan = SaveLoad.abilities[id] - DateTime.Now;

        if (DateTime.Compare(DateTime.Now, SaveLoad.abilities[id]) <= 0)
            active = true;
    }

    public void Update()
    {
        if (active)
        {
            timeSpan = SaveLoad.abilities[id] - DateTime.Now;

            if (DateTime.Compare(DateTime.Now, SaveLoad.abilities[id]) > 0)
                active = false;
        }
    }

    public void Activate()
    {
        if (Mining.instance.ProcessDiamonds(100))
        {
            active = true;
            SaveLoad.abilities[id] = DateTime.Now.AddHours(data.time);
            SaveLoad.SaveProgress();
        }
    }

    public TimeSpan GetTimeSpan()
    {
        return timeSpan;
    }
}