﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Skill", menuName = "General/Skill")]
public class HeroSkill : ScriptableObject
{
    [Header("Main")]
    public SpriteImage icon;
    public SpriteImage littleIcon;
    new public string name;
    public bigNumber cost;
    public enum Types {Passive, Active };
    public Types type;
    [Header("Stats")]
    public string effect;    
    public int manaCost;
    public float cooldown;
    public float castTime;
    public int requiredLevel;
    public float value;
    public float step;
}

public class HeroSkillInstance
{
    public HeroSkill data;
    public int level;
    public bool bought;
    public bool active;
    public bool busy;
    public float busyTime;
    public bool casting;
    public float castingTime;
    public float cooldown;
    public float value;
    public int manaCost;
    public bigNumber cost;
    public enum Parameters{Mana, Buff, Time};
    public delegate void OnUpgrade();
    public OnUpgrade onUpgrade;

    public HeroSkillInstance(HeroSkill _data)
    {
        data = _data;
        bought = false;
        value = _data.value;
        level = 0;
    }

    public void Init(int level)
    {
        value = data.value;
        manaCost = data.manaCost;
        cost = data.cost;
        this.level = level;
        for (int i = 0; i <= level; i++)
        {
            if (i > 1)
                value *= data.step;

            if (i > 0)
            {
                if (i % 2 == 0)
                {
                    cost *= 95;
                    if (i > 1)
                        manaCost++;
                }
                else
                {
                    cost *= 105;
                }
            }
        }
    }

    public void Update()
    {
        if (busyTime > 0)
        {
            busyTime -= Time.deltaTime;
        }
        else if (busy)
        {
            if (data.effect == "holyStrike")
            {
                Mining.instance.Damage(Hero.instance.GetPureDamage() * value * PerksManager.instance.Get("SpellHelperDamage"), DamageSource.Skill, DamageTypes.Hero);
                GameUI.instance.ShowPopup(Camera.main.WorldToScreenPoint(Hero.instance.transform.position + Vector3.up), new PopupData((Hero.instance.GetPureDamage() * value * PerksManager.instance.Get("SpellHelperDamage")).ToString(), Color.cyan, 1.5f));
            }
            busy = false;
        }

        if (castingTime > 0)
        {
            castingTime -= Time.deltaTime;
        }
        else if (casting)
        {
            casting = false;
        }

        if (!casting && active)
            active = false;

        if (cooldown > 0 && !casting)
        {
            cooldown -= Time.deltaTime;
        }
    }

    public void LevelUp()
    {
        level++;
        if (level > 1)
            value *= data.step;
        if (level > 0)
        {
            if (level % 2 == 0)
            {
                cost *= 95;
                if (level > 1)
                    manaCost++;
            }
            else
            {
                cost *= 105;
            }
        }

        if (onUpgrade != null)
            onUpgrade.Invoke();
    }

    public bool Possible(float mana)
    {
        if (!bought)
            return false;

        if (busy || casting || cooldown > 0)
            return false;

        int manaCost = Mathf.RoundToInt(data.manaCost * GetSkillPerkMult(data.effect, Parameters.Mana));

        if (mana < manaCost)
            return false;

        return true;
    }

    public float GetValue()
    {
        return value * GetSkillPerkMult(data.effect, Parameters.Buff);
    }

    public int GetManaCost()
    {
        return Mathf.RoundToInt(manaCost * GetSkillPerkMult(data.effect, Parameters.Mana));
    }

    public float GetCastTime()
    {
        return data.castTime * GetSkillPerkMult(data.effect, Parameters.Time);
    }

    public float GetCooldownTime()
    {
        return data.cooldown * PerksManager.instance.Get("SkillsCooldown");
    }

    public void Activate(out int manaCost)
    {
        castingTime = GetCastTime();
        cooldown = GetCooldownTime();
        casting = true;
        if (data.effect == "holyStrike")
        {
            busy = true;
            busyTime = data.castTime;
        }
        manaCost = this.manaCost;
        active = true;
    }

    public static float GetSkillPerkMult(string effectName, Parameters parameter)
    {
        switch (effectName)
        {
            case "heavenStike":
                switch (parameter)
                {
                    case Parameters.Buff:
                        return PerksManager.instance.Get("HeavenStrikeBuff");

                    case Parameters.Mana:
                        return PerksManager.instance.Get("HeavenStrikeMana");

                    case Parameters.Time:
                        return 1;
                }
                break;

            case "deadlyStrike":
                switch (parameter)
                {
                    case Parameters.Buff:
                        return PerksManager.instance.Get("DeadlyStrikeBuff");

                    case Parameters.Mana:
                        return PerksManager.instance.Get("DeadlyStrikeMana");

                    case Parameters.Time:
                        return PerksManager.instance.Get("DeadlyStrikeTime");
                }
                break;

            case "midasTouch":
                switch (parameter)
                {
                    case Parameters.Buff:
                        return PerksManager.instance.Get("MidasBuff");

                    case Parameters.Mana:
                        return PerksManager.instance.Get("MidasMana");

                    case Parameters.Time:
                        return PerksManager.instance.Get("MidasTime");
                }
                break;

            case "fireSword":
                switch (parameter)
                {
                    case Parameters.Buff:
                        return PerksManager.instance.Get("FireSwordBuff");

                    case Parameters.Mana:
                        return PerksManager.instance.Get("FireSwordMana");

                    case Parameters.Time:
                        return PerksManager.instance.Get("FireSwordTime");
                }
                break;

            case "warCry":
                switch (parameter)
                {
                    case Parameters.Buff:
                        return PerksManager.instance.Get("WarcryBuff");

                    case Parameters.Mana:
                        return PerksManager.instance.Get("WarcryMana");

                    case Parameters.Time:
                        return PerksManager.instance.Get("WarcryTime");
                }
                break;

            case "shadowClone":
                switch (parameter)
                {
                    case Parameters.Buff:
                        return PerksManager.instance.Get("ShadowCloneBuff");

                    case Parameters.Mana:
                        return PerksManager.instance.Get("ShadowCloneMana");

                    case Parameters.Time:
                        return PerksManager.instance.Get("ShadowCloneTime");
                }
                break;

        }

        return 1;
    }
}