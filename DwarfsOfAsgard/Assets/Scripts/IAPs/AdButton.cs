﻿using UnityEngine;
using UnityEngine.UI;

public class AdButton : StoreButton
{

    public int diamonds = 25;
    public Button button;

    void Start()
    {
        rewardText.text = diamonds.ToString();
    }

    void Update()
    {
        if (!gameObject.activeSelf)
            return;

        button.interactable = !SaveLoad.video && AdManager.RewardAdLoaded();
    }

    public override void Buy()
    {
        GameUI.instance.soundContainer.PlaySound("click");
        AdManager.WatchRewarded(BuyAccept);
    }

    public override void BuyAccept()
    {
        Mining.instance.AddDiamonds(diamonds);
        SaveLoad.video = true;
        SaveLoad.SaveProfile();
    }
}