﻿using UnityEngine;
using UnityEngine.UI;

public class StoreButton : MonoBehaviour, IStoreButton
{

    public Text rewardText;
    public Text costText;

    public virtual void Buy()
    {
        throw new System.NotImplementedException();
    }

    public virtual void BuyAccept()
    {
        throw new System.NotImplementedException();
    }
}
