﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BundleButton : StoreButton
{

    public Image[] slots;
    public Sprite slotImage;
    public Image[] icons;
    public Image abilityIcon;
    public Text abilityText;
    public Text coinsText;
    public Text diamondsText;
    public Item[] items;
    public int abilityIndex;
    public bigNumber coins;
    public int diamonds;

    void Start()
    {
        coins.Update();

        abilityIcon.sprite = Hero.instance.abilities[abilityIndex].icon.sprite;
        abilityText.text = LanguageManager.ReturnString(Hero.instance.abilities[abilityIndex].name + "_NAME");
        coinsText.text = coins.ToString();
        diamondsText.text = diamonds.ToString();

        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].sprite = slotImage;
            if (i >= items.Length)
                slots[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < icons.Length; i++)
        {
            if (i < items.Length)
            {
            icons[i].sprite = items[i].icon.sprite;
            }
            else
                icons[i].gameObject.SetActive(false);
        }
    }

    public override void Buy()
    {
        Mining.instance.AddCoins(coins);
        Mining.instance.AddDiamonds(diamonds);
        Hero.instance.ActivateAbility(abilityIndex);
        for (int i = 0; i < items.Length; i++)
        {
            Inventory.instance.Add(items[i]);
        }
    }

}