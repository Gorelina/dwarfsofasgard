﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class BundleManager : MonoBehaviour
{

    public string referenceBundleId;
    public GameObject[] bundles;
    [Header("UI")]
    public Text oldPriceText;
    public Text estimatedTimeText;
    public GameObject locked;
    private DateTime time;
    private DateTime endTime;
    private TimeSpan span;

    void Start()
    {
        InvokeRepeating("UpdateTime", 0, 60);
        Product referenceBundle = CodelessIAPStoreListener.Instance.GetProduct(referenceBundleId);
        oldPriceText.text = referenceBundle.metadata.localizedPriceString;
    }

    void UpdateTime()
    {
        DateTime.TryParseExact(SaveLoad.exitTime, SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out time);
        endTime = new DateTime(time.Year, time.Month, time.Day, 23, 59, 59, 99);
        span = endTime - time;
        estimatedTimeText.text = LanguageManager.ReturnString("STORE_OFFER_TIME").Replace("%T%", span.ToString(@"hh\:mm"));

        locked.SetActive(time.DayOfYear == SaveLoad.bundleIndex);
        for(int i = 0; i < bundles.Length; i++)
        {
            bundles[i].SetActive(i == (int)time.DayOfWeek);
        }
    }

    public void Buy()
    {
        SaveLoad.bundleIndex = time.DayOfYear;
        SaveLoad.SaveProfile();
        UpdateTime();
    }

}