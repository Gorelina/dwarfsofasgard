﻿
public class ChestButton : StoreButton
{

    public int diamonds;
    public Item.Rarity rarity;

    void Start()
    {
        switch (rarity)
        {
            case Item.Rarity.Common:
                rewardText.text = LanguageManager.ReturnString("WAU_ITEM_COMMON");
                break;

            case Item.Rarity.Rare:
                rewardText.text = LanguageManager.ReturnString("WAU_ITEM_RARE");
                break;

            case Item.Rarity.Epic:
                rewardText.text = LanguageManager.ReturnString("WAU_ITEM_EPIC");
                break;

            case Item.Rarity.Legendary:
                rewardText.text = LanguageManager.ReturnString("WAU_ITEM_LEGENDARY");
                break;
        }
        rewardText.text += "!";
        costText.text = diamonds.ToString();
    }

    public override void Buy()
    {
        GameUI.instance.soundContainer.PlaySound("click");
        string desc = LanguageManager.ReturnString("STORE_WINDOW_DESC");
        desc = desc.Replace("%C%", LanguageManager.ReturnString("STORE_CHEST").ToLower());
        desc = desc.Replace("%D%", diamonds.ToString());
        if (Mining.instance.GetDiamonds() < diamonds)
            return;
        GameUI.instance.ShowWindow(desc, delegate { BuyAccept(); });
    }

    public override void BuyAccept()
    {
        if (Mining.instance.ProcessDiamonds(diamonds))
        {
            Inventory.instance.AddRandom(rarity);
        }
    }

}