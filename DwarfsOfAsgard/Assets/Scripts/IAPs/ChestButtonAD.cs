﻿using System;

public class ChestButtonAD : StoreButton
{

    public Item.Rarity rarity;
    private DateTime time;
    private TimeSpan span;

    void Start()
    {
        DateTime.TryParseExact(SaveLoad.adChestTime, SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out time);
        span = time - DateTime.Now;

        switch (rarity)
        {
            case Item.Rarity.Common:
                rewardText.text = LanguageManager.ReturnString("WAU_ITEM_COMMON");
                break;

            case Item.Rarity.Rare:
                rewardText.text = LanguageManager.ReturnString("WAU_ITEM_RARE");
                break;

            case Item.Rarity.Epic:
                rewardText.text = LanguageManager.ReturnString("WAU_ITEM_EPIC");
                break;

            case Item.Rarity.Legendary:
                rewardText.text = LanguageManager.ReturnString("WAU_ITEM_LEGENDARY");
                break;
        }
        rewardText.text += "!";
    }

    void Update()
    {
        if (!gameObject.activeSelf)
            return;

        if (span.TotalSeconds <= 0)
        {
            costText.text = LanguageManager.ReturnString("WATCH_AD");
        }
        else
        {
            span = time - DateTime.Now;
            costText.text = span.ToString(@"hh\:mm\:ss");
        }
    }

    public override void Buy()
    {
        if (span.TotalSeconds > 0 || !AdManager.RewardAdLoaded())
            return;

        time = DateTime.Now.AddDays(1);
        span = time - DateTime.Now;
        SaveLoad.adChestTime = time.ToString(SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture);
        SaveLoad.SaveProfile();
        GameUI.instance.soundContainer.PlaySound("click");
        AdManager.WatchRewarded(BuyAccept);
    }

    public override void BuyAccept()
    {
        Inventory.instance.AddRandom(rarity);
    }

}