﻿
public class CoinsButton : StoreButton
{

    public bigNumber coins;
    public int diamonds;

    void Start()
    {
        coins.Update();
        rewardText.text = coins.ToString();
        costText.text = diamonds.ToString();
    }

    public override void Buy()
    {
        GameUI.instance.soundContainer.PlaySound("click");
        string desc = LanguageManager.ReturnString("STORE_WINDOW_DESC");
        desc = desc.Replace("%C%", coins.ToString());
        desc = desc.Replace("%D%", diamonds.ToString());
        if (Mining.instance.GetDiamonds() < diamonds)
            return;
        GameUI.instance.ShowWindow(desc, delegate { BuyAccept(); });
    }

    public override void BuyAccept()
    {
        if (Mining.instance.ProcessDiamonds(diamonds))
        {
            Mining.instance.AddCoins(coins);
            GameUI.instance.soundContainer.PlaySound("cash");
        }
    }
}