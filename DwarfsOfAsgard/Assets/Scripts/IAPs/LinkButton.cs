﻿using UnityEngine;
using UnityEngine.UI;

public class LinkButton : StoreButton
{

    [Tooltip("0 - facebook, 1 - twitter, 2 - vk")]
    public int index;
    public string link;
    public int diamonds = 25;
    public Button button;

    void Start()
    {
        rewardText.text = diamonds.ToString();
        UpdateUI();
    }

    void UpdateUI()
    {
        switch (index)
        {
            case 0:
                button.interactable = !SaveLoad.facebook;
                break;

            case 1:
                button.interactable = !SaveLoad.twitter;
                break;

            case 2:
                button.interactable = !SaveLoad.vk;
                break;
        }
    }

    public override void Buy()
    {
        GameUI.instance.soundContainer.PlaySound("click");
        Application.OpenURL(link);
        BuyAccept();
    }

    public override void BuyAccept()
    {
        switch (index)
        {
            case 0:
                SaveLoad.facebook = true;
                break;

            case 1:
                SaveLoad.twitter = true;
                break;

            case 2:
                SaveLoad.vk = true;
                break;
        }
        Mining.instance.AddDiamonds(diamonds);
        SaveLoad.SaveProfile();
        UpdateUI();
    }
}