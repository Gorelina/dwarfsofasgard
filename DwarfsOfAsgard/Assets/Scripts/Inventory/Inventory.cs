﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{

    public class SetProcessor
    {
        public List<string> names;
        public List<PerkEffect[]> effects;
        public List<int> n;

        public SetProcessor()
        {
            names = new List<string>();
            effects = new List<PerkEffect[]>();
            n = new List<int>();
        }

        public void Add(string name, PerkEffect effect2, PerkEffect effect3, PerkEffect effect4)
        {
            if (!names.Contains(name))
            {
                names.Add(name);
                PerkEffect[] perkEffects = new PerkEffect[3];
                perkEffects[0] = effect2;
                perkEffects[1] = effect3;
                perkEffects[2] = effect4;
                effects.Add(perkEffects);
                n.Add(1);
            }
            else
            {
                n[names.IndexOf(name)] += 1;
            }
        }

        public PerkEffect[] GetEffects()
        {
            List<PerkEffect> result = new List<PerkEffect>();
            for (int i = 0; i < names.Count; i++)
            {
                if (n[i] >= 2)
                    result.Add(effects[i][0]);
                if (n[i] >= 3)
                    result.Add(effects[i][1]);
                if (n[i] >= 4)
                    result.Add(effects[i][2]);
            }
            return result.ToArray();
        }
    }

    [System.Serializable]
    public class ItemSave
    {
        public int id;
        public int level;

        public ItemSave()
        {
            id = -1;
            level = 0;
        }

        public ItemSave(int id, int level)
        {
            this.id = id;
            this.level = level;
        }
    }

    [System.Serializable]
    public class SaveData
    {
        public ItemSave weapon;
        public ItemSave head;
        public ItemSave chest;
        public ItemSave cloak;
        public ItemSave[] weapons;
        public ItemSave[] heads;
        public ItemSave[] chests;
        public ItemSave[] cloaks;
        public bool[] newItems;
    }

    public GameObject notification;
    public GameObject[] notifications;
    public GameObject[] tabs;
    public Sprite[] bgs;
    public Color[] colors;
    public Sprite weaponEmptyIcon;
    public Sprite headEmptyIcon;
    public Sprite chestEmptyIcon;
    public Sprite cloakEmptyIcon;
    public Sprite weaponStock;
    public Sprite headStock;
    public Sprite chestStock;
    public Sprite cloakStock;
    public Sprite shoulderStock;
    public InventorySlotUI weaponUI;
    public InventorySlotUI headUI;
    public InventorySlotUI chestUI;
    public InventorySlotUI cloakUI;
    public Transform weaponsHolder;
    public Transform headHolder;
    public Transform chestHolder;
    public Transform cloakHolder;
    public Transform inventorySlotPrefab;
    private InventorySlot weapon;
    private InventorySlot head;
    private InventorySlot chest;
    private InventorySlot cloak;
    private List<InventorySlot> weapons;
    private List<InventorySlot> heads;
    private List<InventorySlot> chests;
    private List<InventorySlot> cloaks;
    private bool[] newItems = new bool[4];
    private InventorySlot selectedSlot;
    private int tab = -1;
    private Queue<Item> discoveryQueue = new Queue<Item>();
    public static Inventory instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        weapon = new InventorySlot();
        head = new InventorySlot();
        chest = new InventorySlot();
        cloak = new InventorySlot();

        weapons = new List<InventorySlot>();
        heads = new List<InventorySlot>();
        chests = new List<InventorySlot>();
        cloaks = new List<InventorySlot>();

        Load();

        UpdateUI();
        UpdateHero();
    }

    void Update()
    {
        bool somethingNew = false;
        for (int i = 0; i < newItems.Length; i++)
        {
            if (newItems[i])
                somethingNew = true;
            notifications[i].SetActive(newItems[i]);
        }
        notification.SetActive(somethingNew);

        if (discoveryQueue.Count > 0 && !GameUI.instance.newItemWindow.activeSelf)
        {
            Item newItem = discoveryQueue.Dequeue();
            GameUI.instance.newItemWindow.SetActive(true);
            GameUI.instance.newItemIcon.sprite = newItem.icon.sprite;
            GameUI.instance.newItemName.text = LanguageManager.ReturnString(newItem.name);
            GameUI.instance.newItemEffect.text = string.Format(PerksManager.instance.Description(newItem.effect.effect), newItem.effect.multiplier);
        }
    }

    void UpdateUI()
    {
        if (!weapon.IsEmpty())
            weaponUI.Set(weapon.GetData(), weapon.GetLevel(), bgs[(int)weapon.GetData().rarity], colors[(int)weapon.GetData().rarity], false, !weapon.IsEmpty());
        else
            weaponUI.Set(bgs[0], 0, weaponEmptyIcon);

        if (!head.IsEmpty())
            headUI.Set(head.GetData(), head.GetLevel(), bgs[(int)head.GetData().rarity], colors[(int)head.GetData().rarity], false, !weapon.IsEmpty());
        else
            headUI.Set(bgs[0], 0, headEmptyIcon);

        if (!chest.IsEmpty())
            chestUI.Set(chest.GetData(), chest.GetLevel(), bgs[(int)chest.GetData().rarity], colors[(int)chest.GetData().rarity], false, !weapon.IsEmpty());
        else
            chestUI.Set(bgs[0], 0, chestEmptyIcon);

        if (!cloak.IsEmpty())
            cloakUI.Set(cloak.GetData(), cloak.GetLevel(), bgs[(int)cloak.GetData().rarity], colors[(int)cloak.GetData().rarity], false, !weapon.IsEmpty());
        else
            cloakUI.Set(bgs[0], 0, cloakEmptyIcon);

        Transform clone;
        InventorySlotUI slotUI;

        //weapon slots
        if (weaponsHolder.childCount > 0)
        {
            foreach (Transform child in weaponsHolder.transform)
                Destroy(child.gameObject);
        }

        for (int i = 0; i < weapons.Count; i++)
        {
            clone = Instantiate(inventorySlotPrefab, weaponsHolder);
            slotUI = clone.GetComponent<InventorySlotUI>();
            slotUI.Set(weapons[i].GetData(), weapons[i].GetLevel(), bgs[(int)weapons[i].GetData().rarity], colors[(int)weapons[i].GetData().rarity], true, weapon.GetData() == weapons[i].GetData(), this, weapons[i]);
            if (weapon.GetData() == weapons[i].GetData())
                slotUI.transform.SetAsFirstSibling();
        }

        //head slots
        if (headHolder.childCount > 0)
        {
            foreach (Transform child in headHolder.transform)
                Destroy(child.gameObject);
        }

        for (int i = 0; i < heads.Count; i++)
        {
            clone = Instantiate(inventorySlotPrefab, headHolder);
            slotUI = clone.GetComponent<InventorySlotUI>();
            slotUI.Set(heads[i].GetData(), heads[i].GetLevel(), bgs[(int)heads[i].GetData().rarity], colors[(int)heads[i].GetData().rarity], true, head.GetData() == heads[i].GetData(), this, heads[i]);
            if (head.GetData() == heads[i].GetData())
                slotUI.transform.SetAsFirstSibling();
        }

        //chest slots
        if (chestHolder.childCount > 0)
        {
            foreach (Transform child in chestHolder.transform)
                Destroy(child.gameObject);
        }

        for (int i = 0; i < chests.Count; i++)
        {
            clone = Instantiate(inventorySlotPrefab, chestHolder);
            slotUI = clone.GetComponent<InventorySlotUI>();
            slotUI.Set(chests[i].GetData(), chests[i].GetLevel(), bgs[(int)chests[i].GetData().rarity], colors[(int)chests[i].GetData().rarity], true, chest.GetData() == chests[i].GetData(), this, chests[i]);
            if (chest.GetData() == chests[i].GetData())
                slotUI.transform.SetAsFirstSibling();
        }

        //cloak slots
        if (cloakHolder.childCount > 0)
        {
            foreach (Transform child in cloakHolder.transform)
                Destroy(child.gameObject);
        }

        for (int i = 0; i < cloaks.Count; i++)
        {
            clone = Instantiate(inventorySlotPrefab, cloakHolder);
            slotUI = clone.GetComponent<InventorySlotUI>();
            slotUI.Set(cloaks[i].GetData(), cloaks[i].GetLevel(), bgs[(int)cloaks[i].GetData().rarity], colors[(int)cloaks[i].GetData().rarity], true, cloak.GetData() == cloaks[i].GetData(), this, cloaks[i]);
            if (cloak.GetData() == cloaks[i].GetData())
                slotUI.transform.SetAsFirstSibling();
        }

        UpdateHero();
    }

    void UpdateHero()
    {
        if (!weapon.IsEmpty())
            Hero.instance.pickaxe.sprite = weapon.GetData().icon.sprite;
        else
            Hero.instance.pickaxe.sprite = weaponStock;

        if (!head.IsEmpty())
            Hero.instance.helmet.sprite = head.GetData().icon.sprite;
        else
            Hero.instance.helmet.sprite = headStock;

        if (!chest.IsEmpty())
        {
            Hero.instance.armor.sprite = chest.GetData().icon.sprite;
            Hero.instance.armor.sortingOrder = 6;
            Hero.instance.shoulder.gameObject.SetActive(false);
        }
        else
        {
            Hero.instance.armor.sprite = chestStock;
            Hero.instance.armor.sortingOrder = 4;
            Hero.instance.shoulder.gameObject.SetActive(true);
            Hero.instance.shoulder.sprite = shoulderStock;
        }

        if (!cloak.IsEmpty())
            Hero.instance.cloak.sprite = cloak.GetData().icon.sprite;
        else
            Hero.instance.cloak.sprite = cloakStock;
    }

    public void Add(Item item)
    {
        InventorySlot newItem = new InventorySlot();
        newItem.Set(item, 0);
        switch (item.type)
        {
            case Item.Types.Chest:
                chests.Add(newItem);
                newItems[0] = true;
                break;

            case Item.Types.Cloak:
                cloaks.Add(newItem);
                newItems[1] = true;
                break;

            case Item.Types.Head:
                heads.Add(newItem);
                newItems[2] = true;
                break;

            case Item.Types.PickAxe:
                weapons.Add(newItem);
                newItems[3] = true;
                break;
        }

        Save();
        UpdateUI();
        AchievementManager.instance.Increment("ACHIEVEMENT_ITEMS");

        if (Time.timeSinceLevelLoad > 1)
        {
            discoveryQueue.Enqueue(item);
        }
    }

    public bool AddRandom()
    {
        Item item = DataBase.instance.items[Random.Range(0, DataBase.instance.items.Length)];

        Add(item);
        return true;
    }

    public bool AddRandom(Item.Types type)
    {
        Item item = DataBase.instance.items[Random.Range(0, DataBase.instance.items.Length)];

        while (true)
        {
            if (item.type != type)
                item = DataBase.instance.items[Random.Range(0, DataBase.instance.items.Length)];
            else break;
        }

        Add(item);
        return true;
    }

    public bool AddRandom(Item.Rarity rarity)
    {
        Item item = DataBase.instance.items[Random.Range(0, DataBase.instance.items.Length)];

        while (true)
        {
            if (item.rarity != rarity)
                item = DataBase.instance.items[Random.Range(0, DataBase.instance.items.Length)];
            else break;
        }

        Add(item);
        return true;
    }

    public bool AddRandom(Item.Rarity rarity, Item.Types type)
    {
        Item item = DataBase.instance.items[Random.Range(0, DataBase.instance.items.Length)];

        while (true)
        {
            if (item.type != type || item.rarity != rarity)
                item = DataBase.instance.items[Random.Range(0, DataBase.instance.items.Length)];
            else break;
        }

        Add(item);
        return true;
    }

    public void Equip(InventorySlot slot)
    {
        GameUI.instance.soundContainer.PlaySound("click");

        switch (slot.GetData().type)
        {
            case Item.Types.Chest:
                chest.Set(slot.GetData(), slot.GetLevel());
                break;

            case Item.Types.Head:
                head.Set(slot.GetData(), slot.GetLevel());
                break;

            case Item.Types.Cloak:
                cloak.Set(slot.GetData(), slot.GetLevel());
                break;

            case Item.Types.PickAxe:
                weapon.Set(slot.GetData(), slot.GetLevel());
                break;
        }

        Save();
        UpdateUI();
    }

    public void Disassemble(InventorySlot slot)
    {
        GameUI.instance.soundContainer.PlaySound("click");
        selectedSlot = slot;
        string msg = LanguageManager.ReturnString("ITEM_DISASSEMBLE_WINDOW").Replace("%N%", "4");
        msg = msg.Replace("%I%", LanguageManager.ReturnString(slot.GetData().name));
        GameUI.instance.ShowWindow(msg, delegate { DisassembleConfirm(); }, true);
    }

    public void DisassembleConfirm()
    {
        GameUI.instance.soundContainer.PlaySound("click");
        Mining.instance.AddDiamonds(4);

        switch (selectedSlot.GetData().type)
        {
            case Item.Types.Chest:
                chests.Remove(selectedSlot);
                break;

            case Item.Types.Cloak:
                cloaks.Remove(selectedSlot);
                break;

            case Item.Types.Head:
                heads.Remove(selectedSlot);
                break;

            case Item.Types.PickAxe:
                weapons.Remove(selectedSlot);
                break;
        }

        Save();
        UpdateUI();
    }

    public void Upgrade(InventorySlot slot)
    {
        GameUI.instance.soundContainer.PlaySound("click");

        if (!Mining.instance.ProcessResources(3 + (int)slot.GetData().rarity, new bigNumber(Formules.GetUpgradePrice(slot.GetLevel()))))
            return;

        slot.LevelUp();
        Equip(slot);

        Save();
        UpdateUI();
    }

    void Load()
    {
        if (SaveLoad.inventory == null)
            return;

        if (SaveLoad.inventory.weapon != null && SaveLoad.inventory.weapon.id >= 0)
            weapon.Set(DataBase.instance.items[SaveLoad.inventory.weapon.id], SaveLoad.inventory.weapon.level);
        else weapon.Empty();

        if (SaveLoad.inventory.chest != null && SaveLoad.inventory.chest.id >= 0)
            chest.Set(DataBase.instance.items[SaveLoad.inventory.chest.id], SaveLoad.inventory.chest.level);
        else chest.Empty();

        if (SaveLoad.inventory.head != null && SaveLoad.inventory.head.id >= 0)
            head.Set(DataBase.instance.items[SaveLoad.inventory.head.id], SaveLoad.inventory.head.level);
        else head.Empty();

        if (SaveLoad.inventory.cloak != null && SaveLoad.inventory.cloak.id >= 0)
            cloak.Set(DataBase.instance.items[SaveLoad.inventory.cloak.id], SaveLoad.inventory.cloak.level);
        else cloak.Empty();

        for (int i = 0; i < SaveLoad.inventory.weapons.Length; i++)
        {
            weapons.Add(new InventorySlot());
            weapons[i].Set(DataBase.instance.items[SaveLoad.inventory.weapons[i].id], SaveLoad.inventory.weapons[i].level);
        }

        for (int i = 0; i < SaveLoad.inventory.chests.Length; i++)
        {
            chests.Add(new InventorySlot());
            chests[i].Set(DataBase.instance.items[SaveLoad.inventory.chests[i].id], SaveLoad.inventory.chests[i].level);
        }

        for (int i = 0; i < SaveLoad.inventory.heads.Length; i++)
        {
            heads.Add(new InventorySlot());
            heads[i].Set(DataBase.instance.items[SaveLoad.inventory.heads[i].id], SaveLoad.inventory.heads[i].level);
        }

        for (int i = 0; i < SaveLoad.inventory.cloaks.Length; i++)
        {
            cloaks.Add(new InventorySlot());
            cloaks[i].Set(DataBase.instance.items[SaveLoad.inventory.cloaks[i].id], SaveLoad.inventory.cloaks[i].level);
        }

        newItems = SaveLoad.inventory.newItems;
    }

    void Save()
    {
        SaveData data = new SaveData();

        data.weapon = weapon.IsEmpty() ? new ItemSave(-1, 0) : new ItemSave(weapon.GetData().id, weapon.GetLevel());
        data.chest = chest.IsEmpty() ? new ItemSave(-1, 0) : new ItemSave(chest.GetData().id, chest.GetLevel());
        data.head = head.IsEmpty() ? new ItemSave(-1, 0) : new ItemSave(head.GetData().id, head.GetLevel());
        data.cloak = cloak.IsEmpty() ? new ItemSave(-1, 0) : new ItemSave(cloak.GetData().id, cloak.GetLevel());

        data.weapons = new ItemSave[weapons.Count];
        for (int i = 0; i < data.weapons.Length; i++)
        {
            data.weapons[i] = new ItemSave(weapons[i].GetData().id, weapons[i].GetLevel());
        }

        data.chests = new ItemSave[chests.Count];
        for (int i = 0; i < data.chests.Length; i++)
        {
            data.chests[i] = new ItemSave(chests[i].GetData().id, chests[i].GetLevel());
        }

        data.heads = new ItemSave[heads.Count];
        for (int i = 0; i < data.heads.Length; i++)
        {
            data.heads[i] = new ItemSave(heads[i].GetData().id, heads[i].GetLevel());
        }

        data.cloaks = new ItemSave[cloaks.Count];
        for (int i = 0; i < data.cloaks.Length; i++)
        {
            data.cloaks[i] = new ItemSave(cloaks[i].GetData().id, cloaks[i].GetLevel());
        }

        data.newItems = newItems;

        SaveLoad.inventory = data;
        SaveLoad.SaveProgress();
    }

    public List<PerkEffect> GetPerks()
    {
        List<PerkEffect> effects = new List<PerkEffect>();
        SetProcessor setProcessor = new SetProcessor();

        PerkEffect effect;
        if (!weapon.IsEmpty())
        {
            effect = weapon.GetEffect();
            effect.multiplier *= PerksManager.instance.Get("SwordBuff");
            effects.Add(effect);

            if (weapon.GetData().setItem)
                setProcessor.Add(weapon.GetData().setName, weapon.GetData().set2Effect, weapon.GetData().set3Effect, weapon.GetData().set4Effect);
        }
        if (!chest.IsEmpty())
        {
            effect = chest.GetEffect();
            effect.multiplier *= PerksManager.instance.Get("ArmorBuff");
            effects.Add(effect);

            if (chest.GetData().setItem)
                setProcessor.Add(chest.GetData().setName, chest.GetData().set2Effect, chest.GetData().set3Effect, chest.GetData().set4Effect);
        }
        if (!head.IsEmpty())
        {
            effect = head.GetEffect();
            effect.multiplier *= PerksManager.instance.Get("HelmetBuff");
            effects.Add(effect);

            if (head.GetData().setItem)
                setProcessor.Add(head.GetData().setName, head.GetData().set2Effect, head.GetData().set3Effect, head.GetData().set4Effect);
        }
        if (!cloak.IsEmpty())
        {
            effect = cloak.GetEffect();
            effect.multiplier *= PerksManager.instance.Get("AuraBuff");
            effects.Add(effect);

            if (cloak.GetData().setItem)
                setProcessor.Add(cloak.GetData().setName, cloak.GetData().set2Effect, cloak.GetData().set3Effect, cloak.GetData().set4Effect);
        }

        effects.AddRange(setProcessor.GetEffects());
        return effects;
    }

    public void OpenWindow()
    {
        if (tab < 0)
            SetTab(2);
        else SetTab(tab);
    }

    public void SetTab(int tab)
    {
        GameUI.instance.soundContainer.PlaySound("click");

        this.tab = tab;
        newItems[tab] = false;

        for (int i = 0; i < tabs.Length; i++)
        {
            tabs[i].SetActive(i == tab);
        }
    }

    public int GetItemCount()
    {
        return weapons.Count + heads.Count + cloaks.Count + chests.Count;
    }
}