﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySlot
{

    private Item data;
    private int level;
    private bool empty;

    public InventorySlot()
    {
        data = null;
        level = 0;
        empty = true;
    }

    public Item Empty()
    {
        empty = true;
        Item temp = data;
        data = null;
        level = 0;
        return temp;
    }

    public void Set(Item data, int level)
    {
        this.data = data;
        this.level = level;
        empty = false;
    }

    public bool IsEmpty()
    {
        return empty;
    }

    public Item GetData()
    {
        return data;
    }

    public int GetLevel()
    {
        return level;
    }

    public void LevelUp()
    {
        level++;
    }

    public PerkEffect GetEffect()
    {
        PerkEffect effect = new PerkEffect();
        effect.effect = data.effect.effect;
        float mult = data.effect.multiplier;
        bool larg = mult > 1;
        if (larg)
        {
            mult -= 1;
        }
        mult *= (level + 1);
        if (larg)
            mult++;
        effect.multiplier = mult;
        return effect;
    }

    public Inventory.ItemSave GetSaveData()
    {
        return new Inventory.ItemSave(data.id, level);
    }
}