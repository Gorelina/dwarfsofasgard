﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlotUI : MonoBehaviour
{

    [Header("Common")]
    public Image bg;
    public Image icon;
    public Text level;
    [Header("FullSize")]
    public bool fullSize;
    new public Text name;
    public Text effect;
    public Text rarity;
    public Text set;
    public Text setEffect;
    public GameObject disassembleButton;
    public Button disassembleButtonLogic;
    public GameObject equipButton;
    public Button equipButtonLogic;
    public GameObject upgradeButton;
    public Button upgradeButtonLogic;
    public Text upgradeButtonText;
    private Item data;
    private bigNumber price = new bigNumber();
    private int setIndex = -1;

    void Start()
    {
        InvokeRepeating("UpdateSetText", 0, 2.5f);
    }

    private void Update()
    {
        if (!gameObject.activeSelf)
            return;

        if (!fullSize)
            return;

        switch (data.rarity)
        {
            case Item.Rarity.Common:
                upgradeButtonLogic.interactable = Mining.instance.GetCurrency().iron >= price;
                break;

            case Item.Rarity.Rare:
                upgradeButtonLogic.interactable = Mining.instance.GetCurrency().silver >= price;
                break;

            case Item.Rarity.Epic:
                upgradeButtonLogic.interactable = Mining.instance.GetCurrency().gold >= price;
                break;

            default:
                upgradeButtonLogic.interactable = Mining.instance.GetCurrency().platinum >= price;
                break;
        }
    }

    void UpdateSetText()
    {
        if (!gameObject.activeSelf)
            return;

        if (!data)
            return;

        if (!data.setItem)
        {
            CancelInvoke();
            return;
        }

        if (!fullSize)
            return;

        setIndex++;
        if (setIndex >= 3)
            setIndex = 0;

        switch(setIndex)
        {
            case 0:
                setEffect.text = "2 " + LanguageManager.ReturnString("ITM") + ":   " + string.Format(PerksManager.instance.Description(data.set2Effect.effect), data.set2Effect.multiplier);
                break;

            case 1:
                setEffect.text = "3 " + LanguageManager.ReturnString("ITM") + ":   " + string.Format(PerksManager.instance.Description(data.set3Effect.effect), data.set3Effect.multiplier);
                break;

            case 2:
                setEffect.text = "4 " + LanguageManager.ReturnString("ITM") + ":   " + string.Format(PerksManager.instance.Description(data.set4Effect.effect), data.set4Effect.multiplier);
                break;
        }
    }

    public void Set(Item data, int level, Sprite bgImage, Color textColor, bool fullSize, bool equipped, Inventory inventory = null, InventorySlot slot = null)
    {
        this.data = data;
        icon.sprite = data.icon.sprite;
        this.fullSize = fullSize;
        this.level.text = level > 0 ? (level + 1).ToString() + " " + LanguageManager.ReturnString("LVL") : "";
        if (this.fullSize)
        {
            bg.sprite = bgImage;
            name.text = LanguageManager.ReturnString(data.name);
            name.color = textColor;
            effect.text = string.Format(PerksManager.instance.Description(slot.GetEffect().effect), slot.GetEffect().multiplier);
            rarity.text = LanguageManager.ReturnString("ITEM_RARITY_" + (int)data.rarity);
            rarity.color = textColor;
            if (data.setItem)
                set.text = LanguageManager.ReturnString(data.setName);
            disassembleButton.SetActive(!equipped);
            equipButton.SetActive(!equipped);
            upgradeButton.SetActive(equipped);
            price = Formules.GetUpgradePrice(level);
            string rsc;
            switch (data.rarity)
            {
                case Item.Rarity.Common:
                    rsc = "IRON";
                    break;

                case Item.Rarity.Rare:
                    rsc = "SILVER";
                    break;

                case Item.Rarity.Epic:
                    rsc = "GOLD";
                    break;

                default:
                    rsc = "PLATINUM";
                    break;
            }
            upgradeButtonText.text = LanguageManager.ReturnString("UPGRADE") + "\n" + price + "\n" + LanguageManager.ReturnString(rsc);

            upgradeButtonLogic.onClick.AddListener(delegate { inventory.Upgrade(slot); });
            disassembleButtonLogic.onClick.AddListener(delegate { inventory.Disassemble(slot); });
            equipButtonLogic.onClick.AddListener(delegate { inventory.Equip(slot); });
        }
    }

    public void Set(Sprite bgImage, int level, Sprite icon)
    {
        bg.sprite = bgImage;
        this.level.text = level > 0 ? (level + 1).ToString() + " " + LanguageManager.ReturnString("LVL") : "";
        this.icon.sprite = icon;
    }
}