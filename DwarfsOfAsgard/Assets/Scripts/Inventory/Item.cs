﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Item", menuName = "General/Item")]
public class Item : DBEntry
{
    public SpriteImage icon;
    public enum Types {Head, Chest, PickAxe, Cloak };
    public Types type;
    public bool setItem;
    public string setName;
    public enum Rarity {Common, Rare, Epic, Legendary };
    public Rarity rarity;
    public PerkEffect effect;
    public PerkEffect set2Effect;
    public PerkEffect set3Effect;
    public PerkEffect set4Effect;
}