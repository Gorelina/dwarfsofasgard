﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour
{

    public struct LeaderboardBase
    {
        public string nickname;
        public string value;

        public LeaderboardBase(string nickname, string value)
        {
            this.nickname = nickname;
            this.value = value;
        }
    }

    public GameObject leaderboardWindow;
    public LeaderboardEntry playerEntry;
    public Sprite playerEntrySprite;
    public Transform leaderboardHolder;
    public Transform leaderboardEntry;
    [Space]
    public Image[] tabButtons;
    public Sprite tabNormal;
    public Sprite tabSelected;
    private LeaderboardBase[] entries;
    private int userPosition;

    public void OpenLeaderboard()
    {
        GameUI.instance.soundContainer.PlaySound("click");
        SelectTab(0);
        leaderboardWindow.SetActive(true);
    }

    public void SelectTab(int index)
    {
        GameUI.instance.soundContainer.PlaySound("click");

        for (int i = 0; i < tabButtons.Length; i++)
        {
            tabButtons[i].sprite = i == index ? tabNormal : tabSelected;
        }

        switch (index)
        {
            case 0:
                NetworkManager.instance.GetLeaderboardData("totalLevel", LoadLeaderboardLevels);
                break;

            case 1:
                NetworkManager.instance.GetLeaderboardData("gold", LoadLeaderboardGold);
                break;

            case 2:
                NetworkManager.instance.GetLeaderboardData("levelsPassed", LoadLeaderboardPassed);
                break;
        }
    }

    void LoadLeaderboardGold(LeaderboardOutput leaderboard)
    {
        if (leaderboard.leaderboard == null)
            return;

        userPosition = int.Parse(leaderboard.userPosition);

        entries = new LeaderboardBase[leaderboard.leaderboard.Length];
        for (int i = 0; i < entries.Length; i++)
        {
            entries[i] = new LeaderboardBase(leaderboard.leaderboard[i].nickname, leaderboard.leaderboard[i].gold);
        }

        ShowLeaderboard("gold");
    }

    void LoadLeaderboardPassed(LeaderboardOutput leaderboard)
    {
        if (leaderboard.leaderboard == null)
            return;

        userPosition = int.Parse(leaderboard.userPosition);

        entries = new LeaderboardBase[leaderboard.leaderboard.Length];
        for (int i = 0; i < entries.Length; i++)
        {
            entries[i] = new LeaderboardBase(leaderboard.leaderboard[i].nickname, leaderboard.leaderboard[i].levelsPassed);
        }

        ShowLeaderboard("levelsPassed");
    }

    void LoadLeaderboardLevels(LeaderboardOutput leaderboard)
    {
        if (leaderboard.leaderboard == null)
            return;

        userPosition = int.Parse(leaderboard.userPosition);

        entries = new LeaderboardBase[leaderboard.leaderboard.Length];
        for (int i = 0; i < entries.Length; i++)
        {
            entries[i] = new LeaderboardBase(leaderboard.leaderboard[i].nickname, leaderboard.leaderboard[i].totalLevel);
        }

        ShowLeaderboard("totalLevel");
    }

    void ShowLeaderboard(string sortBy)
    {
        for (int i = 0; i < leaderboardHolder.childCount; i++)
        {
            Destroy(leaderboardHolder.GetChild(i).gameObject);
        }

        int iconIndex = LeaderboardSortIndex(sortBy);

        playerEntry.gameObject.SetActive(false);
        int playerPos = userPosition;
        if (playerPos > entries.Length)
        {
            playerEntry.gameObject.SetActive(true);
            playerEntry.number.text = playerPos.ToString();
            playerEntry.name.text = SaveLoad.nickname;
            switch (sortBy)
            {
                case "gold":
                    playerEntry.gold.text = SaveLoad.wholeGold;
                    break;

                case "levelsPassed":
                    playerEntry.gold.text = (SaveLoad.levelIndex + 1).ToString();
                    break;

                case "totalLevel":
                    playerEntry.gold.text = (SaveLoad.playerLevel + 1).ToString();
                    break;
            }
            for (int i = 0; i < playerEntry.icons.Length; i++)
            {
                playerEntry.icons[i].SetActive(i == iconIndex);
            }
        }

        LeaderboardEntry[] leaderboardEntries = new LeaderboardEntry[entries.Length];

        Transform clone;
        for (int i = 0; i < leaderboardEntries.Length; i++)
        {
            clone = Instantiate(leaderboardEntry, leaderboardHolder);
            leaderboardEntries[i] = clone.GetComponent<LeaderboardEntry>();
            leaderboardEntries[i].number.text = (i + 1).ToString();
            leaderboardEntries[i].name.text = entries[i].nickname;
            if (entries[i].nickname == SaveLoad.nickname)
                leaderboardEntries[i].bg.sprite = playerEntrySprite;
            leaderboardEntries[i].gold.text = entries[i].value;

            for (int c = 0; c < playerEntry.icons.Length; c++)
            {
                leaderboardEntries[i].icons[c].SetActive(c == iconIndex);
            }
        }
    }

    public void CloseLeaderboard()
    {
        GameUI.instance.soundContainer.PlaySound("click");

        for (int i = 0; i < leaderboardHolder.childCount; i++)
        {
            Destroy(leaderboardHolder.GetChild(i).gameObject);
        }

        leaderboardWindow.SetActive(false);
    }

    int LeaderboardSortIndex(string sortBy)
    {
        switch (sortBy)
        {
            case "totalLevel":
                return 0;

            case "gold":
                return 1;

            case "levelsPassed":
                return 2;

            default:
                return 1;
        }
    }

    string LeaderboardSortName(int index)
    {
        switch (index)
        {
            case 0:
                return "totalLevel";

            case 1:
                return "gold";

            case 2:
                return "levelsPassed";

            default:
                return "gold";
        }
    }
}