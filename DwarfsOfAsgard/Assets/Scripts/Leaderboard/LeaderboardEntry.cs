﻿using UnityEngine;
using UnityEngine.UI;

public class LeaderboardEntry : MonoBehaviour
{

    public Image bg;
    public Text number;
    new public Text name;
    public Text gold;
    /// <summary>
    /// 0 - playerLevel, 1 - gold, 2 - levels
    /// </summary>
    public GameObject[] icons;

}