﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{

    public SpriteRenderer spriteRenderer;
    public Sprite[] sprites;
    public Effect[] effects;
    public static Rock instance;

    void Awake()
    {
        instance = this;
    }

    public void Set(float hp)
    {
        int i = 0;

        if (hp <= 0.66f && hp > 0.33f)
            i = 1;
        else if (hp <= 0.33f)
            i = 2;

        if (spriteRenderer.sprite == sprites[i])
            return;

        foreach (Effect effect in effects)
            effect.Play();
        spriteRenderer.sprite = sprites[i];
    }
}