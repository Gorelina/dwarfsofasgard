﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class LanguageManager : MonoBehaviour
{

    public TextAsset languageFile;
    private static string language;
    private static Dictionary<string, Dictionary<string, string>> strings;
    private static LanguageManager instance;
    public delegate void OnReload();
    public event OnReload onReload;

    public static LanguageManager Instance()
    {
        return instance;
    }

    void Awake()
    {
        if (Instance() == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            strings = new Dictionary<string, Dictionary<string, string>>();
        }
        else DestroyImmediate(gameObject);
    }

    void Start()
    {
        Load();
    }

    void Load()
    {
        language = SaveLoad.language;
        strings.Clear();

        string data = languageFile.text;
        string[] lines = data.Split('\n');
        string[] keys = new string[0];
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].IndexOf((char)13) >= 0)
                lines[i] = lines[i].Remove(lines[i].IndexOf((char)13));

            if (lines[i].Length <= 2)
                continue;

            string[] lineData = Parse(lines[i]);
            if (i == 0)
            {
                keys = new string[lineData.Length - 1];
                for (int k = 1; k < lineData.Length; k++)
                {
                    keys[k - 1] = lineData[k];
                }
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                for (int l = 1; l < lineData.Length; l++)
                {
                    dic.Add(keys[l - 1], lineData[l]);
                }
                strings.Add(lineData[0], dic);
            }
        }
    }

    public static string[] Parse(string str)
    {
        List<string> result = new List<string>();
        int startIndex = 0;
        bool ignore = false;

        for (int c = 0; c < str.Length; c++)
        {
            if (c == str.Length - 1)
            {
                if (!ignore)
                    result.Add(str.Substring(startIndex));
                else result.Add(str.Substring(startIndex, c - startIndex));
                break;
            }

            if (str[c] == '"')
            {
                if (!ignore)
                    startIndex = c + 1;

                ignore = !ignore;

                if (!ignore)
                {
                    result.Add(str.Substring(startIndex, c - startIndex));
                    startIndex = c + 1;
                }
            }

            if (str[c] == ',' && !ignore && c - startIndex > 0)
            {
                result.Add(str.Substring(startIndex, c - startIndex));
                startIndex = c + 1;
            }
        }
        return result.ToArray();
    }

    public static void Reload()
    {
        Instance().Load();
        if (Instance().onReload != null)
            Instance().onReload.Invoke();
    }

    public static string ReturnString(string id)
    {
        try
        {
            return strings[id][language];
        }
        catch (System.Exception e)
        {
            Debug.Log(id + " - " + e.Message);
            return id;
        }
    }

    public static bool IsLoaded()
    {
        return strings != null;
    }

    public static string GetLanguage()
    {
        return language;
    }
}