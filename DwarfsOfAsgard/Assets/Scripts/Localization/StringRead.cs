﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StringRead : MonoBehaviour
{

    public string id;
    public enum CaseTypes { None, Lower, Upper };
    public CaseTypes caseType = CaseTypes.None;
    public string prefix;
    public string postfix;
    [System.NonSerialized]
    public Text textComponent;

    void Awake()
    {
        textComponent = GetComponent<Text>();
    }

    void OnEnable()
    {
        LanguageManager.Instance().onReload += Reload;
        Reload();
    }

    void OnDisable()
    {
        LanguageManager.Instance().onReload -= Reload;
    }

    public void Change(string _id)
    {
        id = _id;
        Reload();
    }

    public void Change(string _id, string _postfix)
    {
        id = _id;
        postfix = _postfix;
        Reload();
    }

    public void Change(string _id, string _prefix, string _postfix)
    {
        id = _id;
        prefix = _prefix;
        postfix = _postfix;
        Reload();
    }

    public void Reload()
    {
        if (textComponent)
        {
            string text = prefix + LanguageManager.ReturnString(id) + postfix;

            if (caseType == CaseTypes.Lower)
            {
                text = text.ToLower();
            }
            else if (caseType == CaseTypes.Upper)
            {
                text = text.ToUpper();
            }

            textComponent.text = text;
        }
    }
}