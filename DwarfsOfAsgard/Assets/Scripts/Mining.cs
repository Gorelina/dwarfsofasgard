﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mining : MonoBehaviour
{
    [System.Serializable]
    public struct DetailLine
    {
        public SpriteRenderer[] details;
    }

    public Hero hero;
    public SpriteRenderer[] lines;
    public Sprite[] details;
    public DetailLine[] detailLines;
    public Transform[] bosses;
    public Transform chestPrefab;
    public Transform bossSpawnPoint;
    public Transform rockSpawnPoint;
    public SpriteRenderer[] smallRocks;
    public SpriteRenderer background;
    public SpriteRenderer[] shadows;
    public Image foreground;
    public SpriteRenderer[] trolleys;
    public EffectAnchor platformDestroy;
    public Animator transitionEffect;
    public Text transitionName;
    public Image transitionIcon;
    public Text transitionAdvice;
    public Effect tapEffect;
    public Effect tapCriticalEffect;
    public Transform prestigeEffect;
    [Header("Coin")]
    public Transform coinSpawnPoint;
    public Transform coinPrefab;
    private int diamonds;
    private Drop currency;
    private bigNumber wholeGold;
    private bigNumber coins;
    private bigNumber health;
    private bigNumber maxHealth;
    private bigNumber mainQuantity;
    private bigNumber coinQuantity;
    private Level currentLevel;
    private int levelIndex;
    private int levelId;
    private int maxBlocks;
    private int blockIndex;
    private int visualIndex;
    private bool fightingBoss;
    private bool chest;
    private float bossTimer;
    private bool lost;
    private Animator boss;
    private BossSkill activeSkill;
    private bigNumber heroDamage;
    private bool discount;
    private float discountTime;
    public float discountMult { get; private set; } = 0.5f;
    private float delay;

    private const int minPrestigeHeroLevel = 500;

    //Test
    [Header("Multiskip")]
    public GameObject multiskipGUI;
    public Text realTime;
    public int[] clickCounts;
    public Slider clickCount;
    public Text clickCountText;
    public Text wastedTimeText;
    public Text entryExitTimeText;
    public Text bossText;
    public Toggle unlimitedMoneyToggle;
    public Toggle disableHelpers;
    public Toggle bossMultiplier;
    public Text progress;
    public GameObject statsWindow;
    public Text stats;
    private int testerClicks;
    private float wastedTime;
    //Test

    public static Mining instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        hero.SetLevel(SaveLoad.playerLevel);
        for (int i = 0; i < SaveLoad.skillLevels.Length; i++)
        {
            hero.SetSkill(i, SaveLoad.skillLevels[i]);
        }
        hero.LoadMana();
        for (int i = 0; i < SaveLoad.helpersLevels.Length; i++)
        {
            HelpersController.instance.SetLevel(i, SaveLoad.helpersLevels[i], SaveLoad.helpersSkills[i]);
        }
        heroDamage = new bigNumber();
        SetDiamonds(SaveLoad.diamonds);
        currency = new Drop();
        currency.coins = new bigNumber(SaveLoad.gold);
        currency.coal = new bigNumber(SaveLoad.coal);
        currency.coal.SetAccurate();
        currency.copper = new bigNumber(SaveLoad.copper);
        currency.copper.SetAccurate();
        currency.gold = new bigNumber(SaveLoad.aurum);
        currency.gold.SetAccurate();
        currency.iron = new bigNumber(SaveLoad.iron);
        currency.iron.SetAccurate();
        currency.platinum = new bigNumber(SaveLoad.platinum);
        currency.platinum.SetAccurate();
        currency.silver = new bigNumber(SaveLoad.silver);
        currency.silver.SetAccurate();
        currency.stone = new bigNumber(SaveLoad.stone);
        currency.stone.SetAccurate();
        coins = new bigNumber();
        wholeGold = new bigNumber(SaveLoad.wholeGold);
        coinQuantity = new bigNumber();
        mainQuantity = new bigNumber();
        levelIndex = SaveLoad.levelIndex;
        blockIndex = 0;
        ReloadLevel();
        UpdateVisuals();
        GameUI.instance.SetResources(currency);
        activeSkill = DataBase.instance.bossSkills[0];

        InvokeRepeating("HeroDPS", 0.25f, 0.25f);
        InvokeRepeating("UpdateCoins", Time.deltaTime, 1);
    }

    void HeroDPS()
    {
        heroDamage = bigNumber.Lerp(heroDamage, 0, 0.5f);

        if (heroDamage.number <= 0 && heroDamage.prefixIndex <= 0)
            heroDamage.ToZero();
    }

    //Reload entire level after all blocks has been destroyed
    void ReloadLevel()
    {
        currentLevel = DataBase.instance.GetLevel(levelIndex);
        if (levelId != currentLevel.id && levelIndex > 0 && Time.timeSinceLevelLoad > 5)
        {
            transitionEffect.SetTrigger("Play");
            transitionEffect.GetComponent<AudioSource>().Play();
            transitionName.text = LanguageManager.ReturnString(currentLevel.name).ToUpper();
            transitionIcon.sprite = currentLevel.icons[Random.Range(0, currentLevel.icons.Length)];
            transitionAdvice.text = LanguageManager.ReturnString(Advicer.GetAdvice());
            delay += 2.5f;
        }
        levelId = currentLevel.id;
        blockIndex = 1;
        maxBlocks = 8;
        if (Time.timeScale > 0)
        {
            if (Time.timeSinceLevelLoad <= 5)
            {
                DelayReload();
            }
            else Invoke("DelayReload", 0.5f);
        }
        else
        {
            DelayReload();
        }

        SaveLoad.levelIndex = levelIndex;
        SaveLoad.SaveProgress();
    }

    //Reload a block after its get destroyed
    void ReloadBlock()
    {
        if (!chest)
        {
            Formules.GetLevelNumbers(levelIndex, blockIndex, out bigNumber hp, out bigNumber rsc);
            /*if (rsc == 0)
                Debug.LogError(rsc.ToString());*/
            maxHealth = health = hp;
            if (blockIndex == maxBlocks)
            {
                rsc *= PerksManager.instance.Get("GoldBoss");
                maxHealth *= PerksManager.instance.Get("BossHP");
                health = maxHealth;

                mainQuantity.ToZero();
                coinQuantity = rsc;

                if (levelIndex >= 15)
                    activeSkill = DataBase.instance.bossSkills[Random.Range(1, DataBase.instance.bossSkills.Length)];
                InitBossSkill();
            }
            else
            {
                mainQuantity = rsc;
                coinQuantity.ToZero();
            }
        }
        else
        {
            Formules.GetChestNumbers(levelIndex, out bigNumber hp, out bigNumber rsc);
            maxHealth = health = hp;
            rsc *= PerksManager.instance.Get("ChestAmount");

            mainQuantity.ToZero();
            coinQuantity = rsc;
        }
    }

    //Update visuals - rock, small rocks and blocks
    void UpdateVisuals()
    {
        if (!fightingBoss)
        {
            if (rockSpawnPoint.childCount > 0)
            {
                for (int i = 0; i < rockSpawnPoint.childCount; i++)
                    Destroy(rockSpawnPoint.GetChild(i).gameObject);
            }
            Instantiate(currentLevel.rocks[Random.Range(0, currentLevel.rocks.Length)], rockSpawnPoint);

            Random.InitState((levelIndex + blockIndex) * 2);
            foreach (SpriteRenderer smallRock in smallRocks)
            {
                smallRock.sprite = currentLevel.smallRocks[Random.Range(0, currentLevel.smallRocks.Length)];
                smallRock.flipX = Random.Range(0, 2) == 0 ? true : false;
            }
        }

        for (int i = 0; i < shadows.Length; i++)
        {
            shadows[i].color = currentLevel.effectColors[i];
        }

        if (currentLevel.background != background.sprite)
        {
            background.sprite = currentLevel.background;
            foreground.sprite = currentLevel.foreground;
            for (int i = 0; i < currentLevel.trolleys.Length; i++)
            {
                trolleys[i].sprite = currentLevel.trolleys[i];
            }
        }

        int lvls = lines.Length;
        int index = levelIndex;

        int test = 100;

        visualIndex = blockIndex;

        while (true)
        {
            int maxPossBlocks = 2 + index;
            int ilvls = lvls;
            for (int i = lines.Length - ilvls; i < lines.Length; i++)
            {
                if (i < lines.Length)
                {
                    lines[i].sprite = currentLevel.GetSprite(visualIndex, out bool flip);

                    int count = 0;
                    List<int> detailIndex = new List<int>();
                    Random.InitState(levelIndex * 10 + visualIndex);
                    for (int d = 0; d < detailLines[i].details.Length; d++)
                    {
                        if (!currentLevel.blockDetails)
                        {
                            detailLines[i].details[d].gameObject.SetActive(false);
                            continue;
                        }

                        if (!detailLines[i].details[d])
                            continue;

                        if (count > 3)
                            continue;

                        count++;
                        detailLines[i].details[d].gameObject.SetActive(false);
                        bool show = Random.Range(0, 8) == 1 ? true : false;
                        if (show)
                        {
                            detailLines[i].details[d].gameObject.SetActive(show);
                            int rand = Random.Range(0, details.Length);
                            while (true)
                            {
                                if (detailIndex.Contains(rand))
                                    rand = Random.Range(0, details.Length);
                                else
                                {
                                    detailIndex.Add(rand);
                                    break;
                                }
                            }
                            detailLines[i].details[d].sprite = details[rand];
                            detailLines[i].details[d].transform.eulerAngles = Vector3.forward * Random.Range(0, 360);
                        }
                    }

                    visualIndex++;
                    lines[i].flipX = flip;
                    lvls--;
                }
            }

            index++;
            test--;

            if (test <= 0)
                break;

            if (lvls <= 0)
                break;
        }

        Random.InitState((int)System.DateTime.Now.Ticks);
    }

    void Update()
    {
        if (delay > 0)
            delay -= Time.deltaTime;

        if (fightingBoss)
        {
            if (bossTimer > 0)
                bossTimer -= Time.deltaTime;
            else
            {
                Destroy(boss.gameObject);
                GameUI.instance.bossSkillHolder.SetActive(false);
                if (rockSpawnPoint.childCount > 0)
                {
                    for (int i = 0; i < rockSpawnPoint.childCount; i++)
                        Destroy(rockSpawnPoint.GetChild(i).gameObject);
                }
                Instantiate(currentLevel.rocks[Random.Range(0, currentLevel.rocks.Length)], rockSpawnPoint);

                if (!chest)
                {
                    lost = true;
                    fightingBoss = false;
                    GameUI.instance.bossButton.SetActive(true);
                    GameUI.instance.leaveButton.SetActive(false);    
                    blockIndex = 0;
                    ReloadLevel();
                }
                else
                {
                    fightingBoss = false;
                    Destroy(boss.gameObject);
                    GameUI.instance.bossSkillHolder.SetActive(false);
                    if (rockSpawnPoint.childCount > 0)
                    {
                        for (int i = 0; i < rockSpawnPoint.childCount; i++)
                            Destroy(rockSpawnPoint.GetChild(i).gameObject);
                    }
                    Instantiate(currentLevel.rocks[Random.Range(0, currentLevel.rocks.Length)], rockSpawnPoint);
                }

                ReloadLevel();
            }

            GameUI.instance.bossBar.value = bossTimer / (30f * PerksManager.instance.Get("BossTimer"));
            GameUI.instance.bossTimerText.text = bossTimer.ToString("F1");
        }

        UpdateBossSkill();

        GameUI.instance.bossFire.SetActive(fightingBoss && !chest);

        if (discountTime > 0)
        {
            discountTime -= Time.deltaTime;
        }
        else if (discount)
        {
            discount = false;
            GameUI.instance.UpdateHelpersTab();
            GameUI.instance.UpdateCharacterTab();
        }
        GameUI.instance.discountHolder.SetActive(discount);
        if (discount)
            GameUI.instance.discountTimer.fillAmount = discountTime / 30f;

        GameUI.instance.bossTimer.SetActive(fightingBoss);
        GameUI.instance.healthbarImage.sprite = fightingBoss ? GameUI.instance.bossBarSprite : GameUI.instance.healthbarSprite;
        GameUI.instance.healthbarIcon.sprite = fightingBoss ? GameUI.instance.bossIcon : GameUI.instance.hearthIcon;
        GameUI.instance.blocks.gameObject.SetActive(!lost);
        GameUI.instance.prestigeButton.interactable = CanGetPrestige();

#if UNITY_EDITOR
        #region Test
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            OpenMultiskip();
        }

        if (Input.GetKeyUp(KeyCode.Alpha6))
            SetDiscount();

        if (multiskipGUI.activeSelf)
        {
            System.TimeSpan timeStart = System.TimeSpan.FromSeconds(Time.time);
            realTime.text = timeStart.ToString("dd':'hh':'mm':'ss'.'fff");
            clickCountText.text = "x" + clickCounts[(int)clickCount.value];
            wastedTime = testerClicks / 7f;
            System.TimeSpan time = System.TimeSpan.FromSeconds(wastedTime);
            wastedTimeText.text = time.ToString("dd':'hh':'mm':'ss'.'fff") + "   clicks: " + testerClicks;
            entryExitTimeText.text = "Entry time: " + SaveLoad.entryTime + "\nExit time: " + SaveLoad.exitTime + "\nTimespan: " + SaveLoad.timeSpan + "\nWAU: " + SaveLoad.wauTime + "  " + WAU.instance.GetSpan().ToString();

            if (fightingBoss)
            {
                bigNumber timeToFight = maxHealth / ((Hero.instance.GetPureDamage() * 7f) + HelpersController.instance.GetDPS());
                bool possible = timeToFight <= 30f;
                bossText.text = "Fighting boss:\nPossible to kill = " + possible + "\nTime to kill = " + timeToFight.ToString();
            }
            else
            {
                bossText.text = "No boss or chest";
            }

            progress.text = "Helpers progres: " + HelpersController.instance.GetProgress() + "%";

            if (statsWindow.activeSelf)
            {
                stats.text = PerksManager.instance.GetInfo();
            }
        }
        #endregion
#endif

        GameUI.instance.SetMining(levelIndex, health, maxHealth, blockIndex, maxBlocks);
        GameUI.instance.SetDamages(hero.GetFinalDamage(), HelpersController.instance.GetDPS(), heroDamage + HelpersController.instance.GetDPS());
    }

#if UNITY_EDITOR
    public void OpenMultiskip()
    {
        multiskipGUI.SetActive(!multiskipGUI.activeSelf);
    }

    public void OpenStats()
    {
        statsWindow.SetActive(!statsWindow.activeSelf);
    }
#endif

    public void Tap()
    {
        if (hero.busy || delay > 0)
            return;

        if (PerksManager.instance.Get("ManaTap") > 0)
            Hero.instance.AddMana(PerksManager.instance.Get("ManaTap"));

        AchievementManager.instance.Increment("ACHIEVEMENT_TAPS");
        bigNumber damage = new bigNumber(hero.GetActualDamage(out bool critical));
        if (critical)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos += Random.insideUnitSphere * 0.3f;
            pos.z = 0;
            tapCriticalEffect.transform.position = pos;
            tapCriticalEffect.transform.eulerAngles = Vector3.forward * Random.Range(0, 360);
            tapCriticalEffect.Play();
            CameraScript.instance.Critical();
        }
        else
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos += Random.insideUnitSphere * 0.3f;
            pos.z = 0;
            tapEffect.transform.position = pos;
            tapEffect.transform.eulerAngles = Vector3.forward * Random.Range(0, 360);
            tapEffect.Play();
        }
        hero.Activate();
        GameUI.instance.ShowPopup(Camera.main.WorldToScreenPoint(hero.transform.position + Vector3.up), new PopupData((new bigNumber(clickCounts[(int)clickCount.value]) * damage).ToString(), critical ? Color.yellow : Color.red, critical ? 1.5f : 1));
        for (int i = 0; i < clickCounts[(int)clickCount.value]; i++)
        {
            testerClicks++;

            Damage(damage, DamageSource.Tap, DamageTypes.Hero);
            heroDamage += damage;

            hero.ShowAttack();
        }
    }

    public void Damage(bigNumber _damage, DamageSource damageSource, DamageTypes damageType)
    {
        if (delay > 0)
            return;

        if (boss)
            _damage *= PerksManager.instance.Get("BossDamage");
        else _damage *= PerksManager.instance.Get("OreDamage");

        if (BossSkillCheck())
        {
            switch (activeSkill.effect)
            {
                case "tank":
                    if (damageSource == DamageSource.Tap)
                        _damage *= activeSkill.multiplier;
                    break;

                case "bulletproof":
                    if (damageSource == DamageSource.Helper)
                        _damage *= activeSkill.multiplier;
                    break;

                case "jaggernaut":
                    if (damageSource == DamageSource.Skill)
                        _damage *= activeSkill.multiplier;
                    break;

                case "egoist":
                    if (damageType == DamageTypes.Melee)
                        _damage *= activeSkill.multiplier;
                    break;

                case "bore":
                    if (damageType == DamageTypes.Range)
                        _damage *= activeSkill.multiplier;
                    break;

                case "titan":
                    _damage *= activeSkill.multiplier;
                    break;
            }
        }

        health -= _damage;

        if (BossSkillCheck() && health.prefixIndex <= 0 && Mathf.Round(health.number) <= 0)
        {
            if (activeSkill.effect == "immortal")
            {
                health += maxHealth * activeSkill.multiplier;
                activeSkill = DataBase.instance.bossSkills[0];
            }

            if (activeSkill.effect == "resurrected")
            {
                health = maxHealth;
                activeSkill = DataBase.instance.bossSkills[0];
            }
        }

        if (Rock.instance != null)
        {
            Rock.instance.Set((health / maxHealth).number);
        }

        if (boss)
            boss.SetTrigger("Hit" + Random.Range(1, 3));

        if (health.prefixIndex <= 0 && Mathf.Round(health.number) <= 0)
        {
            platformDestroy.PlayAllEffects();
            blockIndex++;

            bool bonus = Formules.CalculateChance(PerksManager.instance.Get("MoneyBonusChance"));
            if (bonus)
                GameUI.instance.ShowPopup(Camera.main.WorldToScreenPoint(Hero.instance.transform.position + Vector3.up * 2), new PopupData(LanguageManager.ReturnString("MONEY_BONUS"), Color.yellow));

            coins += coinQuantity * (Hero.instance.skillInstances[2].active ? Hero.instance.skillInstances[2].GetValue() : 1) * (bonus ? 10 : 1);
            CoinsEffect(coinQuantity);

            currency += currentLevel.GetDrop(mainQuantity, levelIndex) * (bonus ? 10 : 1);
            SaveResources();
            GameUI.instance.SetResources(currency);

            if (blockIndex == maxBlocks)
            {
                if (lost)
                {
                    blockIndex = 1;

                    delay = 0.75f;
                    Invoke("DelayReload", 0.5f);
                }
                else
                {
                    StartBoss();

                    delay = 0.75f;
                }
            }
            else if (blockIndex > maxBlocks)
            {
                blockIndex = 1;
                if (boss)
                {
                    boss.SendMessage("Die");
                    Destroy(boss.gameObject);
                    GameUI.instance.bossSkillHolder.SetActive(false);
                    AchievementManager.instance.Increment("ACHIEVEMENT_BOSSES");
                    if ((levelIndex > 8 && Formules.CalculateChance(10)) || levelIndex == 8)
                    {
                        Item.Rarity rarity = Item.Rarity.Common;
                        if (Formules.CalculateChance(1))
                        {
                            rarity = Item.Rarity.Legendary;
                        }
                        else if (Formules.CalculateChance(2))
                        {
                            rarity = Item.Rarity.Epic;
                        }
                        if (Formules.CalculateChance(50))
                        {
                            rarity = Item.Rarity.Rare;
                        }

                        Inventory.instance.AddRandom(rarity);
                    }

                    if (levelIndex == 38)
                    {
                        SkillTree.instance.AddPerkPoints(1);
                    }
                }
                fightingBoss = false;
                lost = false;
                GameUI.instance.bossButton.SetActive(false);
                GameUI.instance.leaveButton.SetActive(false);

                Dictionary<string, object> options = new Dictionary<string, object>() {
                    {"LevelIndex", levelIndex + 1}
                };
                Amplitude.Instance.logEvent("level_complete", options);
                levelIndex++;

                AchievementManager.instance.Increment("ACHIEVEMENT_LEVELS");

                delay = 0.75f;
                ReloadLevel();
            }
            else
            {
                if (chest)
                {
                    boss.SendMessage("Die");
                    Destroy(boss.gameObject);
                    fightingBoss = false;
                    lost = false;
                    chest = false;
                    AchievementManager.instance.Increment("ACHIEVEMENT_CHESTS");
                }
                else if (Formules.CalculateChance(PerksManager.instance.Get("ChestChance")))
                {
                    chest = true;
                    StartBoss();
                }

                AchievementManager.instance.Increment("ACHIEVEMENT_ORE");
                delay = 0.75f;
                Invoke("DelayReload", 0.5f);
            }
        }
    }

    void DelayReload()
    {
        ReloadBlock();
        UpdateVisuals();
    }

    public void FightBoss()
    {
        StartBoss();
        UpdateVisuals();
    }

    public void LeaveBoss()
    {
        lost = true;
        fightingBoss = false;
        GameUI.instance.bossButton.SetActive(true);
        GameUI.instance.leaveButton.SetActive(false);
        Destroy(boss.gameObject);
        GameUI.instance.bossSkillHolder.SetActive(false);
        blockIndex = 0;
        ReloadBlock();
        UpdateVisuals();
    }

    void StartBoss()
    {
        CameraScript.instance.Shake();

        if (rockSpawnPoint.childCount > 0)
        {
            for (int i = 0; i < rockSpawnPoint.childCount; i++)
                Destroy(rockSpawnPoint.GetChild(i).gameObject);
        }
        Random.InitState(levelIndex * 5);

        Transform clone;
        if (!chest)
        {
            clone = Instantiate(bosses[Random.Range(0, bosses.Length)], bossSpawnPoint.position, Quaternion.identity);

            blockIndex = maxBlocks;
            
            GameUI.instance.bossButton.SetActive(false);
            GameUI.instance.leaveButton.SetActive(true);
        }
        else
        {
            clone = Instantiate(chestPrefab, bossSpawnPoint.position, Quaternion.identity);

            blockIndex--;
        }
        boss = clone.GetComponent<Animator>();
        fightingBoss = true;
        bossTimer = 30 * PerksManager.instance.Get("BossTimer");
        ReloadBlock();
    }

    public void ZeroClicks()
    {
        testerClicks = 0;
    }

    public int GetCurrentLevel()
    {
        return levelIndex;
    }

    public int GetCurrentBlock()
    {
        return blockIndex;
    }

#region Coins
    public void MultiplyCoins(float multiplier)
    {
        if (multiplier > 2)
            return;

        coins += currency.coins * multiplier;
    }

    public bigNumber GetCoins()
    {
        return currency.coins;
    }

    public bigNumber GetWholeGold()
    {
        return wholeGold;
    }

    public void AddCoins(bigNumber value)
    {
        CoinsEffect(value);

        coins += value;
    }

    void CoinsEffect(bigNumber quantity)
    {
        if (quantity >= 1)
        {
            int q = (quantity.prefixIndex + 1) * (int)Mathf.Clamp(quantity.number, 1, 10);
            for (int i = 0; i < q; i++)
            {
                Instantiate(coinPrefab, coinSpawnPoint.position, Quaternion.identity);
            }
        }
    }

    public bool ProcessPurchase(bigNumber cost)
    {
        if (unlimitedMoneyToggle.isOn)
            return true;

        if (currency.coins < cost)
            return false;

        currency.coins -= cost;
        UpdateCoins();
        return true;
    }

    public void UpdateCoins()
    {
        //actual variables
        currency.coins += coins;
        wholeGold += coins;
        coins.ToZero();

        //achievement
        AchievementManager.instance.Set("ACHIEVEMENT_GOLD", wholeGold);

        //UI
        GameUI.instance.SetCoins(currency.coins);

        //Save load
        NetworkManager.instance.UpdateUser();
    }

    public void SetCoins(bigNumber coins)
    {
        coins.ToZero();
        currency.coins = coins;

        //UI
        GameUI.instance.SetCoins(currency.coins);

        //Save load
        NetworkManager.instance.UpdateUser();
    }

    public void SetDiscount()
    {
        discount = true;
        discountTime = 30;
        GameUI.instance.UpdateHelpersTab();
        GameUI.instance.UpdateCharacterTab();
    }

    public bool GetDiscount()
    {
        return discount;
    }

    public void GetMoney()
    {
        coins += new bigNumber("1E+15");
    }

    public bigNumber GetCurrentLevelCoins()
    {
        return coinQuantity;
    }
#endregion

#region Resources
    public void SellResource(int type)
    {
        bigNumber q = new bigNumber();

        switch (type)
        {
            case 0:
                q = new bigNumber(currency.stone);
                if (q.number <= 0 && q.prefixIndex == 0)
                    return;
                currency.stone.ToZero();
                break;

            case 1:
                q = new bigNumber(currency.coal);
                if (q.number <= 0 && q.prefixIndex == 0)
                    return;
                currency.coal.ToZero();
                break;

            case 2:
                q = new bigNumber(currency.copper);
                if (q.number <= 0 && q.prefixIndex == 0)
                    return;
                currency.copper.ToZero();
                break;

            case 3:
                q = new bigNumber(currency.iron);
                if (q.number <= 0 && q.prefixIndex == 0)
                    return;
                currency.iron.ToZero();
                break;

            case 4:
                q = new bigNumber(currency.silver);
                if (q.number <= 0 && q.prefixIndex == 0)
                    return;
                currency.silver.ToZero();
                break;

            case 5:
                q = new bigNumber(currency.gold);
                if (q.number <= 0 && q.prefixIndex == 0)
                    return;
                currency.gold.ToZero();
                break;

            case 6:
                q = new bigNumber(currency.platinum);
                if (q.number <= 0 && q.prefixIndex == 0)
                    return;
                currency.platinum.ToZero();
                break;
        }

        AddCoins(Formules.ResourceToCoins(type, q));
        GameUI.instance.soundContainer.PlaySound("cash");
        GameUI.instance.SetResources(currency);
        SaveResources();
    }

    public bool CanBuyResources(Drop cost)
    {
        return currency >= cost;
    }

    public bool ProcessResources(int type, bigNumber cost)
    {
        switch (type)
        {
            case 0:
                if (currency.stone >= cost)
                    currency.stone -= cost;
                else
                    return false;
                break;

            case 1:
                if (currency.coal >= cost)
                    currency.coal -= cost;
                else
                    return false;
                break;

            case 2:
                if (currency.copper >= cost)
                    currency.copper -= cost;
                else
                    return false;
                break;

            case 3:
                if (currency.iron >= cost)
                    currency.iron -= cost;
                else
                    return false;
                break;

            case 4:
                if (currency.silver >= cost)
                    currency.silver -= cost;
                else
                    return false;
                break;

            case 5:
                if (currency.gold >= cost)
                    currency.gold -= cost;
                else
                    return false;
                break;

            case 6:
                if (currency.platinum >= cost)
                    currency.platinum -= cost;
                else
                    return false;
                break;
        }

        GameUI.instance.SetResources(currency);
        SaveResources();
        return true;
    }

    public Drop GetCurrency()
    {
        return currency;
    }

    void SaveResources()
    {
        SaveLoad.stone = currency.stone.GetString();
        SaveLoad.coal = currency.coal.GetString();
        SaveLoad.copper = currency.copper.GetString();
        SaveLoad.iron = currency.iron.GetString();
        SaveLoad.silver = currency.silver.GetString();
        SaveLoad.aurum = currency.gold.GetString();
        SaveLoad.silver = currency.silver.GetString();
        SaveLoad.SaveProgress();
    }
#endregion

#region BossSkills
    bool BossSkillCheck()
    {
        if (!fightingBoss || !boss || chest)
            return false;

        if (activeSkill.effect == "None")
            return false;

        return true;
    }

    void InitBossSkill()
    {
        if (!BossSkillCheck())
        {
            GameUI.instance.bossSkillHolder.SetActive(false);
            return;
        }        

        switch (activeSkill.effect)
        {
            case "strong":
                maxHealth *= activeSkill.multiplier;
                break;

            case "thickSkin":
                maxHealth *= activeSkill.multiplier;
                break;

            case "resurrected":
                maxHealth /= 2;
                health = maxHealth;
                break;
        }

        GameUI.instance.bossSkillHolder.SetActive(true);
        GameUI.instance.bossSkillIcon.sprite = activeSkill.icon.sprite;
        GameUI.instance.bossSkillName.text = LanguageManager.ReturnString(activeSkill.name.ToUpper());
    }

    void UpdateBossSkill()
    {
        if (!BossSkillCheck())
            return;

        switch(activeSkill.effect)
        {
            case "healthy":
                if (health < maxHealth)
                    health += maxHealth * activeSkill.multiplier * Time.deltaTime;
                break;

            case "vampire":
                if (!IsInvoking("Vampire"))
                    InvokeRepeating("Vampire", 1, 1);
                break;

        }
    }

    void Vampire()
    {
        if (activeSkill.effect != "vampire")
        {
            CancelInvoke("Vampire");
            return;
        }
        health += maxHealth * activeSkill.multiplier;
    }

    public void ShowBossSkill()
    {
        if (activeSkill.effect == "None")
            return;

        GameUI.instance.ShowWindow(LanguageManager.ReturnString(activeSkill.name.ToUpper() + "_DESC"), null, false, "OK");
    }
#endregion

#region Diamonds
    void SetDiamonds(int value)
    {
        diamonds = value;
        SaveDiamonds();
    }

    public bool ProcessDiamonds(int price)
    {
#if UNITY_EDITOR
        return true;
#else
        if (diamonds >= price)
        {
            diamonds -= price;
            SaveDiamonds();
            return true;
        }
        else
        {
            GameUI.instance.OpenTab(5);
            return false;
        }
#endif
    }

    public int GetDiamonds()
    {
        return diamonds;
    }

    public void AddDiamonds(int value)
    {
        if (value <= 0)
            return;

        GameUI.instance.soundContainer.PlaySound("diamonds");
        diamonds += value;
        SaveDiamonds();
    }

    void SaveDiamonds()
    {
        SaveLoad.diamonds = diamonds;
        SaveLoad.SaveProgress();
        UpdateDiamonds();
    }

    void UpdateDiamonds()
    {
        if (Time.timeSinceLevelLoad > 1 && !GameUI.instance.diamondsPopUp.isPlaying)
            GameUI.instance.diamondsPopUp.Play();
        GameUI.instance.SetDiamonds(diamonds);
    }
#endregion

#region Prestige
    public bool CanGetPrestige()
    {
        return hero.GetLevel() >= minPrestigeHeroLevel;
    }

    int GetMinPerkStage()
    {
        return 49 + SaveLoad.prestiges * 50;
    }

    int GetPerkPoints()
    {
        int perkPoints = 0;
        int minLevel = GetMinPerkStage();
        if (levelIndex >= minLevel)
        {
            perkPoints++;
            perkPoints += (levelIndex - minLevel) / 50;
        }
        return perkPoints;
    }

    bigNumber GetArtefactValue()
    {
        return new bigNumber(((float)levelIndex / 299f) * levelIndex + (Hero.instance.GetLevel() + HelpersController.instance.GetLevels()) / 100);
    }

    public void UpdatePrestigeBonus()
    {
        string text = LanguageManager.ReturnString("PRESTIGE_BONUS");
        text = text.Replace("%P%", GetPerkPoints().ToString());
        text = text.Replace("%S%", GetMinPerkStage().ToString());
        text = text.Replace("%N%", GetArtefactValue().ToString());
        GameUI.instance.prestigeBonus.text = text;
    }

    public void AddPrestige()
    {
        if (!CanGetPrestige())
            return;

        GameUI.instance.prestigeWindow.SetActive(false);
        GameUI.instance.CloseAll();

        StartCoroutine(ShowPrestigeEffect());
    }

    IEnumerator ShowPrestigeEffect()
    {
        Time.timeScale = 0;
        Transform clone = Instantiate(prestigeEffect, Vector3.zero, Quaternion.identity);
        yield return new WaitForSecondsRealtime(5.3f);

        SkillTree.instance.AddPerkPoints(GetPerkPoints());
        SaveLoad.prestiges = Mathf.CeilToInt((float)levelIndex / 50f);

        Artefacts.instance.AddArtefactValue(GetArtefactValue());
        AchievementManager.instance.Add("ACHIEVEMENT_PRESTIGES", new bigNumber(1));

        hero.SetLevel(1);
        SaveLoad.playerLevel = 1;

        hero.ResetSkills();

        hero.SetMana(0);
        SaveLoad.mana = 0;

        HelpersController.instance.ResetHelpers();

        SetCoins(new bigNumber(0));

        levelIndex = 0;
        blockIndex = 0;
        ReloadLevel();
        if (boss)
        {
            lost = false;
            fightingBoss = false;
            GameUI.instance.bossButton.SetActive(false);
            GameUI.instance.leaveButton.SetActive(false);
            Destroy(boss.gameObject);
            GameUI.instance.bossSkillHolder.SetActive(false);
        }
        SaveLoad.SaveProgress();

        GameUI.instance.UpdateCharacterTab();
        yield return new WaitForSecondsRealtime(10.7f);
        Destroy(clone.gameObject, 9);
        Time.timeScale = 1;
    }
#endregion

    public void DeleteSaveFiles()
    {
        SaveLoad.DeleteSaveFiles();
        Application.Quit();
    }
}