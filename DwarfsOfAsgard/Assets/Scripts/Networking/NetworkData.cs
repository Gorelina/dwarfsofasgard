﻿using UnityEngine.Networking;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

class AcceptAllCertificates : CertificateHandler
{

    protected override bool ValidateCertificate(byte[] certificateData)
    {
        return true;
    }
}

[System.Serializable]
public struct User
{
    public string nickname;
    public string gold;
    public string levelsPassed;
    public string totalLevel;

    public User(string nickname, string gold, string levelsPassed, string totalLevel)
    {
        this.nickname = nickname;
        this.gold = gold;
        this.levelsPassed = levelsPassed;
        this.totalLevel = totalLevel;
    }
}

[System.Serializable]
public struct UserBody
{
    public string gold;

    public UserBody(string _gold)
    {
        gold = _gold;
    }
}

[System.Serializable]
public struct UserOutput
{
    public string resultCode;
    public string resultDescription;

    public UserOutput(string _resultCode, string _resultDescription)
    {
        resultCode = _resultCode;
        resultDescription = _resultDescription;
    }
}

[System.Serializable]
public struct UserDataOutput
{

    public string resultCode;
    public string resultDescription;
    public User user;

    public UserDataOutput(string resultCode, string resultDescription, User user)
    {
        this.resultCode = resultCode;
        this.resultDescription = resultDescription;
        this.user = user;
    }
}

[System.Serializable]
public struct LeaderboardOutput
{
    [System.Serializable]
    public struct Leaderboard
    {
        public string nickname;
        public string gold;
        public string levelsPassed;
        public string totalLevel;

        public Leaderboard(string nickname, string gold, string levelsPassed, string totalLevel)
        {
            this.nickname = nickname;
            this.gold = gold;
            this.levelsPassed = levelsPassed;
            this.totalLevel = totalLevel;
        }
    }

    public string resultCode;
    public string resultDescription;
    public string sortedBy;
    public string userPosition;
    public Leaderboard[] leaderboard;

    public LeaderboardOutput(string resultCode, string resultDescription, string sortedBy, string userPosition, Leaderboard[] leaderboard)
    {
        this.resultCode = resultCode;
        this.resultDescription = resultDescription;
        this.sortedBy = sortedBy;
        this.userPosition = userPosition;
        this.leaderboard = leaderboard;
    }
}

[System.Serializable]
public struct RegistrationBody
{
    public string deviceOS;
    public string nickname;
    public string password;

    public RegistrationBody(string _deviceOS, string _nickname, string _password)
    {
        deviceOS = _deviceOS;
        nickname = _nickname;
        password = _password;
    }
}

[System.Serializable]
public struct RegistrationOutput
{
    public string resultCode;
    public string resultDescription;
    public string userUUID;

    public RegistrationOutput(string _resultCode, string _resultDescription, string _userUUID)
    {
        resultCode = _resultCode;
        resultDescription = _resultDescription;
        userUUID = _userUUID;
    }
}

[System.Serializable]
public struct TimeData
{
    public string resultCode;
    public string resultDescription;
    public string currentTime;

    public TimeData(string _resultCode, string _resultDescription, string _currentTime)
    {
        resultCode = _resultCode;
        resultDescription = _resultDescription;
        currentTime = _currentTime;
    }
}

[System.Serializable]
public struct ChangeBody
{
    public string currentPassword;
    public string newPassword;

    public ChangeBody(string currentPassword, string newPassword)
    {
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
    }
}

[System.Serializable]
public struct ChangeOutput
{
    public string resultCode;
    public string resultDescription;
    public string userUUID;

    public ChangeOutput(string resultCode, string resultDescription, string userUUID)
    {
        this.resultCode = resultCode;
        this.resultDescription = resultDescription;
        this.userUUID = userUUID;
    }
}