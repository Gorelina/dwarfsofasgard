﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviour
{

    [Header("UI")]
    public GameObject bg;
    public GameObject launchWindow;
    public InputField nicknameInput;
    public StringRead errorMessage;
    [Header("Message")]
    public GameObject message;
    public Text messageText;
    public Button messageButton;
    [Header("LoadScreen")]
    public GameObject loading;
    public Image loadingBar;
    private bool first;

    private static Regex sUserNameAllowedRegEx = new Regex(@"^[A-Za-z][A-Za-z0-9._]{4,14}$", RegexOptions.Compiled);

    public static NetworkManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    void Start()
    {
        NetworkRequests.CheckConnection(res =>
        {
            if (res)
            {
                first = SaveLoad.first;
                if (SaveLoad.first)
                {
                    GuestRegister();
                    OpenLaunchWindow();
                }
                else
                {
                    Init();
                }
            }
            else
            {
                ShowMessage("Cannot connect to the server!", delegate { Application.Quit(); });
            }
        });
    }

    System.Collections.IEnumerator Loading(string scene)
    {
        loading.SetActive(true);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);
        asyncLoad.allowSceneActivation = false;

        while(!asyncLoad.isDone)
        {
            loadingBar.fillAmount = asyncLoad.progress;

            if (asyncLoad.progress >= 0.9f)
            {
                asyncLoad.allowSceneActivation = true;
                bg.SetActive(false);
                loading.SetActive(false);
            }

            yield return null;
        }
    }

    void OpenLaunchWindow()
    {
        launchWindow.SetActive(true);
    }

    public void SubmitNickname()
    {
        if (!ValidateNickname(nicknameInput.text))
        {
            errorMessage.Change("REGISTRATION_4");
            errorMessage.GetComponent<Tween>().Play();
            return;
        }

        ChangeNicknamePassword(nicknameInput.text, SaveLoad.nickname);
    }

    public void Skip()
    {
        launchWindow.SetActive(false);
        Init();
    }

    void Init()
    {
        NetworkRequests.GetTime(res =>
        {
            if (res.resultCode == "0")
            {
                if (SaveLoad.wauTime == "null")
                    SaveLoad.wauTime = res.currentTime;
                SaveLoad.entryTime = res.currentTime;
                DateTime enTime, exTime;
                DateTime.TryParseExact(SaveLoad.entryTime, SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out enTime);
                DateTime.TryParseExact(SaveLoad.exitTime, SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out exTime);
                SaveLoad.ts = enTime - exTime;
                if (SaveLoad.ts.Days >= 90)
                    SaveLoad.ts = new TimeSpan();
                SaveLoad.timeSpan = SaveLoad.ts.ToString(@"d\-hh\:mm");
                Continue();
            }
            else
            {
                ShowMessage(res.resultDescription, delegate { Quit(); });
            }
        });
    }

    void Continue()
    {
        SaveLoad.first = false;
        SaveLoad.SaveProfile();

        launchWindow.SetActive(false);

        StartCoroutine(Loading("Main"));
    }

    public void UpdateUser()
    {
        string currentGold = Mining.instance.GetCoins().GetString();
        string wholeGold = Mining.instance.GetWholeGold().GetString();
        string levelsPassed = (SaveLoad.levelIndex + 1).ToString();
        string totalLevel = (SaveLoad.playerLevel + 1).ToString();
        SaveLoad.gold = currentGold;
        SaveLoad.wholeGold = wholeGold;
        SaveLoad.SaveProgress();

        NetworkRequests.GetTime(res =>
        {
            if (res.resultCode == "0")
            {
                SaveLoad.exitTime = res.currentTime;
                SaveLoad.SaveProfile();
            }
        });

        SaveUser(wholeGold, levelsPassed, totalLevel);
    }

    void SaveUser(string gold = "0E+0", string levelsPassed = "0", string totalLevel = "0")
    {
        NetworkRequests.PutUserData(SaveLoad.uuid, SaveLoad.nickname, gold, levelsPassed, totalLevel);
    }

    public void GetLeaderboardData(string sortBy, Action<LeaderboardOutput> callback)
    {
        NetworkRequests.GetLeaderboard(SaveLoad.uuid, sortBy, callback);
    }

    public void GuestRegister()
    {
        NetworkRequests.Register("", "", res =>
        {
            if (res.resultCode == "0")
            {
                SaveLoad.first = false;
                SaveLoad.uuid = res.userUUID;
                SaveLoad.SaveProfile();

                NetworkRequests.GetUserData(SaveLoad.uuid, data =>
                {
                    if (data.resultCode == "0")
                    {
                        SaveLoad.nickname = data.user.nickname;
                        SaveLoad.SaveProfile();
                        SaveUser();
                    }
                    else
                    {
                        ShowMessage(data.resultDescription, delegate { CloseMessage(); });
                        return;
                    }
                });
            }
            else
            {
                ShowMessage(res.resultDescription, delegate { CloseMessage(); });
                return;
            }
        });
    }

    public void Register(string login, string password)
    {
        if (!ValidateNickname(login))
        {
            ShowMessage(LanguageManager.ReturnString("REGISTRATION_INVALID_LOGIN"), delegate { CloseMessage(); });
            return;
        }

        if (!ValidateNickname(password))
        {
            ShowMessage(LanguageManager.ReturnString("REGISTRATION_INVALID_PASSWORD"), delegate { CloseMessage(); });
            return;
        }

        if (SaveLoad.nickname != login)
        {
            NetworkRequests.ChangeNickname(SaveLoad.uuid, login, success =>
                {
                    if (success)
                    {
                        NetworkRequests.ChangePassword(SaveLoad.uuid, "", password, succ =>
                        {
                            if (succ)
                            {
                                SaveLoad.nickname = login;
                                SaveLoad.registered = true;
                                SaveLoad.SaveProfile();
                                Mining.instance.AddDiamonds(100);
                            }
                            else
                            {
                                ShowMessage(LanguageManager.ReturnString("REGISTRATION_3"), delegate { CloseMessage(); });
                            }
                        });
                    }
                    else
                    {
                        ShowMessage(LanguageManager.ReturnString("REGISTRATION_3"), delegate { CloseMessage(); });
                    }
                });
        }
        else
        {
            NetworkRequests.ChangePassword(SaveLoad.uuid, "", password, success =>
            {
                if (success)
                {
                    SaveLoad.registered = true;
                    SaveLoad.SaveProfile();
                    Mining.instance.AddDiamonds(100);
                }
                else
                {
                    ShowMessage(LanguageManager.ReturnString("REGISTRATION_3"), delegate { CloseMessage(); });
                }
            });
        }
    }

    public void Login(string login, string password)
    {
        NetworkRequests.GetUUID(login, password, res =>
        {
            if (res.resultCode == "0")
            {
                SaveLoad.nickname = login;
                SaveLoad.uuid = res.userUUID;
                SaveLoad.registered = true;
                SaveLoad.SaveProfile();
                LoadUser();

                Time.timeScale = 0;
                ShowMessage(LanguageManager.ReturnString("RESTART_MESSAGE"), delegate { Application.Quit(); });
            }
            else
            {
                ShowMessage(res.resultDescription, delegate { CloseMessage(); });
            }
        });
    }

    public void LoadUser()
    {
        NetworkRequests.GetUserData(SaveLoad.uuid, res =>
        {
            if (res.resultCode == "0")
            {
                SaveLoad.wholeGold = res.user.gold;
                SaveLoad.gold = res.user.gold;
                SaveLoad.levelIndex = int.Parse(res.user.levelsPassed);
                SaveLoad.playerLevel = int.Parse(res.user.totalLevel);
                SaveLoad.SaveProgress();
            }
            else
            {
                ShowMessage(res.resultDescription, delegate { CloseMessage(); });
            }
        });
    }

    public void ChangeNicknamePassword(string nickname, string password)
    {
        NetworkRequests.ChangeNickname(SaveLoad.uuid, nickname, success =>
        {
            if (success)
            {
                launchWindow.SetActive(false);
                SaveLoad.nickname = nicknameInput.text;
                Init();
            }
            else
            {
                errorMessage.Change("REGISTRATION_3");
                errorMessage.GetComponent<Tween>().Play();
            }
        });
    }

    public void ShowMessage(string _message, UnityEngine.Events.UnityAction _action)
    {
        message.SetActive(true);
        messageText.text = _message;
        messageButton.onClick.RemoveAllListeners();
        messageButton.onClick.AddListener(_action);
        messageButton.onClick.AddListener(delegate { CloseMessage(); });
    }

    public void CloseMessage()
    {
        message.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public static bool ValidateNickname(string nickname)
    {
        if (string.IsNullOrEmpty(nickname)|| !sUserNameAllowedRegEx.IsMatch(nickname))
        {
            return false;
        }
        return true;
    }
}