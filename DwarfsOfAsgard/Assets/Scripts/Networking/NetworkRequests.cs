﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;

public class NetworkRequests
{

    private static readonly string basePath = "https://212.224.118.234:8282";
    private static RequestHelper currentRequest;

    public static void GetTime(Action<TimeData> callback)
    {
        currentRequest = new RequestHelper
        {
            Uri = basePath + "/time",
            CertificateHandler = new AcceptAllCertificates()
        };

        RestClient.Get<TimeData>(currentRequest)
            .Then(res =>
            {
                callback.Invoke(res);
            }).Catch(err =>
            {
                Debug.LogError(err.Message);
            });
    }

    public static void CheckConnection(Action<bool> callback)
    {
        currentRequest = new RequestHelper
        {
            Uri = basePath + "/time",
            CertificateHandler = new AcceptAllCertificates()
        };

        RestClient.Get<TimeData>(currentRequest)
            .Then(res =>
            {
                callback.Invoke(res.resultCode == "0");
            }).Catch(err =>
            {
                callback.Invoke(false);
            });
    }

    public static void GetUserData(string uuid, Action<UserDataOutput> callback)
    {
        currentRequest = new RequestHelper
        {
            Uri = basePath + "/user?userUUID=" + uuid,
            CertificateHandler = new AcceptAllCertificates()
        };

        RestClient.Get<UserDataOutput>(currentRequest)
            .Then(res =>
            {
                callback.Invoke(res);
            }).Catch(err =>
            {
                Debug.LogError(err.Message);
            });
    }

    public static void Register(string login, string password, Action<RegistrationOutput> callback)
    {
        currentRequest = new RequestHelper
        {
            Uri = basePath + "/register",
            Body = new RegistrationBody(Application.platform == RuntimePlatform.Android ? "Android" : "iOS", login, password),
            CertificateHandler = new AcceptAllCertificates()
        };

        RestClient.Post<RegistrationOutput>(currentRequest)
            .Then(res =>
            {
                callback.Invoke(res);
            })
            .Catch(err =>
            {
                Debug.LogError(err.Message);
            });
    }

    public static void GetLeaderboard(string uuid, string sortBy, Action<LeaderboardOutput> callback)
    {
        currentRequest = new RequestHelper
        {
            Uri = basePath + "/leaderboard?userUUID=" + uuid + "&sortBy=" + sortBy,
            CertificateHandler = new AcceptAllCertificates()
        };

        RestClient.Get<LeaderboardOutput>(currentRequest)
            .Then(res =>
            {
                callback.Invoke(res);
            }).Catch(err =>
            {
                Debug.LogError(err.Message);
            });
    }

    public static void PutUserData(string uuid, string nickname, string gold, string levelsPassed, string totalLevel)
    {
        currentRequest = new RequestHelper
        {
            Uri = basePath + "/user/" + uuid,
            Body = new User(nickname, gold, levelsPassed, totalLevel),
            CertificateHandler = new AcceptAllCertificates()
        };

        RestClient.Put<UserOutput>(currentRequest, (err, res, body) =>
        {
            if (err != null)
            {
                Debug.LogError(err.Message);
            }
        });
    }

    public static void GetUUID(string login, string password, Action<RegistrationOutput> callback)
    {
        byte[] loginData = System.Text.Encoding.ASCII.GetBytes(login + ":" + password);
        Dictionary<string, string> loginHeaders = new Dictionary<string, string>(){
            { "Authorization", "Basic " + System.Convert.ToBase64String(loginData) }
        };

        currentRequest = new RequestHelper
        {
            Uri = basePath + "/get_userUUID",
            CertificateHandler = new AcceptAllCertificates(),
            Headers = loginHeaders
        };

        RestClient.Get<RegistrationOutput>(currentRequest)
            .Then(res =>
            {
                callback.Invoke(res);
            }).Catch(err =>
            {
                Debug.LogError(err.Message);
            });
    }

    public static void ChangeNickname(string uuid, string nickname, Action<bool> callback)
    {
        currentRequest = new RequestHelper
        {
            Uri = basePath + "/change_nickname/" + uuid + "?nickname=" + nickname,
            CertificateHandler = new AcceptAllCertificates()
        };

        RestClient.Put<ChangeOutput>(currentRequest, (err, res, body) =>
        {
            if (err == null)
            {
                callback.Invoke(true);
            }
            else
            {
                callback.Invoke(false);
            }
        });
    }

    public static void ChangePassword(string uuid, string currentPassword, string newPassword, Action<bool> callback)
    {
        currentRequest = new RequestHelper
        {
            Uri = basePath + "/change_password/" + uuid,
            Body = new ChangeBody(currentPassword, newPassword),
            CertificateHandler = new AcceptAllCertificates()
        };

        RestClient.Put<ChangeOutput>(currentRequest, (err, res, body) =>
        {
            if (err == null)
            {
                callback.Invoke(true);
            }
            else
            {
                callback.Invoke(false);
            }
        });
    }
}