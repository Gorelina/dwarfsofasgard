﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_IOS
using Unity.Notifications.iOS;
#elif UNITY_ANDROID
using Unity.Notifications.Android;
#endif
using System;

public class NotificationManager : MonoBehaviour
{

    public struct Notification
    {
        public TimeSpan timeSpan;
        public string id;
        public int number;
        public string title;
        public string body;

        public Notification(TimeSpan timeSpan, string id, int number, string title, string body)
        {
            this.timeSpan = timeSpan;
            this.id = id;
            this.number = number;
            this.title = title;
            this.body = body;
        }
    }

    private bool cancelled;
    private bool initialized;
#if UNITY_ANDROID
    private AndroidNotificationChannel androidChannel;
#endif
    private static NotificationManager instance;

    public static NotificationManager Instance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            Init();
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    void Init()
    {
#if UNITY_ANDROID
        try
        {
            androidChannel = new AndroidNotificationChannel()
            {
                Id = "miners",
                Name = "miners",
                Importance = Importance.High,
                Description = "Miners of Asgard channel",
            };
            AndroidNotificationCenter.RegisterNotificationChannel(androidChannel);

            initialized = true;
            AndroidNotificationCenter.CancelAllNotifications();
        }
        catch
        {
            initialized = false;
        }
#elif UNITY_IOS
        if (iOSNotificationCenter.GetNotificationSettings().AuthorizationStatus != AuthorizationStatus.Authorized)
        {
            initialized = false;
        }
        else
        {
            initialized = true;
            iOSNotificationCenter.RemoveAllDeliveredNotifications();
            iOSNotificationCenter.RemoveAllScheduledNotifications();
        }
#endif
    }

    public static void ScheduleNotification(Notification data)
    {
        if (!SaveLoad.notifications)
            return;

        if (!instance.initialized)
            return;

        RemoveNotification(data.id, data.number);

#if UNITY_ANDROID
        DateTime fireTime = DateTime.Now;
        fireTime = fireTime.AddHours(data.timeSpan.Hours);
        fireTime = fireTime.AddMinutes(data.timeSpan.Minutes);
        fireTime = fireTime.AddSeconds(data.timeSpan.Seconds);

        AndroidNotification notification = new AndroidNotification()
        {
            Title = data.title,
            Text = data.body,
            FireTime = fireTime,
            LargeIcon = "large",
            SmallIcon = "small",
            Number = data.number,
        };

        AndroidNotificationCenter.SendNotification(notification, instance.androidChannel.Id);
#elif UNITY_IOS
        iOSNotificationTimeIntervalTrigger timeTrigger = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = data.timeSpan,
            Repeats = false,
        };

        iOSNotification notification = new iOSNotification()
        {
            Identifier = data.id,
            Title = Application.productName,
            Subtitle = data.title,
            Body = data.body,
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier = "miners_category",
            ThreadIdentifier = "miners_thread",
            Trigger = timeTrigger,
        };

        iOSNotificationCenter.ScheduleNotification(notification);
#endif
    }

    public static void RemoveNotification(string id, int number)
    {
#if UNITY_ANDROID
        AndroidNotificationCenter.CancelNotification(number);
#elif UNITY_IOS
        iOSNotificationCenter.RemoveScheduledNotification(id);
#endif
    }

    public static void CancelFurtherNotifications()
    {
        instance.cancelled = true;
    }

    public static void AllowFurtherNotification()
    {
        instance.cancelled = false;
    }

    void OnApplicationPause(bool pause)
    {
        if (!cancelled)
            return;

#if UNITY_ANDROID
        AndroidNotificationCenter.CancelAllNotifications();
#elif UNITY_IOS
        iOSNotificationCenter.RemoveAllScheduledNotifications();
        iOSNotificationCenter.RemoveAllDeliveredNotifications();
#endif
    }
}