﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PassiveGold : MonoBehaviour
{

    public GameObject window;
    public Text welcoming;
    public Text earnedGold;
    private bigNumber gold;
    private bool reward;

    void Start()
    {
        reward = SaveLoad.ts.TotalMinutes > 30;

        if (reward)
        {
            float mult = (float)SaveLoad.ts.TotalMinutes / 180f;
            if (mult > 3)
                mult = 3;

            Formules.GetLevelNumbers(SaveLoad.levelIndex + 3, 8, out bigNumber hp, out bigNumber rsc);
            gold = new bigNumber(rsc * mult) * PerksManager.instance.Get("PassiveGold") * HelpersController.instance.ActiveHelpersCount();

            if (gold.number > 0)
                OpenWindow();
            else WAU.instance.TryOpenWindow();
        }
        else
        {
            WAU.instance.TryOpenWindow();
        }
    }

    void OpenWindow()
    {
        window.SetActive(true);
        InitWindow();
    }

    void CloseWindow()
    {
        window.SetActive(false);
        WAU.instance.TryOpenWindow();
    }

    void InitWindow()
    {
        welcoming.text = LanguageManager.ReturnString("PASSIVE_GOLD_WELCOMING_" + Random.Range(1, 8));
        earnedGold.text = gold.ToString();
    }

    public void Collect()
    {
        GameUI.instance.soundContainer.PlaySound("click");

        Mining.instance.AddCoins(gold);

        CloseWindow();
    }

    public void WatchAd()
    {
        GameUI.instance.soundContainer.PlaySound("click");

        AdManager.WatchRewarded(Reward);
    }

    public void Reward()
    {
        Mining.instance.AddCoins(gold * 2);

        CloseWindow();
    }

    void OnApplicationPause(bool pause)
    {
        if (pause && HelpersController.instance.ActiveHelpersCount() > 0)
        {
            NotificationManager.ScheduleNotification(new NotificationManager.Notification
            {
                id = "gold",
                number = 1,
                timeSpan = new System.TimeSpan(3, 0, 0),
                title = LanguageManager.ReturnString("PASSIVE_GOLD_NT_TITLE"),
                body = LanguageManager.ReturnString("PASSIVE_GOLD_NT_MESSAGE"),
            });
        }
    }
}