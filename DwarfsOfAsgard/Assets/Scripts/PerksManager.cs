﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ToDo
/*
    m_artefactsDamage
*/
//--//

[System.Serializable]
public class PerkEffect
{
    public string effect;
    public float multiplier;
}

public class Perk
{
    public string name;
    public enum Types {Add, Multiply };
    public Types type;
    public float baseValue;
    private float multiplier;
    private float value;

    public Perk(string name, Types type, float baseValue)
    {
        this.name = name;
        this.type = type;
        this.baseValue = baseValue;
        multiplier = 1;
        value = baseValue;
    }

    public void Set(float effect)
    {
        if (type == Types.Multiply)
        {
            multiplier *= effect;
        }
        else
        {
            value += effect;
        }
    }

    public float Get()
    {
        return value * multiplier;
    }

    public void Reset()
    {
        value = baseValue;
        multiplier = 1;
    }

    public string Description()
    {
        return (type == Types.Add ? "+{0} " : "x{0} ") + LanguageManager.ReturnString(name.ToUpper());
    }
}

public class PerksManager : MonoBehaviour
{

    private List<Perk> perks;
    private string debugString;
    public static PerksManager instance;

    void Awake()
    {
        instance = this;

        perks = new List<Perk>();
        perks.Add(new Perk("CritDamage", Perk.Types.Multiply, 5));
        perks.Add(new Perk("CritChance", Perk.Types.Add, 0.3f));
        perks.Add(new Perk("ChestAmount", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ChestChance", Perk.Types.Add, 1));
        perks.Add(new Perk("ManaPoolCap", Perk.Types.Add, 0));
        perks.Add(new Perk("TapDamageFromHelpers", Perk.Types.Add, 1));
        perks.Add(new Perk("AllDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("GoldBoss", Perk.Types.Multiply, 1));
        perks.Add(new Perk("AllHelperDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("RangedHelperDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("GoldAll", Perk.Types.Multiply, 1));
        perks.Add(new Perk("FairyGold", Perk.Types.Multiply, 1));
        perks.Add(new Perk("TapDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("MeleeHelperDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ManaRegen", Perk.Types.Add, 0));
        perks.Add(new Perk("SpellHelperDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("SkillsGold", Perk.Types.Add, 0));
        perks.Add(new Perk("PassiveGold", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ArtefactsDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("GroundDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("AirDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("OreDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("BossDamage", Perk.Types.Multiply, 1));
        perks.Add(new Perk("EngineerSkills", Perk.Types.Multiply, 1));
        perks.Add(new Perk("GunnersSkills", Perk.Types.Multiply, 1));
        perks.Add(new Perk("StonersSkills", Perk.Types.Multiply, 1));
        perks.Add(new Perk("SupportersSkills", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ArtefactsBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("SwordBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("HelmetBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ArmorBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("AuraBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ActiveSkillsBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("HeavenStrikeBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("DeadlyStrikeBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("MidasBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("FireSwordBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("WarcryBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ShadowCloneBuff", Perk.Types.Multiply, 1));
        perks.Add(new Perk("SkillsCooldown", Perk.Types.Multiply, 1));
        perks.Add(new Perk("DamageWhileMidas", Perk.Types.Multiply, 1));
        perks.Add(new Perk("HelpersDamageWhileWarCry", Perk.Types.Multiply, 1));
        perks.Add(new Perk("DeadlyStrikeTime", Perk.Types.Multiply, 1));
        perks.Add(new Perk("MidasTime", Perk.Types.Multiply, 1));
        perks.Add(new Perk("FireSwordTime", Perk.Types.Multiply, 1));
        perks.Add(new Perk("WarcryTime", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ShadowCloneTime", Perk.Types.Multiply, 1));
        perks.Add(new Perk("HeavenStrikeMana", Perk.Types.Multiply, 1));
        perks.Add(new Perk("DeadlyStrikeMana", Perk.Types.Multiply, 1));
        perks.Add(new Perk("MidasMana", Perk.Types.Multiply, 1));
        perks.Add(new Perk("FireSwordMana", Perk.Types.Multiply, 1));
        perks.Add(new Perk("WarcryMana", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ShadowCloneMana", Perk.Types.Multiply, 1));
        perks.Add(new Perk("ManaBack", Perk.Types.Multiply, 0));
        perks.Add(new Perk("ManaTap", Perk.Types.Add, 0));
        perks.Add(new Perk("MoneyBonusChance", Perk.Types.Add, 3));
        perks.Add(new Perk("FewDroppersChance", Perk.Types.Add, 0));
        perks.Add(new Perk("AllChances", Perk.Types.Multiply, 1));
        perks.Add(new Perk("HeroCost", Perk.Types.Multiply, 1));
        perks.Add(new Perk("BossHP", Perk.Types.Multiply, 1));
        perks.Add(new Perk("BossTimer", Perk.Types.Multiply, 1));
        perks.Add(new Perk("UpgradeCost", Perk.Types.Multiply, 1));
    }

    int GetPerk(string name)
    {
        for (int i = 0; i < perks.Count; i++)
        {
            if (perks[i].name == name)
                return i;
        }

        Debug.LogError("Perks with name " + name + " not found");
        return -1;
    }

    public void ResetMultipliers()
    {
        foreach (Perk perk in perks)
            perk.Reset();

        ProcessArtefacts();
        ProcessSkillTree();
        ProcessInventory();
        ProcessWarehouse();
    }

    public void Set(string name, float multiplier)
    {
        int index = GetPerk(name);

        if (index >= 0)
            perks[index].Set(multiplier);
    }

    public float Get(string name)
    {
        int index = GetPerk(name);

        if (index >= 0)
            return perks[index].Get();
        else return 0;
    }

    public string Description(string name)
    {
        int index = GetPerk(name);

        if (index >= 0)
            return perks[index].Description();
        else return "";
    }

    public void ProcessArtefacts()
    {
        List<ArtefactInstance> artefacts = Artefacts.instance.GetArtefacts();
        for (int i = 0; i < artefacts.Count; i++)
        {
            PerkEffect effect = artefacts[i].GetEffect();
            Set(effect.effect, effect.multiplier * Get("ArtefactsBuff"));
            Set("TapDamage", artefacts[i].GetData().tapDamage);
        }
    }

    public void ProcessSkillTree()
    {
        List<PerkEffect> effects = SkillTree.instance.GetEffects();
        for (int i = 0; i < effects.Count; i++)
        {
            Set(effects[i].effect, effects[i].multiplier);
        }
    }

    public void ProcessInventory()
    {
        List<PerkEffect> effects = Inventory.instance.GetPerks();
        for (int i = 0; i < effects.Count; i++)
        {
            Set(effects[i].effect, effects[i].multiplier);
        }
    }

    public void ProcessWarehouse()
    {
        List<WarehouseBuff> effects = Warehouse.instance.GetBuffs();
        for (int i = 0; i < effects.Count; i++)
        {
            Set(effects[i].effect.effect, effects[i].effect.multiplier);
        }
    }

    public string GetInfo()
    {
        debugString = "<b>CHANCES</b>\n";
        debugString += "allChances: " + Get("AllChances") + "\n";
        debugString += "critChance: " + Get("CritChance") + "\n";
        debugString += "chestChance: " + Get("ChestChance") + "\n";
        debugString += "moneyBonusChances: " + Get("MoneyBonusChance") + "\n";
        debugString += "fewDroppersChances: " + Get("FewDroppersChance") + "\n";
        debugString += "<b>GOLD</b>\n";
        debugString += "heroCost: " + Get("HeroCost") + "\n";
        debugString += "upgradeCost: " + Get("UpgradeCost") + "\n";
        debugString += "passiveGold: " + Get("PassiveGold") + "\n";
        debugString += "skillsGold: " + Get("SkillsGold") + "\n";
        debugString += "goldBoss: " + Get("GoldBoss") + "\n";
        debugString += "chestAmount: " + Get("ChestAmount") + "\n";
        debugString += "allGold: " + Get("GoldAll") + "\n";
        debugString += "fairyGold: " + Get("FairyGold") + "\n";
        debugString += "<b>HERO</b>\n";
        debugString += "critDamage: " + Get("CritDamage") + "\n";
        debugString += "manaPoolCap: " + Get("ManaPoolCap") + "\n";
        debugString += "manaRegen: " + Get("ManaRegen") + "\n";
        debugString += "tapDamage: " + Get("TapDamage") + "\n";
        debugString += "artefactsDamage: " + Get("ArtefactsDamage") + "\n";
        debugString += "artefactsBuff: " + Get("ArtefactsBuff") + "\n";
        debugString += "swordBuff: " + Get("SwordBuff") + "\n";
        debugString += "armorBuff: " + Get("ArmorBuff") + "\n";
        debugString += "helmetBuff: " + Get("HelmetBuff") + "\n";
        debugString += "auraBuff: " + Get("AuraBuff") + "\n";
        debugString += "activeSkillsBuff: " + Get("ActiveSkillsBuff") + "\n";
        debugString += "heavenStrikeBuff: " + Get("HeavenStrikeBuff") + "\n";
        debugString += "deadlyStrikeBuff: " + Get("DeadlyStrikeBuff") + "\n";
        debugString += "midasBuff: " + Get("MidasBuff") + "\n";
        debugString += "fireSwordBuff: " + Get("FireSwordBuff") + "\n";
        debugString += "warcryBuff: " + Get("WarcryBuff") + "\n";
        debugString += "shadowCloneBuff: " + Get("ShadowCloneBuff") + "\n";
        debugString += "skillsCooldown: " + Get("SkillsCooldown") + "\n";
        debugString += "deadlyStrikeTime: " + Get("DeadlyStrikeTime") + "\n";
        debugString += "midasTime: " + Get("MidasTime") + "\n";
        debugString += "fireSwordTime: " + Get("FireSwordTime") + "\n";
        debugString += "warcryTime: " + Get("WarcryTime") + "\n";
        debugString += "shadowCloneTime: " + Get("ShadowCloneTime") + "\n";
        debugString += "heavenStrikeMana: " + Get("HeavenStrikeMana") + "\n";
        debugString += "deadlyStrikeMana: " + Get("DeadlyStrikeMana") + "\n";
        debugString += "midasMana: " + Get("MidasMana") + "\n";
        debugString += "fireSwordMana: " + Get("FireSwordMana") + "\n";
        debugString += "warcryMana: " + Get("WarcryMana") + "\n";
        debugString += "shadowCloneMana: " + Get("ShadowCloneMana") + "\n";
        debugString += "manaBack: " + Get("ManaBack") + "\n";
        debugString += "<b>HELPERS</b>\n";
        debugString += "tapDamageFromHelpers: " + Get("TapDamageFromHelpers") + "\n";
        debugString += "allHelperDamage: " + Get("AllHelperDamage") + "\n";
        debugString += "rangedHelperDamage: " + Get("RangedHelperDamage") + "\n";
        debugString += "meleeHelperDamage: " + Get("MeleeHelperDamage") + "\n";
        debugString += "spellHelperDamage: " + Get("SpellHelperDamage") + "\n";
        debugString += "groundDamage: " + Get("GroundDamage") + "\n";
        debugString += "airDamage: " + Get("AirDamage") + "\n";
        debugString += "engineerSkills: " + Get("EngineerSkills") + "\n";
        debugString += "gunnersSkills: " + Get("GunnersSkills") + "\n";
        debugString += "supportersSkills: " + Get("SupportersSkills") + "\n";
        debugString += "stonersSkills: " + Get("StonersSkills") + "\n";
        debugString += "<b>OTHER</b>\n";
        debugString += "allDamage: " + Get("AllDamage") + "\n";
        debugString += "oreDamage: " + Get("OreDamage") + "\n";
        debugString += "bossDamage: " + Get("BossDamage") + "\n";
        debugString += "bossHp: " + Get("BossHP") + "\n";
        debugString += "bossTimer: " + Get("BossTimer") + "\n";

        return debugString;
    }
}