﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Security.Cryptography;

public class SaveLoad : MonoBehaviour
{

    [System.Serializable]
    public class Progress
    {
        public int version;
        public int mana;
        public int diamonds;
        public int perkPoints;
        public string artefactValue;
        public string wholeGold;
        public string gold;
        public string stone;
        public string coal;
        public string copper;
        public string iron;
        public string silver;
        public string aurum;
        public string platinum;
        public int levelIndex;
        public int playerLevel;
        public int[] skillLevels;
        public string[] abilities;
        public bool[] skillTree;
        public int[] helpersLevels;
        public int[] helpersSkills;
        public Inventory.ItemSave[] artefacts;
        public Inventory.SaveData inventory;
        public float passiveFarm;
        public int passiveFarmStep;
        public int[] warehouseBuffs;
        public AchievementData[] achievementDatas;
        public int wauProgress;
        public int prestiges;
        public int tutorialStep;

        public string Show()
        {
            string result = "version: " + version;
            result += "\nmana: " + mana;
            result += "\ndiamonds: " + diamonds;
            result += "\nwholeGold: " + wholeGold;
            result += "\ngold: " + gold;
            result += "\nlevelIndex: " + levelIndex;
            result += "\nplayerLevel: " + playerLevel;
            result += "\nskillLevels:\n";
            for (int i = 0; i < skillLevels.Length; i++)
                result += skillLevels[i].ToString() + '\n';
            result += "\nabilities:\n";
            for (int i = 0; i < abilities.Length; i++)
                result += abilities[i] + '\n';
            result += "\nhelpersLevels:\n";
            for (int i = 0; i < helpersLevels.Length; i++)
            {
                result += helpersLevels[i].ToString() + '|' + helpersSkills[i] + '\n';
            }
            result += "\nartefacts:\n";
            for (int i = 0; i < artefacts.Length; i++)
            {
                result += artefacts[i].ToString() + '\n';
            }
            /*result += "\nitems:\n";
            result += inventory.head + " " + inventory.chest + " " + inventory.weapon + " " + inventory.cloak;
            result += "\nweapons: ";
            for (int i = 0; i < inventory.weapons.Length; i++)
                result += inventory.weapons[i] + " ";
            result += "\nheads: ";
            for (int i = 0; i < inventory.heads.Length; i++)
                result += inventory.heads[i] + " ";
            result += "\nchests: ";
            for (int i = 0; i < inventory.chests.Length; i++)
                result += inventory.chests[i] + " ";
            result += "\ncloaks: ";
            for (int i = 0; i < inventory.cloaks.Length; i++)
                result += inventory.cloaks[i] + " ";
            result += "\nwauProgress: " + wauProgress;*/
            return result;
        }
    }

    [System.Serializable]
    public class Profile
    {
        public int version;
        public string nickname;
        public bool registered;
        public string uuid;
        public bool first;
        public string entryTime;
        public string exitTime;
        public string wauTime;
        public string adBuffTime;
        public string adChestTime;
        public bool facebook;
        public bool twitter;
        public bool video;
        public bool vk;
        public int bundleIndex;
        public string language;
        public bool sounds;
        public bool music;
        public bool notifications;

        public string Show()
        {
            string result = "version: " + version;
            result += "\nnickname: " + nickname;
            result += "\nregistered: " + registered;
            result += "\nuuid: " + uuid;
            result += "\nfirst: " + first;
            result += "\nentryTime: " + entryTime;
            result += "\nexitTime: " + exitTime;
            result += "\nwauTime: " + wauTime;
            result += "\nandroid notifications:";
            return result;
        }
    }

    public static int diamonds;
    public static int mana;
    public static int perkPoints;
    public static string artefactValue;
    public static string wholeGold;
    public static string gold;
    public static string stone;
    public static string coal;
    public static string copper;
    public static string iron;
    public static string silver;
    public static string aurum;
    public static string platinum;
    public static int levelIndex;
    public static int playerLevel;
    public static int[] skillLevels;
    public static DateTime[] abilities;
    public static bool[] skillTree;
    public static int[] helpersLevels;
    public static int[] helpersSkills;
    public static List<Inventory.ItemSave> artefacts = new List<Inventory.ItemSave>();
    public static Inventory.SaveData inventory;
    public static float passiveFarm;
    public static int passiveFarmStep;
    public static int[] warehouseBuffs;
    public static AchievementData[] achievementDatas;
    public static int wauProgress;
    public static int prestiges;
    public static int tutorialStep;

    public static string nickname;
    public static bool registered;
    public static string uuid;
    public static bool first;
    public static string entryTime;
    public static string exitTime;
    public static string wauTime;
    public static string adBuffTime;
    public static string adChestTime;
    public static bool facebook;
    public static bool twitter;
    public static bool video;
    public static bool vk;
    public static int bundleIndex;
    public static string language;
    public static bool sounds;
    public static bool music;
    public static bool notifications;
    public static string timeSpan;
    public static TimeSpan ts;
    public const string timePattern = "yyyy-MM-ddTHH:mm:ss.FFF";
    private static SaveLoad instance;
    private static string profilePath;
    private static int profileVersion = 0;
    private static string progressPath;
    private static int progressVersion = 0;

    public static SaveLoad Instance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            Init();
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    void Init()
    {
        string dirPath;
        profilePath = Application.persistentDataPath + "/save/profile.dat";
        dirPath = Application.persistentDataPath + "/save";
        if (!Directory.Exists(dirPath))
            Directory.CreateDirectory(dirPath);

        if (!LoadProfile())
        {
            registered = false;
            entryTime = exitTime = adBuffTime = adChestTime = "2019-01-01T00:00:00.000";
            wauTime = "null";
            timeSpan = "0";
            first = true;
            facebook = false;
            twitter = false;
            video = false;
            vk = false;
            bundleIndex = -1;
            language = "en";
            sounds = true;
            music = true;
            notifications = true;
            SaveProfile();
        }

        progressPath = Application.persistentDataPath + "/save/progress.dat";

        if (!LoadProgress())
        {
            diamonds = 0;
            perkPoints = 0;
            artefactValue = "0E+0";
            mana = 0;
            wholeGold = "0E+0";
            gold = "0E+0";
            stone = "0E+0";
            coal = "0E+0";
            copper = "0E+0";
            iron = "0E+0";
            silver = "0E+0";
            aurum = "0E+0";
            platinum = "0E+0";
            levelIndex = 0;
            playerLevel = 0;
            skillLevels = new int[6];
            skillTree = new bool[DataBase.instance.treeSkills.Length];
            helpersLevels = new int[16];
            helpersSkills = new int[16];
            abilities = new DateTime[4];
            inventory = null;
            passiveFarm = 0;
            passiveFarmStep = 0;
            warehouseBuffs = new int[0];
            achievementDatas = new AchievementData[DataBase.instance.achievements.Length];
            for (int i = 0; i < achievementDatas.Length; i++)
                achievementDatas[i] = new AchievementData();
            wauProgress = 0;
            prestiges = 0;
            tutorialStep = 0;
            SaveProgress();
        }
    }

    /*public static string GetPassword()
    {
        return instance.Decrypt(password);
    }

    public static void SetPassword(string newPassword)
    {
        password = instance.Encrypt(newPassword);
        SaveProfile();
    }*/

    static bool LoadProfile()
    {
        if (!File.Exists(profilePath))
            return false;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(profilePath, FileMode.Open);

        Profile data = (Profile)bf.Deserialize(file);

        if (profileVersion == data.version)
        {
            registered = data.registered;
            exitTime = data.exitTime;
            wauTime = data.wauTime;
            first = data.first;
            nickname = data.nickname;
            uuid = data.uuid;
            adBuffTime = data.adBuffTime;
            adChestTime = data.adChestTime;
            facebook = data.facebook;
            twitter = data.twitter;
            video = data.video;
            vk = data.vk;
            bundleIndex = data.bundleIndex;
            language = data.language;
            sounds = data.sounds;
            music = data.music;
            notifications = data.notifications;
            file.Close();
        }
        else
        {
            file.Close();
            File.Delete(profilePath);

            return false;
        }

        return true;
    }

    public static void SaveProfile()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(profilePath);

        Profile data = new Profile();
        data.version = profileVersion;
        data.registered = registered;
        data.entryTime = entryTime;
        data.exitTime = exitTime;
        data.wauTime = wauTime;
        data.first = first;
        data.uuid = uuid;
        data.nickname = nickname;
        data.adBuffTime = adBuffTime;
        data.adChestTime = adChestTime;
        data.facebook = facebook;
        data.twitter = twitter;
        data.video = video;
        data.vk = vk;
        data.bundleIndex = bundleIndex;
        data.language = language;
        data.sounds = sounds;
        data.music = music;
        data.notifications = notifications;

        bf.Serialize(file, data);
        file.Close();
    }

    static bool LoadProgress()
    {
        if (!File.Exists(progressPath))
            return false;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(progressPath, FileMode.Open);

        Progress data = (Progress)bf.Deserialize(file);

        if (progressVersion == data.version)
        {
            diamonds = data.diamonds;
            perkPoints = data.perkPoints;
            artefactValue = data.artefactValue;
            mana = data.mana;
            wholeGold = data.gold;
            gold = data.gold;
            stone = data.stone;
            coal = data.coal;
            copper = data.copper;
            iron = data.iron;
            silver = data.silver;
            aurum = data.aurum;
            platinum = data.platinum;
            levelIndex = data.levelIndex;
            playerLevel = data.playerLevel;
            skillLevels = data.skillLevels;
            abilities = new DateTime[data.abilities.Length];
            for (int i = 0; i < data.abilities.Length; i++)
            {
                DateTime.TryParseExact(data.abilities[i], timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out abilities[i]);
            }
            skillTree = data.skillTree;
            helpersLevels = data.helpersLevels;
            helpersSkills = data.helpersSkills;
            artefacts.AddRange(data.artefacts);
            inventory = data.inventory;
            passiveFarm = data.passiveFarm;
            passiveFarmStep = data.passiveFarmStep;
            warehouseBuffs = data.warehouseBuffs;
            achievementDatas = data.achievementDatas;
            wauProgress = data.wauProgress;
            prestiges = data.prestiges;
            tutorialStep = data.tutorialStep;
            file.Close();
        }
        else
        {
            file.Close();
            File.Delete(progressPath);

            return false;
        }

        return true;
    }

    public static void SaveProgress()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(progressPath);

        Progress data = new Progress();
        data.version = progressVersion;
        data.mana = mana;
        data.diamonds = diamonds;
        data.perkPoints = perkPoints;
        data.artefactValue = artefactValue;
        data.wholeGold = gold;
        data.gold = gold;
        data.stone = stone;
        data.coal = coal;
        data.copper = copper;
        data.iron = iron;
        data.silver = silver;
        data.aurum = aurum;
        data.platinum = platinum;
        data.levelIndex = levelIndex;
        data.playerLevel = playerLevel;
        data.skillLevels = skillLevels;
        data.abilities = new string[abilities.Length];
        for (int i = 0; i < abilities.Length; i++)
        {
            data.abilities[i] = abilities[i].ToString(timePattern, System.Globalization.CultureInfo.InvariantCulture);
        }
        data.skillTree = skillTree;
        data.helpersLevels = helpersLevels;
        data.helpersSkills = helpersSkills;
        data.artefacts = artefacts.ToArray();
        data.inventory = inventory;
        data.passiveFarm = passiveFarm;
        data.passiveFarmStep = passiveFarmStep;
        data.warehouseBuffs = warehouseBuffs;
        data.achievementDatas = achievementDatas;
        data.wauProgress = wauProgress;
        data.prestiges = prestiges;
        data.tutorialStep = tutorialStep;

        bf.Serialize(file, data);
        file.Close();
    }

    public static void DeleteSaveFiles()
    {
        if (File.Exists(profilePath))
            File.Delete(profilePath);

        if (File.Exists(progressPath))
            File.Delete(progressPath);
    }

    string GetKey()
    {
        return SystemInfo.deviceUniqueIdentifier;
    }

    string Encrypt(string clearText)
    {
        byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(GetKey(), new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    string Decrypt(string cipherText)
    {
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(GetKey(), new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = System.Text.Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }
}