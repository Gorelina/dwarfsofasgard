﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AspectFix : MonoBehaviour
{

    public bool pos;
    public bool scale;

    void Awake()
    {
        if (pos)
        {
            transform.position = new Vector3(Formules.FixAspect(transform.position.x), transform.position.y, transform.position.z);
        }

        if (scale)
        {
            transform.localScale = new Vector3(Formules.FixAspect(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
    }
}