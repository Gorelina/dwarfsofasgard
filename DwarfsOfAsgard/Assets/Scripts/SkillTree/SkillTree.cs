﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillTree : MonoBehaviour
{

    public TreeNode[] nodes;
    [Header("UI")]
    public GameObject notification;
    public GameObject selection;
    public Text perkPointsText;
    public Text skillName;
    public Text skillEffects;
    public Image selectedIcon;
    public Button buyButton;
    [Space]
    public GameObject[] tabs;
    public Image[] tabButton;
    public Sprite tabNormal;
    public Sprite tabSelected;
    private int perkPoints;
    private TreeNode prevSelected;
    public static SkillTree instance;

    void Awake()
    {
        instance = this;
        prevSelected = TreeNode.selected;
    }

    void Start()
    {
        perkPoints = SaveLoad.perkPoints;
        perkPointsText.text = perkPoints + " " + LanguageManager.ReturnString("TREE_POINTS");
        SelectTab(0);
    }

    void Update()
    {
        if (prevSelected != TreeNode.selected)
        {
            UpdateSelection();
            prevSelected = TreeNode.selected;
        }

        notification.SetActive(perkPoints > 0);
    }

    void UpdateSelection()
    {
        if (TreeNode.selected == null)
        {
            selection.SetActive(false);
            return;
        }

        buyButton.interactable = perkPoints > 0;
        selection.SetActive(true);
        selectedIcon.sprite = TreeNode.selected.data.icon.sprite;
        skillName.text = LanguageManager.ReturnString(TreeNode.selected.data.name);
        string effects = "";
        for (int i = 0; i < TreeNode.selected.data.effects.Length; i++)
        {
            effects += string.Format(PerksManager.instance.Description(TreeNode.selected.data.effects[i].effect), TreeNode.selected.data.effects[i].multiplier) + '\n';
        }
        skillEffects.text = effects;
        perkPointsText.text = perkPoints + " " + LanguageManager.ReturnString("TREE_POINTS");
    }

    public void AddPerkPoints(int _value)
    {
        perkPoints += _value;
        SaveLoad.perkPoints = perkPoints;
        perkPointsText.text = perkPoints + " " + LanguageManager.ReturnString("TREE_POINTS");
    }

    public void Buy()
    {
        if (TreeNode.selected == null)
            return;

        if (perkPoints <= 0)
            return;

        GameUI.instance.soundContainer.PlaySound("click");
        perkPoints--;
        perkPointsText.text = perkPoints + " " + LanguageManager.ReturnString("TREE_POINTS");
        SaveLoad.perkPoints = perkPoints;
        SaveLoad.SaveProgress();

        TreeNode.selected.Buy();
    }

    public List<PerkEffect> GetEffects()
    {
        List<PerkEffect> effects = new List<PerkEffect>();
        for (int i = 0; i < nodes.Length; i++)
        {
            if (nodes[i].IsBought())
                effects.AddRange(nodes[i].data.effects);
        }

        return effects;
    }

    public void SelectTab(int index)
    {
        GameUI.instance.soundContainer.PlaySound("click");

        for (int i = 0; i < tabs.Length; i++)
        {
            tabs[i].SetActive(i == index);
            tabButton[i].sprite = i == index ? tabNormal : tabSelected;
        }

        TreeNode.ClearSelection();
    }

    public void ClearSelection()
    {
        TreeNode.ClearSelection();
    }

}