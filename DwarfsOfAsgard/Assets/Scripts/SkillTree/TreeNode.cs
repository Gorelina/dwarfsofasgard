﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreeNode : MonoBehaviour
{

    public TreeSkill data;
    public TreeNode[] parents;
    [Header("UI")]
    public Tween arrow;
    public Tween[] paths;
    public Image icon;
    public Image circle;
    public Sprite normalSprite;
    public Sprite selectedSprite;
    public GameObject boughtCircle;
    public static TreeNode selected;
    private bool bought;

    void Start()
    {
        icon.sprite = data.icon.sprite;

        bought = SaveLoad.skillTree[data.id];

        UpdateState();
    }

    bool CanBuy()
    {
        foreach (TreeNode parent in parents)
        {
            if (!parent.bought)
                return false;
        }

        return true;
    }

    public void Select()
    {
        GameUI.instance.soundContainer.PlaySound("click");

        if (selected == this)
        {
            selected = null;
        }
        else
        {
            if (selected != null)
            {
                TreeNode temp = selected;
                selected = this;
                temp.UpdateState();
            }
            else
            {
                selected = this;
            }
        }

        UpdateState();
    }

    public void Buy()
    {
        if (!CanBuy())
            return;

        if (bought)
            return;

        bought = true;

        SaveLoad.skillTree[data.id] = true;
        SaveLoad.SaveProgress();

        foreach (Tween path in paths)
            path.Play();
        arrow.Play();

        SkillTree skillTree = SkillTree.instance;
        for (int i = 0; i < skillTree.nodes.Length; i++)
        {
            skillTree.nodes[i].UpdateState();
        }
    }

    public bool IsBought()
    {
        return bought;
    }

    void UpdateState()
    {
        if (boughtCircle.activeSelf != bought)
            boughtCircle.SetActive(bought);

        circle.sprite = selected == this ? selectedSprite : normalSprite;
        if (bought)
        {
            icon.color = Color.white;

            if (paths.Length > 0 && !paths[0].isPlaying)
            {
                foreach (Tween path in paths)
                    path.ToEnd();
            }
        }
        else
        {
            icon.color = CanBuy() ? Color.white : new Color(0.75f, 0.75f, 0.75f, 0.5f);
        }
    }

    public static void ClearSelection()
    {
        selected = null;

        SkillTree skillTree = SkillTree.instance;
        for (int i = 0; i < skillTree.nodes.Length; i++)
        {
            skillTree.nodes[i].UpdateState();
        }
    }
}