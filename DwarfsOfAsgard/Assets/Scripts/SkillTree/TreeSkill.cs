﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Tree Skill", menuName = "General/Tree Skill")]
public class TreeSkill : DBEntry
{

    public SpriteImage icon;
    public PerkEffect[] effects;
}