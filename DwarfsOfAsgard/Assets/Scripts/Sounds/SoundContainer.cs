﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundContainer : MonoBehaviour
{
    [System.Serializable]
    public class Sound
    {
        public string id;
        [Range(0, 128)]
        public int priority;
        [Range(0, 1)]
        public float volume;
        public AudioClip[] clips;
    }

    public Sound[] sounds;
    public AudioSource source;
    private int lastPriority;

    public void StopSound()
    {
        source.Stop();
    }

    public void PlaySound(string index)
    {
        int ind = -1;
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].id == index)
                ind = i;
        }

        if (ind < 0)
            return;

        if (source.isPlaying && sounds[ind].priority > lastPriority)
            return;

        if (sounds[ind].clips.Length <= 0)
            return;

        lastPriority = sounds[ind].priority;
        source.Stop();
        source.volume = sounds[ind].volume;
        source.clip = sounds[ind].clips.Length > 1 ? sounds[ind].clips[Random.Range(0, sounds[ind].clips.Length)] : sounds[ind].clips[0];
        source.Play();
    }
}