﻿using UnityEngine;

[System.Serializable]
public struct SpriteImage
{
    public Sprite sprite;
}