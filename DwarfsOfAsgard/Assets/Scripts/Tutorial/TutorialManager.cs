﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{

    [System.Serializable]
    public struct TutorialSubstep
    {
        public List<string> buttonNames;
    }

    [System.Serializable]
    public class TutorialStep
    {
        [Tooltip("Only for internal use")]
        public string name;
        public bool stopTime;
        public TutorialSubstep[] substeps;
    }

    public Animator animator;
    public TutorialStep[] steps;
    private int step;
    private int subStep;
    private Button[] buttons;
    private ScrollRect[] scrollRects;
    public static TutorialManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        step = SaveLoad.tutorialStep;
        subStep = 0;
        SetAnimator();
        animator.SetTrigger("Init");

        buttons = FindObjectsOfTypeAll(typeof(Button)) as Button[];
        scrollRects = FindObjectsOfTypeAll(typeof(ScrollRect)) as ScrollRect[];
    }

    void Update()
    {
        #region StepsSequence
        //Tap -> empty
        if (step == 0 && Mining.instance.GetCurrentBlock() > 1)
            NextStep();

        //Empty -> boss -> empty(dialog window)
        if (step == 1 && Mining.instance.GetCurrentBlock() == 8)
            NextStep();

        //empty -> upgrade -> empty(after upgrade)
        if (step == 3 && Mining.instance.GetCoins() >= Hero.instance.GetLevelCost(1))
            NextStep();

        //empty -> sell -> empty(after sell)
        if (step == 5 && Mining.instance.GetCurrentLevel() == 1 && Mining.instance.GetCurrentBlock() == 2)
            NextStep();

        //empty -> helper
        if (step == 7 && Mining.instance.GetCoins() >= HelpersController.instance.helpers[HelpersController.instance.helpers.Length - 1].GetLevelCost(1))
            NextStep();

        //helper -> empty(after buying)
        if (step == 8 && HelpersController.instance.ActiveHelpersCount() > 0)
            NextStep();

        //empty -> helper upgrade
        if (step == 9 && Mining.instance.GetCoins() > HelpersController.instance.helpers[HelpersController.instance.helpers.Length - 1].GetLevelCost(1))
            NextStep();

        //helper upgrade -> empty(after upgrade)
        if (step == 10 && HelpersController.instance.helpers[15].GetLevel() > 1)
            NextStep();

        //empty -> new item -> empty(dialog window)
        if (step == 11 && Inventory.instance.GetItemCount() > 0)
            NextStep();

        //empty -> end
        if (step >= 13 && Mining.instance.GetCurrentLevel() > 9 && animator.enabled)
        {
            animator.enabled = false;
            ActivateAll();
        }
        #endregion

        if (Time.timeScale == 1)
        {
            if (steps[step].stopTime)
                Time.timeScale = 0;
        }
        else
        {
            if (!steps[step].stopTime)
                Time.timeScale = 1;
        }

#if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.F1))
        {
            Skip();
        }
#endif
    }

    void Skip()
    {
        print("skip");
        SetStep(13);
        animator.enabled = false;
        ActivateAll();
    }

    void ActivateAll()
    {
        foreach (Button button in buttons)
            button.enabled = true;

        foreach (ScrollRect scrollRect in scrollRects)
        {
            scrollRect.vertical = true;
        }
    }

    void ReloadButtons()
    {
        if (steps[step].substeps[subStep].buttonNames.Count == 0)
        {
            ActivateAll();
            return;
        }

        foreach (ScrollRect scrollRect in scrollRects)
        {
            scrollRect.vertical = false;
            scrollRect.StopMovement();
        }

        foreach (Button button in buttons)
        {
            button.enabled = steps[step].substeps[subStep].buttonNames.Contains(button.gameObject.name) || button == GameUI.instance.messageYesButton;
        }
    }

    public void SetStep(int step)
    {
        this.step = step;
        subStep = 0;
        SaveLoad.tutorialStep = step;
        SaveLoad.SaveProgress();
        SetAnimator();
    }

    public int GetStep()
    {
        return step;
    }

    public void NextStep()
    {
        GameUI.instance.HideTabs();
        step++;
        subStep = 0;
        SaveLoad.tutorialStep = step;
        SaveLoad.SaveProgress();
        SetAnimator();
        ReloadButtons();
    }

    public void NextSubStep()
    {
        subStep++;
        if (subStep >= steps[step].substeps.Length)
            NextStep();
        else ReloadButtons();
        SetAnimator();
    }

    public void ShowTutorialMessage(string text)
    {
        UnityEngine.Events.UnityAction action = null;

        if (step == 2 || step == 12)
            action = delegate { NextStep(); };

        GameUI.instance.ShowWindow(LanguageManager.ReturnString(text), action, false, "OK");
    }

    void SetAnimator()
    {
        animator.SetInteger("Step", step);
        animator.SetInteger("SubStep", subStep);
    }
}