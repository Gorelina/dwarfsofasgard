﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WAU : MonoBehaviour
{

    public WAUItem[] items;
    public WAUForm[] forms;
    [Header("UI")]
    public GameObject window;
    private int activeForm;
    private DateTime time;
    private TimeSpan span;
    private bool reward;
    private TimeSpan targetSpan = new TimeSpan(12, 0, 0);
    public static WAU instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        DateTime.TryParseExact(SaveLoad.wauTime, SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out time);
        DateTime.TryParseExact(SaveLoad.entryTime, SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime nowTime);

        span = nowTime - time;
        if (span >= targetSpan)
            reward = true;
        if (span >= new TimeSpan(targetSpan.Hours * 2, targetSpan.Minutes * 2, targetSpan.Seconds * 2) && SaveLoad.levelIndex >= 10)
        {
            SaveLoad.wauProgress = 0;
            SaveLoad.SaveProfile();
            reward = true;
        }

        InvokeRepeating("UpdateTime", 60, 60);
    }

    void UpdateTime()
    {
        DateTime.TryParseExact(SaveLoad.wauTime, SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out time);
        DateTime.TryParseExact(SaveLoad.exitTime, SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime nowTime);

        span = nowTime - time;
        if (span >= targetSpan && SaveLoad.levelIndex >= 10)
            reward = true;

        TryOpenWindow();
    }

    void UpdateForms()
    {
        for (int i = 0; i < forms.Length; i++)
        {
            if (i == SaveLoad.wauProgress)
                activeForm = i;

            forms[i].Set(items[i], i == SaveLoad.wauProgress, i < SaveLoad.wauProgress, i + 1);
        }
    }

    public void TryOpenWindow()
    {
        if (!reward)
            return;

        UpdateForms();
        window.SetActive(true);
    }

    public void CloseWindow()
    {
        window.SetActive(false);
    }

    public void Collect()
    {
        if (!reward)
            return;

        GameUI.instance.soundContainer.PlaySound("click");
        NetworkRequests.GetTime(res =>
        {
            if (res.resultCode == "0")
            {
                SaveLoad.wauTime = res.currentTime;
                SaveLoad.wauProgress++;
                if (SaveLoad.wauProgress >= forms.Length)
                    SaveLoad.wauProgress = 0;
                SaveLoad.SaveProgress();
                SaveLoad.SaveProfile();

                reward = false;
                //reward logic here
                switch (items[activeForm].type)
                {
                    case WAUItem.Types.Gold:
                        Mining.instance.AddCoins(forms[activeForm].quantity);
                        break;

                    case WAUItem.Types.Diamonds:
                        Mining.instance.AddDiamonds((int)forms[activeForm].quantity.number);
                        break;

                    case WAUItem.Types.Perk:
                        SkillTree.instance.AddPerkPoints(1);
                        break;

                    case WAUItem.Types.Skill:
                        Hero.instance.ActivateSkill();
                        break;

                    case WAUItem.Types.ItemCommon:
                        Inventory.instance.AddRandom(Item.Rarity.Common);
                        break;

                    case WAUItem.Types.ItemRare:
                        Inventory.instance.AddRandom(Item.Rarity.Rare);
                        break;

                    case WAUItem.Types.ItemRareAbove:
                        Inventory.instance.AddRandom((Item.Rarity)UnityEngine.Random.Range(1, 4));
                        break;
                }
                UpdateForms();

                Invoke("CloseWindow", 1);
            }
        });
    }

    public TimeSpan GetSpan()
    {
        return span;
    }

    void OnApplicationPause(bool pause)
    {
        if (pause && SaveLoad.levelIndex >= 10)
        {
            TimeSpan ts = targetSpan - span;
            NotificationManager.ScheduleNotification(new NotificationManager.Notification(ts, "wau", 0, LanguageManager.ReturnString("WAU_NT_TITLE"), LanguageManager.ReturnString("WAU_NT_MESSAGE")));
        }
    }
}