﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WAUForm : MonoBehaviour
{

    public Image icon;
    public Text index;
    public Text name;
    public GameObject activeOutline;
    public GameObject collectedMark;
    [System.NonSerialized]
    public WAUItem data;
    private bool active;
    private bool collected;
    [System.NonSerialized]
    public bigNumber quantity;

    public void Set(WAUItem data, bool active, bool collected, int index)
    {
        this.data = data;
        this.active = active;
        this.collected = collected;
        if (data.type == WAUItem.Types.Gold)
        {
            Formules.GetLevelNumbers(SaveLoad.levelIndex, 8, out bigNumber hp, out bigNumber coins);
            quantity = coins;
            name.text = quantity.ToString() + "\n" + LanguageManager.ReturnString(data.name);
        }
        else if (data.type == WAUItem.Types.Diamonds)
        {
            quantity = new bigNumber(5);
            name.text = quantity.ToString() + "\n" + LanguageManager.ReturnString(data.name);
        }
        else
        {
            quantity = new bigNumber(1);
            name.text = LanguageManager.ReturnString(data.name);
        }

        icon.sprite = data.icon.sprite;
        this.index.text = index.ToString();
        activeOutline.SetActive(active);
        collectedMark.SetActive(collected);
    }

}