﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New WAU Item", menuName = "General/WAU Item")]
public class WAUItem : ScriptableObject
{

    public SpriteImage icon;
    new public string name;
    public enum Types {Diamonds, Gold, ItemCommon, ItemRare, ItemRareAbove, Skill, Perk };
    public Types type;

}