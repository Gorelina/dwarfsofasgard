﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Warehouse : MonoBehaviour
{

    public GameObject warehouseWindow;
    public GameObject notification;
    public GameObject buttonNotification;
    public Slider sliderFarm;
    public Text farmTime;
    public Button collectButton;
    [Header("Upgrade window")]
    public GameObject upgradeWindow;
    public Text levelText;
    public Slider progress;
    public Text progressTime;
    public Text currentFarm;
    public Text nextFarm;
    public Button upgradeButton;
    public Text stonePrice;
    public Text coalPrice;
    public Text copperPrice;
    private List<WarehouseBuff> buffs;
    private int step;
    private float passiveFarm;
    private float passiveTime;
    private bigNumber initialPrice;
    private bigNumber stones;
    private bigNumber coals;
    private bigNumber coppers;
    public static Warehouse instance;
    
    void Awake()
    {
        instance = this;
        InvokeRepeating("Farm", 60, 60);
        buffs = new List<WarehouseBuff>();
    }

    void Start()
    {
        InitPrice();
        LoadBuffs();

        passiveFarm = SaveLoad.passiveFarm;
        step = SaveLoad.passiveFarmStep;
        passiveTime = (1f - passiveFarm) / 0.01f;

        UpdateUI();
    }

    void Update()
    {
        if (passiveFarm < 1)
        {
            if (Hero.instance.IsActive())
            {
                passiveFarm += 0.00083f * Time.deltaTime;
                passiveFarm = Mathf.Clamp01(passiveFarm);
                passiveTime = (1f - passiveFarm) / 0.05f * 60;
            }
            else
            {
                passiveFarm += 0.00016f * Time.deltaTime;
                passiveFarm = Mathf.Clamp01(passiveFarm);
                passiveTime = (1f - passiveFarm) / 0.01f * 60;
            }
        }

        notification.SetActive(!warehouseWindow.activeSelf && passiveFarm >= 1);
        buttonNotification.SetActive(passiveFarm >= 1);

        if (warehouseWindow.activeSelf)
            UpdateUI();

        if (upgradeWindow.activeSelf)
        {
            progress.value = passiveFarm;
            TimeSpan result = TimeSpan.FromSeconds(passiveTime);
            progressTime.text = result.ToString(@"hh\:mm\:ss");
        }
    }

    void InitPrice()
    {
        stones = new bigNumber();
        coals = new bigNumber();
        coppers = new bigNumber();

        bigNumber cost = new bigNumber(0);
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                Formules.GetLevelNumbers(i, 8, out bigNumber hp, out bigNumber coins);
                cost += coins;
            }
        }

        Drop drop = Formules.CoinsToResources(cost);
        stones = drop.stone;
        coals = drop.coal;
        coppers = drop.copper;
    }

    void GetPrice(out bigNumber stone, out bigNumber coal, out bigNumber copper)
    {
        float mult = (1 + step * 3f);
        stone = new bigNumber(stones) * mult;
        coal = new bigNumber(coals) * mult;
        copper = new bigNumber(coppers) * mult;
    }

    void Farm()
    {
        SaveLoad.passiveFarm = passiveFarm;
        SaveLoad.SaveProgress();
    }

    bigNumber GetFarmReward(int additionalStep = 0)
    {
        Formules.GetLevelNumbers(16, 8, out bigNumber hp, out bigNumber coins);
        for (int i = 0; i < step + additionalStep; i++)
        {
            coins *= 1.3f;
        }
        return coins;
    }

    public void CollectFarm()
    {
        if (passiveFarm < 1)
            return;

        Mining.instance.AddCoins(GetFarmReward());
        passiveFarm = 0;
        if (Hero.instance.IsActive())
            passiveTime = 1f / 0.05f;
        else
            passiveTime = 1f / 0.01f;

        SaveLoad.passiveFarm = passiveFarm;
        SaveLoad.SaveProgress();
        UpdateUI();
    }

    public void Upgrade()
    {
        GameUI.instance.soundContainer.PlaySound("click");

        GetPrice(out bigNumber stone, out bigNumber coal, out bigNumber copper);
        Mining.instance.ProcessResources(0, stone);
        Mining.instance.ProcessResources(1, coal);
        Mining.instance.ProcessResources(2, copper);

        step++;
        SaveLoad.passiveFarmStep = step;
        SaveLoad.SaveProgress();

        UpdateUpgradeWindow();
    }

    public void OpenUpgradeWindow()
    {
        GameUI.instance.soundContainer.PlaySound("click");
        UpdateUpgradeWindow();
        upgradeWindow.SetActive(!upgradeWindow.activeSelf);
    }

    void UpdateUpgradeWindow()
    {
        levelText.text = LanguageManager.ReturnString("LEVEL").ToUpper() + ": " + (step + 1);
        currentFarm.text = GetFarmReward().ToString();
        nextFarm.text = GetFarmReward(1).ToString();
        GetPrice(out bigNumber stone, out bigNumber coal, out bigNumber copper);
        stonePrice.text = stone.ToString();
        coalPrice.text = coal.ToString();
        copperPrice.text = copper.ToString();
        Drop cost = new Drop();
        cost.stone = stone;
        cost.coal = coal;
        cost.copper = copper;

        upgradeButton.interactable = Mining.instance.CanBuyResources(cost);
    }

    void UpdateUI()
    {
        collectButton.interactable = passiveFarm >= 1;
        TimeSpan result = TimeSpan.FromSeconds(passiveTime);
        farmTime.text = result.ToString(@"hh\:mm\:ss");
        sliderFarm.value = passiveFarm;
    }

    public void AddBuff(WarehouseBuff buff)
    {
        buffs.Add(buff);
        SaveBuffs();
    }

    public List<WarehouseBuff> GetBuffs()
    {
        return buffs;
    }

    public int GetBuffsCount()
    {
        return buffs.Count;
    }

    void LoadBuffs()
    {
        buffs.Clear();
        for (int i = 0; i < SaveLoad.warehouseBuffs.Length; i++)
        {
            buffs.Add(DataBase.instance.warehouseBuffs[SaveLoad.warehouseBuffs[i]]);
        }
    }

    void SaveBuffs()
    {
        int[] data = new int[buffs.Count];
        for (int i = 0; i < buffs.Count; i++)
            data[i] = buffs[i].id;

        SaveLoad.warehouseBuffs = data;
        SaveLoad.SaveProgress();
    }
}