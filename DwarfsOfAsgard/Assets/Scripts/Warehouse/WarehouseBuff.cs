﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Warehouse Buff", menuName = "General/Warehouse Buff")]
public class WarehouseBuff : DBEntry
{

    public PerkEffect effect;

}