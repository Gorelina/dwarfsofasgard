﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class WarehouseBuffEntry : MonoBehaviour
{

    public enum Types {Resource, Diamonds, Ad };
    public Types type;
    public WarehouseBuff[] buffs;
    public Text buffName;
    public Text price;
    public Button buyButton;
    public int randomOffset;
    private DateTime adTime;
    private TimeSpan adSpan;
    private string adBuffName;
    private WarehouseBuff buff;
    private bigNumber rPrice;
    private int dPrice;

    void Start()
    {
        UnityEngine.Random.InitState(DateTime.Now.DayOfYear + randomOffset);

        if (type == Types.Ad)
        {
            DateTime.TryParseExact(SaveLoad.adBuffTime, SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out adTime);
            adSpan = DateTime.Now - adTime;
        }

        UpdateNumbers();
    }

    public void Buy()
    {
        switch (type)
        {
            case Types.Resource:
                if (Mining.instance.ProcessResources(0, rPrice))
                    Purchase();
                break;

            case Types.Diamonds:
                if (Mining.instance.ProcessDiamonds(dPrice))
                    Purchase();
                break;

            case Types.Ad:
                if (AdManager.RewardAdLoaded())
                {
                    AdManager.WatchRewarded(Purchase);
                }
                break;
        }
    }

    void Update()
    {
        switch (type)
        {
            case Types.Ad:
                if (adSpan.TotalMinutes < 10)
                {
                    adSpan = DateTime.Now - adTime;
                    buffName.text = (9 - adSpan.Minutes).ToString("D2") + ":" + (59 - adSpan.Seconds).ToString("D2");
                }
                else
                {
                    buffName.text = adBuffName;
                }
                buyButton.interactable = AdManager.RewardAdLoaded() && adSpan.TotalMinutes >= 10;
                break;

            case Types.Diamonds:
                buyButton.interactable = Mining.instance.GetDiamonds() >= dPrice;
                break;

            case Types.Resource:
                buyButton.interactable = Mining.instance.GetCurrency().stone >= rPrice;
                break;
        }
    }

    void Purchase()
    {
        switch (type)
        {
            case Types.Resource:
                break;

            case Types.Diamonds:
                Warehouse.instance.AddBuff(buff);
                break;

            case Types.Ad:
                adTime = DateTime.Now;
                SaveLoad.adBuffTime = adTime.ToString(SaveLoad.timePattern, System.Globalization.CultureInfo.InvariantCulture);
                SaveLoad.SaveProfile();
                Warehouse.instance.AddBuff(buff);
                break;
        }

        UpdateAllNumbers();
    }

    void UpdateNumbers()
    {
        buff = buffs[UnityEngine.Random.Range(0, buffs.Length)];
        int count = Warehouse.instance.GetBuffsCount();

        if (type == Types.Resource)
        {
            rPrice = bigNumber.RandomRange(new bigNumber(250), new bigNumber(500));
            rPrice = Formules.CoinsToResource(0, rPrice);

            for (int i = 0; i < count; i++)
                rPrice *= 1.3f;

            price.text = rPrice.ToString() + "\n" + LanguageManager.ReturnString("STONE").ToUpper();
        }
        else if (type == Types.Diamonds)
        {
            dPrice = UnityEngine.Random.Range(20, 31);

            for (int i = 0; i < count; i++)
                dPrice = (int)(dPrice * 1.1f);

            price.text = dPrice.ToString();
        }

        buffName.text = string.Format(PerksManager.instance.Description(buff.effect.effect), buff.effect.multiplier);
        if (type == Types.Ad)
            adBuffName = buffName.text;
    }

    static void UpdateAllNumbers()
    {
        WarehouseBuffEntry[] entries = FindObjectsOfType<WarehouseBuffEntry>();
        foreach (WarehouseBuffEntry entry in entries)
            entry.UpdateNumbers();
    }

}