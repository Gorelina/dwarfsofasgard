﻿using System.Diagnostics;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dbTest : MonoBehaviour
{
    public Text text;
    public Text text2;
    public int level;
    public bool hero;
    public bigNumber baseDamage;
    public bigNumber baseCost;
    private bigNumber cost;
    private bigNumber damage;
    private int lastLevelUpgrade;
    private List<DataBase.ImprovementEntry> improvementTable;

    void Start()
    {
        string result = "";
        //Random.InitState(1);

        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        for (int i = 0; i < 4; i++)
        {
            for (int j = 1; j <= 8; j++)
            {
                Formules.GetLevelNumbers(i, j, out bigNumber hp, out bigNumber resources);
                if (j < 8)
                    result += string.Format("HP = {0}   RSC = {1}\n", hp, resources);
                else
                    result += string.Format("<b>BOSS HP = {0}   RSC = {1}</b>\n", hp, resources);
            }
        }
        stopwatch.Stop();
        result += stopwatch.Elapsed.TotalSeconds.ToString();
        text.text = result;
    }
}