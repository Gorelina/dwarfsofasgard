﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Snapscreen))]
public class EditorSnapscreen : Editor {

    new SerializedProperty name;
    SerializedProperty path;
    SerializedProperty factor;
    SerializedProperty button;
    SerializedProperty key;
    SerializedProperty dateStamp;
    SerializedProperty useGUI;
    Snapscreen script;

    void OnEnable()
    {
        name = serializedObject.FindProperty("name");
        path = serializedObject.FindProperty("path");
        factor = serializedObject.FindProperty("factor");
        button = serializedObject.FindProperty("button");
        key = serializedObject.FindProperty("key");
        dateStamp = serializedObject.FindProperty("dateStamp");
        useGUI = serializedObject.FindProperty("useGUI");
        script = (Snapscreen)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(name);
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.PropertyField(path);
        if (path.stringValue.Length <= 0 || !System.IO.Directory.Exists(path.stringValue))
            path.stringValue = Application.dataPath;
        if (GUILayout.Button("browse", GUILayout.MaxWidth(64)))
        {
            path.stringValue = EditorUtility.SaveFolderPanel("Screenshot directory", "", "");
        }
        if (path.stringValue.Length > 0 && path.stringValue[path.stringValue.Length - 1] != '/')
        {
            path.stringValue += '/';
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.PropertyField(dateStamp);
        EditorGUILayout.PropertyField(factor);
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(button);
        EditorGUILayout.PropertyField(key);
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(useGUI);
        EditorGUILayout.Space();
        if (GUILayout.Button("Take"))
        {
            script.Take();
        }
        serializedObject.ApplyModifiedProperties();
    }
}