﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class Snapscreen : MonoBehaviour
{
    [Tooltip("Screenshot file name")]
    new public string name;
    [Tooltip("Path to save screenshot file")]
    public string path;
    [Tooltip("Resolution scale multiplier. E.g.: 1024x1024 with multiplier of 2 will give 2048x2048 screenshot")]
    public int factor = 1;
    [Tooltip("Button name to take screenshot from Input settings")]
    public string button = "Submit";
    [Tooltip("Custom key to take screenshot")]
    public KeyCode key;
    [Tooltip("Put date stamp on screenshot file name")]
    public bool dateStamp;
    [Tooltip("Show GUI. May cause delay while taking a screenshot")]
    public bool useGUI;
    private bool hideGUI;

    //On component reset
    void Reset()
    {
        name = "";
        path = Application.dataPath;
        factor = 1;
        button = "Submit";
        key = KeyCode.None;
        dateStamp = false;
        useGUI = false;
    }

    void Update()
    {
#if UNITY_EDITOR
        //Do not execute if game is not playing
        if (!Application.isPlaying)
            return;

        //Take screenshot by pressing button or key
        if (Input.GetButtonUp(button) || Input.GetKeyUp(key))
        {
            Take();
        }
#endif
    }

    void OnGUI()
    {
#if UNITY_EDITOR
        //Do not execute if gui is disabled
        if (!useGUI)
            return;

        //Do not execute if game is not playing
        if (!Application.isPlaying)
            return;

        //Do not execute if GUI is hidden
        if (hideGUI)
            return;

        //Draw window
        GUI.Window(0, new Rect(0,0, 256, 128), Window, "Snapscreen");
#endif
    }

    //Window
    void Window(int windowID)
    {
        //Draw "Take" button
        if (GUI.Button(new Rect(30, 41, 196, 46), "Take"))
        {
            //Hide GUI and take screenshot few frames after
            hideGUI = true;
            Invoke("Take", Time.deltaTime * 2);
        }
    }

    public void Take()
    {
#if UNITY_EDITOR
        //If game is not playing then open game window
        if (!Application.isPlaying)
            EditorApplication.ExecuteMenuItem("Window/General/Game");

        //Combining file path and name
        string additional;
        if (dateStamp)
            additional = System.DateTime.Now.ToString(" yyyy-MM-dd H-mm-ss.f") + ".png";
        else additional = ".png";
        //Capture screenshot
        ScreenCapture.CaptureScreenshot(path + "/" + name + additional, factor);

        //if GUI is hidden then unhide it
        if (hideGUI)
            Invoke("UnhideGUI", Time.deltaTime * 2);
#endif
    }

    //Unhide gui
    void UnhideGUI()
    {
        hideGUI = false;
    }
}