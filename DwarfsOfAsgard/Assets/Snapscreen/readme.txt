Place "Screenshooter" on empty GameObject or any other GameObject. Select approtiate resolution in Game view window of the Unity. Configure the script according to this description:

Name - file name of your screenshot.
Path - directory to save your screenshot.
Date stamp - place date stamp in the name of your screenshot file.
Factor - resolution multiplier. For example, if you set 1024*1024 resolution with factor 2 you would get 2048*2048 screenshot.

Button - input button name from Input settings to take screenshot.
Key - custom key to take screenshot.

Use GUI - use GUI to take screenshots inside of Play mode. WARNING: Snapping with GUI requires delays, not good if you want to take instant screenshot.
Take button - use it to take screenshots outside of Play mode.

Enjoy!